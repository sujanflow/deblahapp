//
//  AudioAndRecorderViewController.h
//  DeblahApp
//
//  Created by Sabuj on 11/6/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AudioAndRecorderViewController : UIViewController

@property(nonatomic, weak) IBOutlet UIButton *cancelBtn;

@property(nonatomic, weak) IBOutlet UIButton *playPauseBtn;
@property(nonatomic, weak) IBOutlet UIButton *stopBtn;

@property(nonatomic, weak) IBOutlet UILabel *timerLabel;

@property(nonatomic, weak) IBOutlet UISlider *slider;

@end
