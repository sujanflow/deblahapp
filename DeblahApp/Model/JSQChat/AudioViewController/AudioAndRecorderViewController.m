//
//  AudioAndRecorderViewController.m
//  DeblahApp
//
//  Created by Sabuj on 11/6/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "AudioAndRecorderViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface AudioAndRecorderViewController ()<AVAudioRecorderDelegate,AVAudioPlayerDelegate>
{
    //--- Recorder---
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    NSTimer *myTimer;
    BOOL isAudioPlay;
}
@end

@implementation AudioAndRecorderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
      [self setupRecorder];
    
    _slider.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


#pragma mark - Button Action

-(IBAction)cancelBtnAction:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)playPauseBtnAction:(UIButton *)button{
    
    if (isAudioPlay == NO) { // have to start record
        
        [self performSelector:@selector(recordStart) withObject:nil afterDelay:1.0];
       
      
        
    }

}

-(IBAction)stopBtnAction:(UIButton *)button{
    
     [self performSelector:@selector(recordStop) withObject:nil afterDelay:1.0];
}

#pragma mark - UI Slider Action

- (IBAction)sliderValueChanged:(UISlider *)sender {
    
   
    
}


#pragma mark - Recorder


-(void)setupRecorder{
    
    // Setup audio session
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error: nil];
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
                             sizeof (audioRouteOverride),&audioRouteOverride);
    
    
    
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
}


- (void)recordStart{
    
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        [recorder record];
        myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateSliderForRecord) userInfo:nil repeats:YES];
    }
    
}

- (void)recordStop{
    
    [recorder stop];
    
    recorder = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    
}

- (void)audioPlayWithURL:(NSURL *)url{
    
    if (!recorder.recording){
        
        if(isAudioPlay){
            
            [recorder stop];
            [player pause];
        }
        
        
        isAudioPlay = YES;
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [player setDelegate:self];
        [player play];
        
        [myTimer invalidate];
        myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateSliderForPlayer) userInfo:nil repeats:YES];
        
    }
}


- (void)updateSliderForPlayer{
    
    
    float minutes = floor(player.currentTime/60);
    float seconds = player.currentTime - (minutes * 60);
    
    NSString *time = [[NSString alloc]
                      initWithFormat:@"%0.0f:%0.0f",
                      minutes, seconds];
    self.timerLabel.text = time;
    
    
}
- (void)updateSliderForRecord {
    
    if([recorder isRecording])
    {
        float minutes = floor(recorder.currentTime/60);
        float seconds = recorder.currentTime - (minutes * 60);
        
        NSString *time = [[NSString alloc]
                          initWithFormat:@"%0.0f:%0.0f",
                          minutes, seconds];
        self.timerLabel.text = time;
        
    }
}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"Recorder" message:@"Want to play?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
     
        [self audioPlayWithURL:avrecorder.url];
      
        
    }];
                               
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
      
    }];
                                   
    [alertControl addAction:okAction];
    [alertControl addAction:cancelAction];

    [self presentViewController:alertControl animated:YES completion:nil];
    
    
    
    
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
    //[alert show];
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"Done" message:@"Finish playing the recording!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alertControl addAction:okAction];
    [self presentViewController:alertControl animated:YES completion:nil];
    isAudioPlay = NO;
    _slider.hidden = YES;
    
}
@end
