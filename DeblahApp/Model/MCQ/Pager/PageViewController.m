//
//  PageViewController.m
//  mypractice
//
//  Created by Twinbit Mac2 on 10/17/16.
//  Copyright © 2016 Dhiman Das. All rights reserved.
//

#import "PageViewController.h"
#import "MatchingTableViewCell.h"

@interface PageViewController ()<UITableViewDelegate,UITableViewDataSource>
{

    NSMutableArray *dataArray;

}
@end


@implementation PageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removePager) name:@"REMOVE_PAGER" object:nil];
    
    NSLog(@"%ld",(long)self.index);
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    if ([DELEGATE.quizType isEqualToString:@"PSYCHOLOGICAL"]) {
        [self loadPsychologicalData:self.index];
    }else{
        [self loadSocialData:self.index];
    }
    
 
   
}

-(void)loadPsychologicalData:(NSInteger)index{
    
    dataArray = [[NSMutableArray alloc] init];
    
    NSDictionary *dic = [DELEGATE.psychologicalQuizArray objectAtIndex:index];
    
    dataArray = [[dic valueForKey:@"OPTION"] mutableCopy];
    
    _titlelabel.text = [dic valueForKey:@"QUESTION"];
    
    [self.tableView reloadData];
    
}

-(void)loadSocialData:(NSInteger)index{
    
    dataArray = [[NSMutableArray alloc] init];
    
    NSDictionary *dic = [DELEGATE.socialQuizArray objectAtIndex:index];
    
    dataArray = [[dic valueForKey:@"OPTION"] mutableCopy];
    
    _titlelabel.text = [dic valueForKey:@"QUESTION"];
    
    [self.tableView reloadData];
    
}

-(void)viewWillLayoutSubviews{


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return dataArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"matchingCell"];
    
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MatchingTableViewCell" owner:self options:nil];
        cell = (MatchingTableViewCell *)[nib objectAtIndex:0];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    cell.titlelabel.text = [dataArray objectAtIndex:indexPath.row];
    
    if ([DELEGATE.quizType isEqualToString:@"PSYCHOLOGICAL"]) {
        
        NSNumber *number = [DELEGATE.psychologicalAnswerArray objectAtIndex:self.index];
        NSInteger value = [number integerValue];
        
        [cell.button setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
        
        if (value == -1) {
            [cell.button setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
            
        }else if(dataArray.count == 2 ) {
            
            if (value == indexPath.row)
            {
                [cell.button setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
            }
            
        }else if( dataArray.count > 2) {
            
            if(value-1 == indexPath.row){
               [cell.button setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
            }
           
        }
        
        
    }else{
        
        NSNumber *number = [DELEGATE.socialAnswerArray objectAtIndex:self.index];
        NSInteger value = [number integerValue];
        
        [cell.button setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
        
        if (value == -1) {
            [cell.button setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
            
        }else if(dataArray.count == 2 ) {
            
            if (value == indexPath.row)
            {
                [cell.button setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
            }
            
        }else if( dataArray.count > 2) {
            
            if(value-1 == indexPath.row){
                [cell.button setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
            }
            
        }
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MatchingTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    for (int i =0; i<dataArray.count; i++) {
        
        NSIndexPath *customIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        MatchingTableViewCell *cell = [tableView cellForRowAtIndexPath:customIndexPath];
        [cell.button setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
    }
    
    [cell.button setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
    
    
    if ([DELEGATE.quizType isEqualToString:@"PSYCHOLOGICAL"]) {
        
        NSInteger optionValue = (dataArray.count == 2 ) ? indexPath.row:  indexPath.row+1;
        [DELEGATE.psychologicalAnswerArray replaceObjectAtIndex:self.index withObject:[NSNumber numberWithInteger:optionValue]];
    }else{
        
        NSInteger optionValue = (dataArray.count == 2 ) ? indexPath.row:  indexPath.row+1;
        [DELEGATE.socialAnswerArray replaceObjectAtIndex:self.index withObject:[NSNumber numberWithInteger:optionValue]];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 35 :( IS_IPHONE_6S_PLUS ? 30 : ( IS_IPHONE_6 ? 28 : ( IS_IPHONE_X ? 28 : 25)));
}




-(void)removePager{
    
    dataArray = [[NSMutableArray alloc] init];
    
    _titlelabel.text = @"";
    
    [self.tableView reloadData];
}

@end
