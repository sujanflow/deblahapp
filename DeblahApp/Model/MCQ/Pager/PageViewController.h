//
//  PageViewController.h
//  mypractice
//
//  Created by Twinbit Mac2 on 10/17/16.
//  Copyright © 2016 Dhiman Das. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIViewController

@property (assign, nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UILabel *titlelabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
