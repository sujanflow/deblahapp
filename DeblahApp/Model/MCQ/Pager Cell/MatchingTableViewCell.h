//
//  MatchingTableViewCell.h
//  PageView
//
//  Created by TWINBIT MAC3 on 3/27/18.
//  Copyright © 2018 TWINBIT MAC3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titlelabel;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end
