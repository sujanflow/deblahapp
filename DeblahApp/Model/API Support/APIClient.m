//
//  APIClient.m
//  Mazurka
//
//  Created by Md Zaman on 10/20/15.
//  Copyright © 2015 Twinbit Limited. All rights reserved.
//

#import "APIClient.h"
#import "SVProgressHUD.h"

@implementation APIClient
{
    int i;
}


+(APIClient *)sharedInstance
{
    static APIClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[APIClient alloc] init];
    });
    return _sharedInstance;
}

-(void)saveUserDetails:(NSDictionary *)info{
    
    

    NSString *mail = [info valueForKey:@"email"];
    if (mail.length>0) {
        [[NSUserDefaults standardUserDefaults] setValue:mail forKey:@"MAIL_ID"];
    }
    
    NSString *userName = [info valueForKey:@"user_name"];
    if (userName.length>0) {
        [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"USER_NAME"];
    }
    
    NSString *firstName = [info valueForKey:@"first_name"];
    if (firstName.length>0) {
        [[NSUserDefaults standardUserDefaults] setValue:firstName forKey:@"FIRST_NAME"];
    }
    
    NSString *lastName = [info valueForKey:@"last_name"];
    if (lastName.length>0) {
        [[NSUserDefaults standardUserDefaults] setValue:lastName forKey:@"LAST_NAME"];
    }
    
    NSString *birthDay = [info valueForKey:@"birthday"];
    if (![birthDay isEqual: [NSNull null]]) {
        [[NSUserDefaults standardUserDefaults] setValue:birthDay forKey:@"BIRTH_DAY"];
    }
    
    NSString *phone = [info valueForKey:@"phone_number"];
    if (phone.length>0) {
        [[NSUserDefaults standardUserDefaults] setValue:phone forKey:@"PHONE_NUMBER"];
    }
    
    NSString *occupation = [info valueForKey:@"occupation"];
    if (occupation.length>0) {
        [[NSUserDefaults standardUserDefaults] setValue:occupation forKey:@"OCCUPATION"];
    }
    
    NSString *profileImage = [info valueForKey:@"profile_image"];
    if (![profileImage isEqual: [NSNull null]]) {
        [[NSUserDefaults standardUserDefaults] setValue:profileImage forKey:@"PROFILE_IMAGE"];
    }
    
    
    if (![[info valueForKey:@"gender"] isEqual: [NSNull null]]) {
        NSString *gender = [NSString stringWithFormat:@"%ld",[[info valueForKey:@"gender"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:gender forKey:@"GENDER"];
    }
    
    if (![[info valueForKey:@"religion"] isEqual: [NSNull null]]) {
        NSString *religion = [NSString stringWithFormat:@"%ld",[[info valueForKey:@"religion"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:religion forKey:@"RELIGION"];
    }
    
    if (![[info valueForKey:@"blood_group"] isEqual: [NSNull null]]) {
        NSString *blood =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"blood_group"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:blood forKey:@"BLOOD_GROUP"];
    }
    
    if (![[info valueForKey:@"beauty_level"] isEqual: [NSNull null]]) {
        NSString *beauty =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"beauty_level"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:beauty forKey:@"BEAUTY_LEVEL"];
    }
    
    if (![[info valueForKey:@"skin_color"] isEqual: [NSNull null]]) {
        NSString *skin =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"skin_color"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:skin forKey:@"SKIN_COLOR"];
    }
    
    if (![[info valueForKey:@"living_status"] isEqual: [NSNull null]]) {
        NSString *living =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"living_status"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:living forKey:@"LIVING_STATUS"];
    }
    
    if (![[info valueForKey:@"tribe_type"] isEqual: [NSNull null]]) {
        NSString *tribe =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"tribe_type"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:tribe forKey:@"TRIBE_TYPE"];
    }
    
    if (![[info valueForKey:@"education"] isEqual: [NSNull null]]) {
        NSString *education =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"education"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:education forKey:@"EDUCATION"];
    }
    
    
    if (![[info valueForKey:@"height"] isEqual: [NSNull null]]) {
        NSString *height =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"height"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:height forKey:@"HEIGHT"];
    }
    
    
    if (![[info valueForKey:@"weight"] isEqual: [NSNull null]]) {
        NSString *weight =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"weight"] integerValue]];
        [[NSUserDefaults standardUserDefaults] setValue:weight forKey:@"WEIGHT"];
    }
    
  
    //Points
    NSString *points =  [NSString stringWithFormat:@"%ld",[[info valueForKey:@"points"] integerValue]];
    [[NSUserDefaults standardUserDefaults] setValue:points forKey:@"POINTS"];
    
  
    //id
    NSString *myID =  [info valueForKey:@"id"];
    [[NSUserDefaults standardUserDefaults] setValue:myID forKey:@"MY_ID"];
    
   


    NSNumber *soulmate =  [info valueForKey:@"soulmate_complete"];
    BOOL soul = [soulmate boolValue];
    [[NSUserDefaults standardUserDefaults] setBool:soul forKey:@"SOULMATE_COMPLETE"];


    NSNumber *quizComplete =  [info valueForKey:@"quiz_complete"];
    BOOL quiz = [quizComplete boolValue];
    [[NSUserDefaults standardUserDefaults] setBool:quiz forKey:@"QUIZ_COMPLETE"];
    
    
 
}


- (void)postRegisterFCMToken:(NSDictionary*)info Foruser:(NSString *)userId WithComplisionBlock: (complitionBlockDic)compblock;{


    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,FCM_URL];
    
   
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];

    NSLog(@"_params %@",_params);

    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];


    

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:60];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];

    NSLog(@"urlString   %@",urlString);
    
    [manager POST:urlString parameters:_params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        NSLog(@"???????????????responseObject %@",responseObject);
        
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
         NSLog(@"responseCode  %ld %@", (long)[operation.response statusCode],operation.responseString);
        
    }];

}

#pragma mark -
#pragma mark - Basic Details

- (void)signUpWithInfo:(NSDictionary*)info andImage:(UIImage *)profileImage WithComplisionBlock: (complitionBlockDic)compblock
{
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,SIGNUP_URL];
    

    UIImage *squareImage = [AppSupport cropSquareImage:profileImage];
    UIImage *resize = [AppSupport resizeImageWithImage:squareImage convertToSize:CGSizeMake(370, 370)];
    UIImage *image = [AppSupport cropImageWithImage:resize andTargetSize:CGSizeMake(370, 280)];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *FileParamConstant = @"profile_image";
    
    NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
    _params = [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:1000];
    [request setHTTPMethod:@"POST"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; application/json; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: multipart/form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *imageName = [NSString stringWithFormat:@"%@_image.png",mail];
    
    NSLog(@"%@",imageName);
    
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: multipart/form-data; name=\"%@\"; filename=\"%@\"\r\n", FileParamConstant,imageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                
                if(dict.count>5){
                    [self saveUserDetails: dict];
                }
                
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
           compblock(nil);
        }
       
    }];
    
    
    
}


- (void)userProfileUpdateWithInfo:(NSDictionary*)info andImage:(UIImage *)profileImage WithComplisionBlock: (complitionBlockDic)compblock
{
    

    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,USER_BASIC_URL];
    
    UIImage *squareImage = [AppSupport cropSquareImage:profileImage];
    UIImage *resize = [AppSupport resizeImageWithImage:squareImage convertToSize:CGSizeMake(370, 370)];
    UIImage *image = [AppSupport cropImageWithImage:resize andTargetSize:CGSizeMake(370, 280)];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *FileParamConstant = @"profile_image";
    
    NSMutableDictionary *_params = [[NSMutableDictionary alloc] init];
    _params = [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:1000];
    [request setHTTPMethod:@"PUT"];
    
   //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];

    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; application/json; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: multipart/form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *imageName = [NSString stringWithFormat:@"%@_image.png",mail];
    
    NSLog(@"%@",imageName);
    
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: multipart/form-data; name=\"%@\"; filename=\"%@\"\r\n", FileParamConstant,imageName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                
                if(dict.count>5){
                    [self saveUserDetails: dict];
                }
                
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            
             compblock(nil);
        }
        
       
    }];
    
}

- (void)getUserDetailsWithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,USER_DETAILS_URL];
    

    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
 
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
   
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
   
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                
                if(dict.count>3){
                   [self saveUserDetails: dict];
                }
              
                
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
             compblock(nil);
        }
       
    }];
    
    
}



- (void)getBasicDropDownWithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,BASIC_DROPDOWN_URL];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
    
}
- (void)getSoulmateDropDownWithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,SOULMATE_DROPDOWN_URL];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
    
}


- (void)soulmateRequirmentWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,SOULMATE_REQUIREMENT_URL];
     
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
          
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
             compblock(nil);
        }
        
       
    }];
    
   
    
}


- (void)quizPsychologicalWithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,QUIZ_PSYCHOLOGICAL_URL];
    
    
  
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
             compblock(nil);
        }
        
        
    }];
    
    
    
}

- (void)quizSocialWithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,QUIZ_SOCIAL_URL];
    
    
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
          
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
             compblock(nil);
        }
        
       
    }];
    
    
    
}

- (void)answerQuizWithArray:(NSArray*)info  WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,QUIZ_ANSWER_URL];
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [[info objectAtIndex:0] mutableCopy];
  
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
   
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
   
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
   
    
    
    
   
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
             compblock(nil);
        }
        
    }];
    
    
}


- (void)answerQuizWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,QUIZ_ANSWER_URL];
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info  mutableCopy];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
          
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
              
                if ([httpResponse statusCode]== 500) {
                    
                    NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
                    [dic setValue:@"quiz" forKey:@"quiz"];
                     compblock((NSDictionary *)dic);
                }
                else{
                     compblock(nil);
                }
            }
            
        }else{
             compblock(nil);
        }
        
       
    }];
    
}


-(void)loginWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{
    
   
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,LOGIN_URL];
    
    NSLog(@"login urlString %@",urlString);
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
  
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
 
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
        
        
    }];
     

   
}

-(void)logOutWithComplisionBlock: (complitionBlockDic)compblock;
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,LOGOUT_URL];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
   
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
        
    }];
    
    
    
    
    
}

#pragma mark -
#pragma Mark - Forget Password

-(void)forgetPasswordWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,FORGET_MAIL_URL];
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
 //   [request setValue:token forHTTPHeaderField:@"TOKEN"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
             compblock(nil);
        }
        
       
    }];
}


-(void)resetForgetPasswordWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,FORGET_PASSWORD_RESET_URL];
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //   [request setValue:token forHTTPHeaderField:@"TOKEN"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
        
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                
                if ([httpResponse statusCode]== 201) {
                    
                    NSMutableDictionary *dic =[[NSMutableDictionary alloc] init];
                    [dic setValue:@"quiz" forKey:@"id"];
                    compblock((NSDictionary *)dic);
                }
                else{
                    compblock(nil);
                }
                
            }
            
        }else{
           compblock(nil);
        }
        
       
    }];
}


#pragma mark -
#pragma mark - Video Article Details


-(void)getVideoInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
{
    
    NSString *pageNo = [info valueForKey:@"PAGE_NO"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,VIDEO_URL,pageNo];
    
  
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
   
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {


        if (connectionError == nil) {

            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];

            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }

        }else{
           compblock(nil);
        }
    }];
}

-(void)getArticleInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
{
    
    NSString *pageNo = [info valueForKey:@"PAGE_NO"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,ARTICLE_URL,pageNo];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
           compblock(nil);
        }
        
       
    }];
}


- (void)getBannerInfoWithComplisionBlock: (complitionBlockDic)compblock{
   
   
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,BANNER_URL];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"dict %@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
        
        
    }];
}


#pragma mark -
#pragma mark - Other Profile

- (void)likeOtherProfileWithInfo:(NSDictionary*)info andID:(NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *likeUrl = [NSString stringWithFormat:@"%@%@/likes/",OTHER_USER_DETAILS_URL,userID];

    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,likeUrl];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
        
    }];
}

- (void)dislikeOtherProfileWithInfo:(NSDictionary*)info andID:(NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *dislikeUrl = [NSString stringWithFormat:@"%@%@/unlikes/",OTHER_USER_DETAILS_URL,userID];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,dislikeUrl];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
           
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
           compblock(nil);
        }
        
       
    }];
}

- (void)getUnlikeReasonWithInfoWithComplisionBlock: (complitionBlockDic)compblock{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,UNLIKE_REASON_URL];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
}

- (void)getOtherProfileDetailsWithInfo:(NSString*)info WithComplisionBlock: (complitionBlockDic)compblock
{
   
    NSString *userID = [NSString stringWithFormat:@"%@/",info];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,OTHER_USER_DETAILS_URL,userID];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                
                compblock((NSDictionary *)dict);
                
                
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
    
}


#pragma mark - Search Details

- (void)getSearchInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *name = [info valueForKey:@"NAME"];
    NSString *quiryUrl = [NSString stringWithFormat:@"?user_name=%@&first_name=%@&last_name=%@",name,name,name];

    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SEARCH_URL,quiryUrl];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
}

- (void)getSearchInfo:(NSDictionary*)info andpageNo:(NSInteger)pageNo WithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *name = [info valueForKey:@"NAME"];
   
    NSString *quiryUrl = [NSString stringWithFormat:@"?page=%ld&user_name=%@&first_name=%@&last_name=%@",pageNo,name,name,name];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SEARCH_URL,quiryUrl];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
}

#pragma mark - Tab Bar Details

- (void)getMySoulmateInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
 
    NSString *pageNo = [info valueForKey:@"PAGE_NO"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,MY_SOULMATE_URL,pageNo];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
    }];
    
}

- (void)getSoulmateSentRequestInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *pageNo = [info valueForKey:@"PAGE_NO"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SOULMATE_SENT_REQUEST_URL,pageNo];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
    }];
    
}
- (void)getSoulmateReceivedRequestInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *pageNo = [info valueForKey:@"PAGE_NO"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SOULMATE_RECEIVED_REQUEST_URL,pageNo];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
    }];
    
}


- (void)getNotificationInfoWithComplisionBlock: (complitionBlockDic)compblock
{
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,NOTIFICATION_URL];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
        
        
    }];
    
}
#pragma mark - Payment Details

- (void)getPackageListInfoWithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,PACKAGE_LIST_URL];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    //add JWT token
    NSString *token = [NSString stringWithFormat:@"jwt %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"]]  ;
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
}
- (void)getCreatePayInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,CREATE_PAY_URL];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
        
        
    }];
    
}
- (void)getTransactionInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,TRANSACTION_URL];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
        
        
    }];
    
}


#pragma mark - Chat Details

- (void)getChatHistryListInfoPage:(NSInteger)pageNo WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *pageNum = [NSString stringWithFormat:@"%ld",(long)pageNo];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,CHAT_LIST_URL,pageNum];
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            //NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
            
        }else{
            compblock(nil);
        }
    }];
}


- (void)getChatMessageInfo: (NSDictionary *)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *userID = [info valueForKey:@"USER_ID"];
    NSString *pageNO = [info valueForKey:@"PAGE"];
    
    NSString *getUrl = [NSString stringWithFormat:@"%@%@%@%@",CHAT_MY_POST_URL,userID,CHAT_GET_URL,pageNO];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,getUrl];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSString *boundary = @"unique-consistent-string";
    
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    
    [request setHTTPBody:body];
    
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSLog(@"%@",dict);
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
    }];
    
    
}

- (void)postChatMessageInfo: (NSDictionary *)info andToUserId: (NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock{
    
 
    NSString *postUrl = [NSString stringWithFormat:@"%@%@/chat/",CHAT_MY_POST_URL,userID];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,postUrl];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
  //  NSData *imageData = UIImageJPEGRepresentation([_params objectForKey:@"media"], 1.0);
    
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    NSLog(@"_params %@",_params);
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"attachment\"; filename=\%@\r\n",@"file"] dataUsingEncoding:NSUTF8StringEncoding]];
//    //[body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[NSData dataWithData:imageData]];
   
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError == nil) {
            
            NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            if ([(NSDictionary *)dict count]) {
                compblock((NSDictionary *)dict);
            }else{
                compblock(nil);
            }
        }else{
            compblock(nil);
        }
        
        
    }];
    
}


- (void)postPhotoMessageInfo: (NSDictionary *)info andToUserId: (NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock{
    
    
    NSString *postUrl = [NSString stringWithFormat:@"%@%@/chat/",CHAT_MY_POST_URL,userID];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,postUrl];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    NSString *fileName=[_params objectForKey:@"fileName"];
    
    NSData *imageData = UIImageJPEGRepresentation([_params objectForKey:@"media"], 1.0);
    
    [_params removeObjectForKey:@"media"];
    NSLog(@"_params %@",_params);
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:60];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
     manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager POST:urlString parameters:_params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileData:imageData name:@"attachment" fileName:fileName mimeType:@"image/jpeg"];
         
     }success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([(NSDictionary *)responseObject count]) {
             compblock((NSDictionary *)responseObject);
         }else{
             compblock(nil);
         }
         
     }failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSInteger statusCode = [operation.response statusCode];
         NSLog(@"responseCode  %ld %@", (long)statusCode,operation.responseString);
         
         NSLog(@"localizedDescription %@",error);
         compblock(nil);
     }
     
     ];
        
}

// For Audio and Video
- (void)postMediaMessageInfo: (NSDictionary *)info andToUserId: (NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock{
    
    
    NSString *postUrl = [NSString stringWithFormat:@"%@%@/chat/",CHAT_MY_POST_URL,userID];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,postUrl];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    NSString *fileName=[_params objectForKey:@"fileName"];
    
    
    NSData *mediaData = [NSData dataWithContentsOfFile:[_params objectForKey:@"media"]];
    
    [_params removeObjectForKey:@"media"];
    [_params removeObjectForKey:@"fileType"];
    NSLog(@"_params %@",_params);
    
    // Basic Auth
    NSString *mail = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:@"PASSWORD"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", mail, password];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:60];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
   // manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"text/html", nil];
    
    
    [manager POST:urlString parameters:_params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         NSString *filetype = [info objectForKey:@"fileType"];
         
         //for video
         if ([filetype isEqualToString:@"video"]) {
             
             [formData appendPartWithFileData:mediaData name:@"attachment" fileName:fileName mimeType:@"video/quicktime"];
         }else{
             //for audio
             
             [formData appendPartWithFileData:mediaData name:@"attachment" fileName:fileName mimeType:@"audio/m4a"];
             
         }
         
         
     }success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"success success");
         
         if ([(NSDictionary *)responseObject count]) {
             compblock((NSDictionary *)responseObject);
         }else{
             compblock(nil);
         }
         
     }failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSInteger statusCode = [operation.response statusCode];
         NSLog(@"responseCode  %ld %@", (long)statusCode,operation.responseString);
         
         NSLog(@"localizedDescription %@",error);
         compblock(nil);
     }
     
     ];
    
}


/*


/*

- (void)restoreForgetPasswordWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,FORGET_PASSWORD_STRING];
    
    [self showSVProgressHUD];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[info valueForKey:@"EmailOrPhone"], @"EmailOrPhone", [info valueForKey:@"UserEmail"], @"UserEmail", [info valueForKey:@"PhoneNumber"], @"PhoneNumber", nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];
}


- (void)updateProfileWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,UPDATE_PROFILE_STRING];
    
    [self showSVProgressHUD];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];
}


-(void)uploadPhoto: (UIImage *)image andInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock{
    
    [self showSVProgressHUDWithStatus:@"Uploading"];
    
    NSString *urlString = [NSString stringWithFormat:BASE_URL,UPDATE_PROFILE_STRING];
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"UserPicture";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data

    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
    
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        [self dismissSVProgressHUD];
        
        
        NSLog(@"%@",dict);
        if ([(NSDictionary *)dict count]) {
            compblock((NSDictionary *)dict);
        }else{
            compblock(nil);
        }
    }];
}



- (void)addToCartWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{
    [self showSVProgressHUD];
    NSString *urlString = [NSString stringWithFormat:BASE_URL,ADD_TO_CART_STRING];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];

    [manager POST:urlString parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        [self dismissSVProgressHUD];

        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getAllAreaNameWithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,AREA_BASE_STRING];
    
    [self searchUrlString:urlString WithComplisionBlockDic:^(NSDictionary *resultData) {
        if ([resultData count]) {
            compblock(resultData);
        }else{
            compblock(nil);
        }
    }];
    
}


- (void)getAllRestaurentsWithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,BASE_SEARCH_STRING];

    
    [self searchUrlStringWithParams:urlString WithComplisionBlockDic:^(NSDictionary *resultData) {
        if ([resultData count]) {
            compblock(resultData);
        }else{
            compblock(nil);
        }
    }];
}


- (void)getAllReviews:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{    NSString *urlString = [NSString stringWithFormat:BASE_URL,REVIEW_SEARCH_STRING];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];
}

- (void)addReviews:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{    NSString *urlString = [NSString stringWithFormat:BASE_URL,ADD_REVIEW_STRING];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];
}

- (void)editReviews:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,EDIT_REVIEW_STRING];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];
}


- (void)getRestaurantInfoWithID:(NSString*)resID WithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,RESTAURANT_INFO_STRING];
    NSDictionary* dict=[NSDictionary dictionaryWithObjectsAndKeys:resID,@"RestaurantID", nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];
}



- (void)getAllOrdersWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock
{
    
    
    NSString *urlString = [NSString stringWithFormat:BASE_URL,ORDER_STRING];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissSVProgressHUD];
        
        if ([(NSDictionary *)responseObject count]) {
            compblock((NSDictionary *)responseObject);
        }else{
            compblock(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dismissSVProgressHUD];
        
    }];

}


- (void)getAllRestaurentsMenuItemWithRestaurantID:(NSString *)resID WithComplisionBlock: (complitionBlockDic)compblock
{
    NSString *urlString = [NSString stringWithFormat:BASE_URL,ITEM_SEARCH_STRING];
    
    [self searchMenuForRestaurantWithURL:urlString andParams:resID WithComplisionBlockDic:^(NSDictionary *resultData) {
        if ([resultData count]) {
            compblock(resultData);
        }else{
            compblock(nil);
        }
    }];
}


- (void)searchUrlString:(NSString*)urlString WithComplisionBlockDic: (complitionBlockDic)compblockDic
{
    NSLog(@"URL :%@", urlString);
    
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:urlString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        compblockDic((NSDictionary *)responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        
    }];
}


- (void)searchUrlStringWithParams:(NSString*)urlString WithComplisionBlockDic: (complitionBlockDic)compblockDic
{
    NSLog(@"URL :%@", urlString);
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:DELEGATE.cityName, @"location", DELEGATE.areaName, @"area", DELEGATE.foodName, @"food", nil];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];

    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        compblockDic((NSDictionary *)responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


- (void)searchMenuForRestaurantWithURL:(NSString*)urlString andParams:(NSString *)resID WithComplisionBlockDic: (complitionBlockDic)compblockDic
{
    NSLog(@"URL :%@", urlString);
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params = [NSMutableDictionary dictionaryWithObjectsAndKeys:resID, @"RestaurantID", nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        compblockDic((NSDictionary *)responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}



- (void)showSVProgressHUD
{
    [SVProgressHUD setRingRadius:800];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD show];
}


- (void)showSVProgressHUDWithStatus: (NSString *)status
{
//    [SVProgressHUD setRingRadius:800];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD showWithStatus:status];
}


- (void)dismissSVProgressHUD
{
    [SVProgressHUD dismiss];
}


-(void)showAlertWithMessage: (NSString *)msg withTwoOption:(BOOL) isTwo{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:ok];
    if (isTwo) {
        [alert addAction:cancel];
    }
    
    [DELEGATE.window.rootViewController presentViewController:alert animated:YES completion:nil];
}
 
 */



#pragma mark - Token

- (void)obtainTokenWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,OBTAIN_TOKEN_URL];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        
        NSLog(@"%@",dict);
        if ([(NSDictionary *)dict count]) {
            compblock((NSDictionary *)dict);
        }else{
            compblock(nil);
        }
    }];
}

- (void)refreshTokenWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,LOGIN_URL];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        
        NSLog(@"%@",dict);
        if ([(NSDictionary *)dict count]) {
            compblock((NSDictionary *)dict);
        }else{
            compblock(nil);
        }
    }];
}
- (void)verifyTokenWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,LOGIN_URL];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    _params= [info mutableCopy];
    
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    
    
    // set Content-Type in HTTP header
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        
        NSLog(@"%@",dict);
        if ([(NSDictionary *)dict count]) {
            compblock((NSDictionary *)dict);
        }else{
            compblock(nil);
        }
    }];
}



- (void)showSVProgressHUD
{
    [SVProgressHUD setRingRadius:800];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD show];
}


- (void)showSVProgressHUDWithStatus: (NSString *)status
{
    [SVProgressHUD setRingRadius:800];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD showWithStatus:status];
}


- (void)dismissSVProgressHUD
{
    [SVProgressHUD dismiss];
}


@end
