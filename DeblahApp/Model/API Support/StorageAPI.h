//
//  StorageAPI.h
//  DeblahApp
//
//  Created by Sabuj on 20/4/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#ifndef StorageAPI_h
#define StorageAPI_h



//--------API----------

//-- BASE URL DETAILS ----------//
#define BASE_URL @"http://159.65.22.249:24525"

//----------FCMToken

#define FCM_URL @"/api/device/"

//--------TOKEN -----------
#define OBTAIN_TOKEN_URL @"/api/core/jwt/obtain-token/"
#define REFRESH_TOKEN_URL @"/api/core/jwt/refresh-token/"
#define VERIFY_TOKEN_URL @"/api/core/jwt/verify-token/"


//-- LOGIN URL DETAILS ----------//
#define LOGIN_URL @"/api/core/auth/login/"
#define LOGOUT_URL @"/api/core/auth/logout/"

#define SIGNUP_URL @"/api/core/auth/register/"

#define USER_BASIC_URL @"/api/core/auth/user/"
#define USER_DETAILS_URL @"/api/core/auth/user/"

#define SOULMATE_REQUIREMENT_URL @"/api/core/auth/user/soulmate-settings/requirement/"

#define BASIC_DROPDOWN_URL @"/api/core/auth/user/profile-options/"
#define SOULMATE_DROPDOWN_URL @"/api/core/auth/user/soulmate-settings/options/"



//-- QUIZ  DETAILS ----------//
#define QUIZ_PSYCHOLOGICAL_URL @"/api/quiz/psychological/"
#define QUIZ_SOCIAL_URL @"/api/quiz/social/"
#define QUIZ_ANSWER_URL @"/api/quiz/answer/"




//-- FORGET PASSWORD DETAILS ----------//
#define FORGET_MAIL_URL @"/api/core/auth/request-password-reset/"
#define FORGET_PASSWORD_RESET_URL @"/api/core/auth/password-reset/"



//-- VIDEO ARTICLE BANNER DETAILS ----------//
#define VIDEO_URL @"/api/content/videos/?page="
#define ARTICLE_URL @"/api/content/articles/?page="
#define BANNER_URL @"/api/content/banners/"


//----Unlike Reason---

#define UNLIKE_REASON_URL @"/api/core/auth/user/preference/unlike-reasons/"

//-- OTHERS PROFILE ----------//
#define OTHER_USER_DETAILS_URL @"/api/core/auth/user/"


//--SEARCH -----
#define SEARCH_URL @"/api/core/auth/user/soulmates-search/"


//-- TAB BAR ----------//
#define MY_SOULMATE_URL @"/api/core/auth/user/soulmates/?page="
#define SOULMATE_SENT_REQUEST_URL @"/api/core/auth/user/preference/sent-requests/?page="
#define SOULMATE_RECEIVED_REQUEST_URL @"/api/core/auth/user/preference/received-requests/?page="

#define NOTIFICATION_URL @"/api/notifications/"
#define CHAT_HISTORY_URL @"/api/core/auth/user/likes/"


//-- PAYMENT ----------//
#define PACKAGE_LIST_URL @"/api/payment/packages/"
#define TRANSACTION_URL @"/api/payment/transaction/"
#define CREATE_PAY_URL @"/api/payment/create-pay/"


//------CHAT----
#define CHAT_LIST_URL @"/api/chat/?page="
#define CHAT_MY_POST_URL @"/api/core/auth/user/"
#define CHAT_GET_URL @"/chat/?page="




#endif /* StorageAPI_h */
