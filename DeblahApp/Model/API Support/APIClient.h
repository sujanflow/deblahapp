//
//  APIClient.h
//  Mazurka
//
//  Created by Md Zaman on 10/20/15.
//  Copyright © 2015 Twinbit Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^complitionBlockArray)(NSArray *);
typedef void(^complitionBlockDic)(NSDictionary *);
typedef void(^thumbNailomplitionBlock)(UIImage *);


typedef void(^completionBlock)(NSDictionary*);

@interface APIClient : AFHTTPSessionManager
@property (nonatomic, strong) AFHTTPRequestOperationManager *operationManager;

+(APIClient *)sharedInstance;

//------HUD-----

- (void)showSVProgressHUD;
- (void)showSVProgressHUDWithStatus: (NSString *)status;
- (void)dismissSVProgressHUD;
-(void)showAlertWithMessage: (NSString *)msg withTwoOption:(BOOL) isTwo;

//------Token-----
- (void)obtainTokenWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock;
- (void)refreshTokenWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock;
- (void)verifyTokenWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock;

//------FCMToken-----

- (void)postRegisterFCMToken:(NSDictionary*)info Foruser:(NSString *)userId WithComplisionBlock: (complitionBlockDic)compblock;

//------Basic-----

- (void)signUpWithInfo:(NSDictionary*)info andImage:(UIImage *)profileImage WithComplisionBlock: (complitionBlockDic)compblock;

- (void)userProfileUpdateWithInfo:(NSDictionary*)info andImage:(UIImage *)profileImage WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getUserDetailsWithComplisionBlock: (complitionBlockDic)compblock;


- (void)getBasicDropDownWithComplisionBlock: (complitionBlockDic)compblock;
- (void)getSoulmateDropDownWithComplisionBlock: (complitionBlockDic)compblock;


- (void)soulmateRequirmentWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock;
- (void)quizPsychologicalWithComplisionBlock: (complitionBlockDic)compblock;
- (void)quizSocialWithComplisionBlock: (complitionBlockDic)compblock;
- (void)answerQuizWithInfo:(NSDictionary*)info  WithComplisionBlock: (complitionBlockDic)compblock;
- (void)answerQuizWithArray:(NSArray*)info  WithComplisionBlock: (complitionBlockDic)compblock;


- (void)loginWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)logOutWithComplisionBlock: (complitionBlockDic)compblock;
- (void)forgetPasswordWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)resetForgetPasswordWithInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;


//------Video-Article Banner-----

- (void)getVideoInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getArticleInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getBannerInfoWithComplisionBlock: (complitionBlockDic)compblock;



//------Other-Profile-----
- (void)likeOtherProfileWithInfo:(NSDictionary*)info andID:(NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock;
- (void)dislikeOtherProfileWithInfo:(NSDictionary*)info andID:(NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getUnlikeReasonWithInfoWithComplisionBlock: (complitionBlockDic)compblock;
- (void)getOtherProfileDetailsWithInfo:(NSString*)info WithComplisionBlock: (complitionBlockDic)compblock;


//------Search Details-----
- (void)getSearchInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getSearchInfo:(NSDictionary*)info andpageNo:(NSInteger)pageNo WithComplisionBlock: (complitionBlockDic)compblock;


//------Tab Bar Details-----
- (void)getMySoulmateInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getSoulmateSentRequestInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getSoulmateReceivedRequestInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;

- (void)getNotificationInfoWithComplisionBlock: (complitionBlockDic)compblock;

//------Payment Details-----
- (void)getPackageListInfoWithComplisionBlock: (complitionBlockDic)compblock;
- (void)getCreatePayInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getTransactionInfo:(NSDictionary*)info WithComplisionBlock: (complitionBlockDic)compblock;

//------ Chat Details----
- (void)getChatHistryListInfoPage: (NSInteger)pageNo WithComplisionBlock: (complitionBlockDic)compblock;
- (void)getChatMessageInfo: (NSDictionary *)info WithComplisionBlock: (complitionBlockDic)compblock;
- (void)postChatMessageInfo: (NSDictionary *)info andToUserId: (NSString *)userId WithComplisionBlock: (complitionBlockDic)compblock;
- (void)postPhotoMessageInfo: (NSDictionary *)info andToUserId: (NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock;
- (void)postMediaMessageInfo: (NSDictionary *)info andToUserId: (NSString *)userID WithComplisionBlock: (complitionBlockDic)compblock;


@end
