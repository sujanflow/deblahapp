//
//  AppSupport.h
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSupport : NSObject

+(BOOL) mailAuthentication:(NSString *)mailText;
+(BOOL) phoneAuthentication:(NSString *)phoneText;


+(NSString *) dateParser:(NSString *)dateStr;
+(NSString *) getTotalAge:(NSString *)dateStr;

+(NSInteger) nextPageParser:(NSString *)str;



// -----Image
+ (UIImage *)cropImageWithImage:(UIImage *)image andTargetSize: (CGSize)size;
+(UIImage *)cropSquareImage:(UIImage *)image ;
+ (UIImage *)resizeImageWithImage:(UIImage *)image convertToSize:(CGSize)size ;

+(NSString*) getDateStringFromDate:(NSDate*)date;
+(NSDate*) getNSDateFromServerDate:(NSString*)serverDateString;

@end
