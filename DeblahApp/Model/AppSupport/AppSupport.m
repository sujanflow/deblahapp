//
//  AppSupport.m
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "AppSupport.h"

@implementation AppSupport

+(BOOL) mailAuthentication:(NSString *)mailText{
    
    BOOL stricterFilter = NO; 
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:mailText];
}


+(BOOL) phoneAuthentication:(NSString *)phoneText{
    
//    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
//    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//
//    return [phoneTest evaluateWithObject:phoneText];
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([phoneText rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        return YES;
    }
    
    return NO;
}

+(BOOL) hightAuthentication:(NSString *)hightText{
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([hightText rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        NSInteger height = [hightText integerValue];
        
        return (height > 30 && height <120) ? YES : NO;
    }
    
    return NO;
}

+(BOOL) weightAuthentication:(NSString *)weightText{
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([weightText rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        NSInteger weight = [weightText integerValue];
        
        return weight > 20 ? YES : NO;
    }
    
     return NO;
}


+(NSString *) dateParser:(NSString *)dateStr{
    
    //article video
   // 2018-05-02T07:33:55.094961Z
    
    NSArray *items = [dateStr componentsSeparatedByString:@"T"];
    
    NSString *str = [items objectAtIndex:0];

    return str;
}

+(NSString *) getTotalAge:(NSString *)birthDate
{
    //2016-05-02

    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:birthDate]];
    int allDays = (((time/60)/60)/24);
    int days = allDays%365;
    int years = (allDays-days)/365;
    
    NSString *yearStr = [NSString stringWithFormat:@"%d",years];
    return yearStr;
}

+(NSDate*) getNSDateFromServerDate:(NSString*)serverDateString
{
    //2016-05-02
    
    NSDateFormatter *datef=[[NSDateFormatter alloc]init];
    [datef setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z"]; // Date formater
    NSDate *date = [datef dateFromString:serverDateString];
    
    return date;
}
+(NSString*) getDateStringFromDate:(NSDate*)date
{
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    df.timeStyle = NSDateFormatterShortStyle;
//    df.dateStyle = NSDateFormatterNoStyle;
//    df.doesRelativeDateFormatting = NO;
//    return [df stringFromDate:date];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd MMM , YYYY"]; // Date formater
    NSString *dateString = [dateformate stringFromDate:date]; // Convert date to string
    NSLog(@"dateString :%@",dateString);
    
    return dateString.length>0?dateString:@"N/A";
}


+(NSInteger) nextPageParser:(NSString *)str{
    
   // http://159.65.22.249:24525/api/content/videos/?page=2
    NSArray *items = [str componentsSeparatedByString:@"page="];
    
    NSString *count = [items objectAtIndex:1];
    
    NSInteger page = [count integerValue];
    
    page = page>1 ? page : 1;
    
    return page;
}



#pragma mark - UIImage Catagory

+ (UIImage *)cropImageWithImage:(UIImage *)image andTargetSize: (CGSize)size{
    

    double refWidth = CGImageGetWidth(image.CGImage);
    double refHeight = CGImageGetHeight(image.CGImage);
    
    double x=0,y=0,h=0,w=0;
    
    
    x = 0;
    y = (refWidth-size.height)/2.0;
    w = refWidth;
    h = refHeight-(2*y);
    
    CGRect cropRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return croppedImage;
}
+(UIImage *)cropSquareImage:(UIImage *)image {
    
    
    
    double refWidth = CGImageGetWidth(image.CGImage);
    double refHeight = CGImageGetHeight(image.CGImage);
    
    double x=0,y=0,h=0,w=0;
    
    if (refWidth>=refHeight) {
        
        
        x = (refWidth/2.0) - (refHeight/2.0);
        y = 0;
        w = refHeight;
        h = refHeight;
        
        
    }else{
        
        y = (refHeight/2.0) - (refWidth/2.0);
        x = 0;
        w = refWidth;
        h = refWidth;
    }
    
    
    CGRect cropRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return croppedImage;
}

+ (UIImage *)resizeImageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

@end
