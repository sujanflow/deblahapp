//
//  SettingsTableViewCell.h
//  DeblahApp
//
//  Created by Sabuj on 4/1/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *cardLabel;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@end
