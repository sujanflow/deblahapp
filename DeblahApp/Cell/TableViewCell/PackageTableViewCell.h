//
//  PackageTableViewCell.h
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIView *mainContentView;


@property (nonatomic, weak) IBOutlet UIImageView *coverImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UIButton *loveButton;

@end
