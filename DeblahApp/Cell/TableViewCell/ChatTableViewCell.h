//
//  ChatTableViewCell.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *mainContentView;


@property (nonatomic, weak) IBOutlet UIImageView *coverImageView;

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;

@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@end
