//
//  HomeTableViewCell.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
    self.mainContentView.clipsToBounds = YES;
    self.mainContentView.layer.cornerRadius = 15;
    
    self.coverImageView.clipsToBounds = YES;
    self.coverImageView.layer.cornerRadius = 15;
    
    self.parcentLabel.clipsToBounds = YES;
    self.parcentLabel.layer.cornerRadius = self.parcentLabel.frame.size.width/2.0;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)likeBtnAction:(UIButton *)sender {
    
    UIView *parentCell = sender.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSLog(@"%ld",indexPath.row);
    
    if (self.delegate) {
          [self.delegate likeButtonPressedAtIndexPath:indexPath];
    }
  
}


@end
