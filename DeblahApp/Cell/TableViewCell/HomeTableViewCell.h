//
//  HomeTableViewCell.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol HomeTableViewCellDelegate <NSObject>

- (void)likeButtonPressedAtIndexPath:(NSIndexPath*)indexPath;


@end


@interface HomeTableViewCell : UITableViewCell

@property (weak, nonatomic) id<HomeTableViewCellDelegate> delegate;


@property (nonatomic, weak) IBOutlet UIView *mainContentView;

@property (nonatomic, weak) IBOutlet UIImageView *coverImageView;

@property (nonatomic, weak) IBOutlet UILabel *parcentLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageLabel;

@property (nonatomic, weak) IBOutlet UIButton *loveButton;



@end
