//
//  ChatTableViewCell.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "ChatTableViewCell.h"

@implementation ChatTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.mainContentView.clipsToBounds = YES;
    self.mainContentView.layer.cornerRadius = 12;
   
    self.coverImageView.clipsToBounds = YES;
    self.coverImageView.layer.cornerRadius = self.coverImageView.frame.size.width/2.0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
