//
//  AppDelegate.h
//  DeblahApp
//
//  Created by Sabuj on 3/22/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MainViewController.h"

#import <UserNotifications/UserNotifications.h>


#define DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
@property (strong, nonatomic) NSString *strDeviceToken;

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
- (void)saveContext;

@property (nonatomic) BOOL isReachable;
@property (nonatomic) BOOL isEditedProfile;

//--Payment--
@property (nonatomic) BOOL isPaymentComplete;
@property (nonatomic,strong) NSString *paymentID;


//---Search
@property (nonatomic,strong) NSString *SEARCH_TEXT;


//---TabBar
@property (strong,nonatomic) NSString *TAB_IMAGE_NAME;
@property (nonatomic) NSInteger TABBAR_SELECTED_INDEX;

//--Banner ---
@property (nonatomic,strong) NSURL *bannerNotificationURL;
@property (nonatomic,strong) NSURL *bannerVideoURL;
@property (nonatomic,strong) NSURL *bannerArticleURL;


//Array
 @property (nonatomic,strong) NSMutableArray *soulmateArray;


//---Quiz

@property (nonatomic, strong) NSString *quizType;
@property (nonatomic,strong) NSMutableArray *psychologicalQuizArray;
@property (nonatomic,strong) NSMutableArray *socialQuizArray;

@property (nonatomic,strong) NSMutableArray *psychologicalAnswerArray;
@property (nonatomic,strong) NSMutableArray *socialAnswerArray;


#pragma mark - Methods

-(void)makeLoginViewControllerAsRootViewController;
-(void)makeTabViewControllerAsRootViewController;


-(void)makeFillupPopUp:(NSString *)text;


@end

