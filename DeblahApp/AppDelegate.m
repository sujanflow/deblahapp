//
//  AppDelegate.m
//  DeblahApp
//
//  Created by Sabuj on 3/22/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "BasicInfoViewController.h"
#import "ParentTabViewController.h"
#import "SoulmateInfoViewController.h"
#import "MatchingStepOneViewController.h"
#import "MatchingStepTwoViewController.h"

#import "AFNetworkActivityIndicatorManager.h"

#import "NSUserDefaults+DemoSettings.h"

#import "Firebase.h"
@import UserNotifications;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate ()<UNUserNotificationCenterDelegate,FIRMessagingDelegate>
{
    UIView *fillUpPopUp;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
    [NSUserDefaults saveIncomingAvatarSetting:YES];
    [NSUserDefaults saveOutgoingAvatarSetting:NO];
    
    [self checkInternet];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DID_COMPLETE_LOGING"]) {
        
        [self makeTabViewControllerAsRootViewController];
        
       
        
    }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"QUIZ_COMPLETE"]){
        [self makeLoginViewControllerAsRootViewController];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DID_COMPLETE_SIGNUP"]){
        [self makeBasicInfoViewControllerAsRootViewController];
    }
    else{
        [self makeLoginViewControllerAsRootViewController];
    }
    
    
   // [[UIView appearance] setSemanticContentAttribute: UISemanticContentAttributeForceLeftToRight];
    
#pragma mark  configure_Firebase
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
#pragma mark  for_get_device_token_and_push_notification
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}




#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"DeblahApp"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}



#pragma mark - PUSH Notification


//- (void)registerForRemoteNotification {
//
//    if (@available(iOS 10.0, *)) {
//
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
//            if( !error ){
//                [[UIApplication sharedApplication] registerForRemoteNotifications];
//            }
//        }];
//
//    }else{
//
//        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        [[UIApplication sharedApplication] registerForRemoteNotifications];
//    }
//
//}

#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    [FIRMessaging messaging].APNSToken = deviceToken;
    
    NSString *deviceTokenString = [[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceTokenString = [deviceTokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    
    
    NSLog(@"deviceToken %@",deviceTokenString);
    NSLog(@"refreshedToken %@",fcmToken);
    
    self.strDeviceToken=deviceTokenString;
    
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] setValue:fcmToken forKey:@"registrationToken"];
    

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DID_COMPLETE_LOGING"]) {
        
        [self registerFCMTokenFromAppDelegate];
        
    }
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification Information : %@",userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    NSLog(@"didReceiveRemoteNotification %@",userInfo);
    
    if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive - the user has tapped in the notification when app was closed or in background");
        //do some tasks
        //    [self manageRemoteNotification:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
    }
    else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"application Background - notification has arrived when app was in background");
        //    NSString* contentAvailable = [NSString stringWithFormat:@"%@", [[userInfo valueForKey:@"aps"] valueForKey:@"content-available"]];
        
        //        if([contentAvailable isEqualToString:@"1"]) {
        //            // do tasks
        //            [self manageRemoteNotification:userInfo];
        //            NSLog(@"content-available is equal to 1");
        completionHandler(UIBackgroundFetchResultNewData);
        //        }
    }
    else {
        NSLog(@"application Active - notication has arrived while app was opened");
        //Show an in-app banner
        //do tasks
        //    [self manageRemoteNotification:userInfo];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateMessages" object:nil];
        
        completionHandler(UIBackgroundFetchResultNewData);
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateMessages" object:nil];
    
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    NSLog(@"User Info didReceiveNotificationResponse = %@",response.notification.request.content.userInfo);
    
    completionHandler();
}


- (void) registerFCMTokenFromAppDelegate{
    
    NSLog(@"registrationToken in appdelegate : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"registrationToken"]);
    NSLog(@"deviceToken in appdelegate : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]);
    
    
    
}


#pragma mark - RootViewController

-(void)makeLoginViewControllerAsRootViewController{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    LoginViewController *loginVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"LoginViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginVC];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}

-(void)makeBasicInfoViewControllerAsRootViewController{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    BasicInfoViewController *basicVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"BasicInfoViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:basicVC];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}

-(void)makeTabViewControllerAsRootViewController{
    
   // dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
        ParentTabViewController *tabVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"ParentTabViewController"];
        
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:tabVC];
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
 //   });
   
}


-(void)makeSoulmateViewControllerAsRootViewController{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    SoulmateInfoViewController *soulmateVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"SoulmateInfoViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:soulmateVC];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}

-(void)makeMatchingStepOneViewControllerAsRootViewController{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    MatchingStepOneViewController *oneVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"MatchingStepOneViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:oneVC];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}

-(void)makeMatchingStepTwoViewControllerAsRootViewController{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    MatchingStepTwoViewController *twoVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"MatchingStepTwoViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:twoVC];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}


#pragma mark-
#pragma mark - Internet Check

-(void)checkInternet{
    
    self.isReachable = [[AFNetworkReachabilityManager sharedManager] isReachable];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        
        if ([AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            
            self.isReachable = NO;
            
            [self makeFillupPopUp:@"No Internet Access"];
        }
        else
            self.isReachable = YES;
        
    }];
}
#pragma mark-
#pragma mark - PopUp

-(void)makeFillupPopUp:(NSString *)text{
    
    if (fillUpPopUp != nil) {
        [fillUpPopUp removeFromSuperview];
    }
    
    fillUpPopUp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 90)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 220, 60)];
    label.text = text;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    [fillUpPopUp addSubview:label];
    
    fillUpPopUp.backgroundColor = [UIColor colorWithRed:20/255.0 green:20/255.0 blue:20/255.0 alpha:0.7];
    fillUpPopUp.center = DELEGATE.window.center;
    [DELEGATE.window addSubview:fillUpPopUp];
    fillUpPopUp.alpha = 0.0;
    
    fillUpPopUp.clipsToBounds = YES;
    fillUpPopUp.layer.cornerRadius = 10;
    
    [UIView animateWithDuration:0.3 animations:^{
        
         fillUpPopUp.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
        [self performSelector:@selector(removeFillupPopUp) withObject:nil afterDelay:0.3];
    }];
    
}

-(void)removeFillupPopUp{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        fillUpPopUp.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        [fillUpPopUp removeFromSuperview];
    }];
}

@end
