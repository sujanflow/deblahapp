//
//  VideoArticleTableViewCell.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "VideoArticleTableViewCell.h"

@implementation VideoArticleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.coverImageView.clipsToBounds = YES;
    self.coverImageView.layer.cornerRadius = 8.0;
    
    self.mainContentView.clipsToBounds = YES;
    self.mainContentView.layer.cornerRadius = 8.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
