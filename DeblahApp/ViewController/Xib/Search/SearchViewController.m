//
//  SearchViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "SearchViewController.h"
#import "HomeTableViewCell.h"

#import "OtherProfileViewController.h"
#import "UnlikeViewController.h"


@interface SearchViewController ()<UITableViewDelegate, UITableViewDataSource,HomeTableViewCellDelegate>
{
  
    NSMutableArray *searchArray;
    
    BOOL nextSearchPageAvailable;
    NSInteger searchCurrentPage;
    
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    self.navigationController.navigationBar.hidden = YES;
    
    _homeTableViewCell.delegate = self;
    UINib *nib = [UINib nibWithNibName:@"HomeTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"homeCell"];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    searchArray = [[NSMutableArray alloc] init];
    [self getSearchDataFromAPI:DELEGATE.SEARCH_TEXT];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getSearchDataFromAPI: (NSString *)searchText{
    
    searchCurrentPage = 1;
    searchArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *searchDic = [[NSMutableDictionary alloc] init];
    [searchDic setValue:searchText forKey:@"NAME"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getSearchInfo:searchDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if (result.count>3) {
            
            [searchArray removeAllObjects];
            
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextSearchPageAvailable = YES;
                searchCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextSearchPageAvailable = NO;
            }
            
            NSArray *resultArray = [[result valueForKey:@"results"]  mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSDictionary *searchDic = [resultArray objectAtIndex:i];
                
                NSString *userId = [searchDic valueForKey:@"id"];
                NSString *profileImg = [searchDic valueForKey:@"profile_image"];
                NSString *userName = [searchDic valueForKey:@"user_name"];
                NSString *userBirth = [searchDic valueForKey:@"age"];
                
                NSNumber *userLike = [searchDic valueForKey:@"liked"];
                NSInteger liked =  [userLike integerValue];
                NSString *like_Status = (liked == 0)? @"NO" : @"YES";
                
                NSNumber *userChat = [searchDic valueForKey:@"chat"];
                NSInteger chated =  [userChat integerValue];
                NSString *chat_Status = (chated == 0)? @"NO" : @"YES";
                
                
                if (profileImg == [NSNull null]) {
                    profileImg = @"www.google.com";
                }
                if (userBirth == [NSNull null]) {
                    userBirth = @"0";
                }
               
             
                
                
                NSMutableDictionary *soulmateDictionary = [[NSMutableDictionary alloc] init];
                
                [soulmateDictionary setValue:userId forKey:@"ID"];
                [soulmateDictionary setValue:profileImg forKey:@"IMAGE_URL"];
                [soulmateDictionary setValue:userName forKey:@"NAME"];
                [soulmateDictionary setValue:userBirth forKey:@"AGE"];
                
                [soulmateDictionary setValue:like_Status forKey:@"LOVE_REACTION"];
                [soulmateDictionary setValue:chat_Status forKey:@"CHAT_REACTION"];
                
                
                [searchArray addObject:soulmateDictionary];
                
            }
            
            [_tableView reloadData];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
    }];
}


-(void)getSearchDetailsByPage:(NSInteger) page{
    
    
    
    NSMutableDictionary *searchDic = [[NSMutableDictionary alloc] init];
    [searchDic setValue:DELEGATE.SEARCH_TEXT forKey:@"NAME"];
   
    

    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getSearchInfo:searchDic andpageNo:page WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if (result.count>3) {
            
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextSearchPageAvailable = YES;
                searchCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextSearchPageAvailable = NO;
            }
            
            NSArray *resultArray = [[result valueForKey:@"results"]  mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSDictionary *searchDic = [resultArray objectAtIndex:i];
                
                NSString *userId = [searchDic valueForKey:@"id"];
                NSString *profileImg = [searchDic valueForKey:@"profile_image"];
                NSString *userName = [searchDic valueForKey:@"user_name"];
                NSString *userBirth = [searchDic valueForKey:@"age"];
                
                NSNumber *userLike = [searchDic valueForKey:@"liked"];
                NSInteger liked =  [userLike integerValue];
                NSString *like_Status = (liked == 0)? @"NO" : @"YES";
                
                NSNumber *userChat = [searchDic valueForKey:@"chat"];
                NSInteger chated =  [userChat integerValue];
                NSString *chat_Status = (chated == 0)? @"NO" : @"YES";
                
                
                if (profileImg == [NSNull null]) {
                    profileImg = @"www.google.com";
                }
                if (userBirth == [NSNull null]) {
                    userBirth = @"0";
                }
                
                
                
                NSMutableDictionary *soulmateDictionary = [[NSMutableDictionary alloc] init];
                
                [soulmateDictionary setValue:userId forKey:@"ID"];
                [soulmateDictionary setValue:profileImg forKey:@"IMAGE_URL"];
                [soulmateDictionary setValue:userName forKey:@"NAME"];
                [soulmateDictionary setValue:userBirth forKey:@"AGE"];
                
                [soulmateDictionary setValue:like_Status forKey:@"LOVE_REACTION"];
                [soulmateDictionary setValue:chat_Status forKey:@"CHAT_REACTION"];
                
                
                [searchArray addObject:soulmateDictionary];
            }
            
            [_tableView reloadData];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
    }];
}

#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return searchArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.homeTableViewCell = (HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    self.homeTableViewCell.delegate = self;
    

    
    self.homeTableViewCell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self.homeTableViewCell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [searchArray objectAtIndex:indexPath.row];
    
    NSURL *imageUrl = [dic valueForKey:@"IMAGE_URL"];
    NSString *name = [dic valueForKey:@"NAME"];
    NSString *age = [dic valueForKey:@"AGE"];
   
    NSString *loveReaction = [dic valueForKey:@"LOVE_REACTION"];
    NSString *chatReaction = [dic valueForKey:@"CHAT_REACTION"];
    
    
    self.homeTableViewCell.nameLabel.text = name;
    self.homeTableViewCell.ageLabel.text = [NSString stringWithFormat:@"%@ Years",age];
    self.homeTableViewCell.parcentLabel.hidden = YES;
    
    if ([loveReaction isEqualToString:@"NO"]) {
        self.homeTableViewCell.loveButton.tag = 0;
        [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-off"] forState:UIControlStateNormal];
        
    }else{
        [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-on"] forState:UIControlStateNormal];
         self.homeTableViewCell.loveButton.tag = 1;
        if ([chatReaction isEqualToString:@"YES"]) {
            [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-all"] forState:UIControlStateNormal];
             self.homeTableViewCell.loveButton.tag = 2;
        }
    }
    
    
    [self.homeTableViewCell.coverImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"home-default"] options:SDWebImageRefreshCached];
    
    
    self.homeTableViewCell.layer.shadowColor= [UIColor redColor].CGColor;
    self.homeTableViewCell.layer.shadowRadius = 1.0;
    self.homeTableViewCell.layer.shadowOffset = CGSizeMake(0, 1);
    self.homeTableViewCell.layer.shadowOpacity = 0.1;
    
    
    
    if (indexPath.row == searchArray.count-5) {
        if (DELEGATE.isReachable) {
            if (nextSearchPageAvailable) {
                dispatch_async(dispatch_get_main_queue(), ^{
                   [self getSearchDetailsByPage:searchCurrentPage];
                });
            }
        }
    }
    
    return self.homeTableViewCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 550:(IS_IPHONE_5 ? 285 : (IS_IPHONE_6 ? 320:(IS_IPHONE_X ? 320: 353)));
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    NSDictionary *dic = [searchArray objectAtIndex:indexPath.row];
    NSString *userID = [dic valueForKey:@"ID"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getOtherProfileDetailsWithInfo:userID WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            NSNumber *profileComplete = [result valueForKey:@"profile_complete"];
            NSNumber *soulmateComplete = [result valueForKey:@"soulmate_complete"];
            NSNumber *quizComplete = [result valueForKey:@"quiz_complete"];
            
            if ([profileComplete boolValue] && [soulmateComplete boolValue] && [quizComplete boolValue] ) {
               
                NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
                
                OtherProfileViewController *otherProfileViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"OtherProfileViewController"];
                otherProfileViewController.userDic = dic;
                otherProfileViewController.fromVC = @"SEARCH";
                otherProfileViewController.indexPathRow = indexPath.row;
                
                [self.navigationController pushViewController:otherProfileViewController animated:YES];
                
                
            }else{
                
                [DELEGATE makeFillupPopUp:@"Profile Not Completed"];
            }
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}


#pragma mark - Cell Delegate

-(void)likeButtonPressedAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = [searchArray objectAtIndex:indexPath.row];
    
    NSString *loveReaction = [dic valueForKey:@"LOVE_REACTION"];
    
    if ([loveReaction isEqualToString:@"NO"]) {
        [self likeAction:dic andIndex:indexPath];
    }else{
        [self unlikeAction:dic andIndex:indexPath.row];
    }
}

-(void)likeAction:(NSMutableDictionary *)dic andIndex:(NSIndexPath *)indexPath{
    
    NSString *userId = [dic valueForKey:@"ID"];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] likeOtherProfileWithInfo:nil andID:userId WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            HomeTableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
            
            [cell.loveButton setImage:[UIImage imageNamed:@"home-love-on"] forState:UIControlStateNormal];
            cell.loveButton.tag = 1;
            
            if ([[dic valueForKey:@"CHAT_REACTION"] isEqualToString:@"YES"]) {
                [cell.loveButton setImage:[UIImage imageNamed:@"home-love-all"] forState:UIControlStateNormal];
                cell.loveButton.tag = 2;
            }
            
            [dic removeObjectForKey:@"LOVE_REACTION"];
            [dic setValue:@"YES" forKey:@"LOVE_REACTION"];
            
            NSLog(@"%@",dic);
            
            [searchArray replaceObjectAtIndex:indexPath.row withObject:dic];
            
        }
        else if ([result valueForKey:@"non_field_errors"]){
            
            [DELEGATE makeFillupPopUp:@"Insufficient points. Please purchase more"];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

-(void)unlikeAction:(NSDictionary *)dic andIndex: (NSInteger)indexPathRow{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    UnlikeViewController *unlikeViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"UnlikeViewController"];
    
    unlikeViewController.userDic = dic;
    unlikeViewController.indexPathRow = indexPathRow;
    unlikeViewController.fromVC = @"SEARCH";
    
    [self.navigationController pushViewController:unlikeViewController animated:YES];
}

@end

