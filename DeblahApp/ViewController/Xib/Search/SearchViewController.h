//
//  SearchViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"

@interface SearchViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (strong, nonatomic) HomeTableViewCell *homeTableViewCell;


@end
