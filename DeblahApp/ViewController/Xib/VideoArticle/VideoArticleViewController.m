//
//  VideoArticleViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "VideoArticleViewController.h"
#import "VideoArticleTableViewCell.h"


@interface VideoArticleViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    
    NSMutableArray *videoArray;
    NSMutableArray *articleArray;

    BOOL loadVideoTableView;
    
    BOOL nextVideoPageAvailable;
    BOOL nextArticlePageAvailable;

    NSInteger videoCurrentPage;
    NSInteger articleCurrentPage;
    
}
@end

@implementation VideoArticleViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1];
    self.navigationController.navigationBar.hidden = YES;
    
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionHeaderHeight = IS_DEVICE_IPAD ? 60 : 40;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);

    
    videoArray = [[NSMutableArray alloc] init];
    articleArray = [[NSMutableArray alloc] init];
    
    videoCurrentPage = 1;
    articleCurrentPage = 1;
    
    [self videoButtonAction:nil];
    
    [self getDataFromAPI];
    
    

}

-(void)viewWillLayoutSubviews{
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)videoButtonAction:(UIButton *)button{
    
    loadVideoTableView = YES;
    [_videoButton setBackgroundImage:[UIImage imageNamed:@"video-selected"] forState:UIControlStateNormal];
    [_articleButton setBackgroundImage:[UIImage imageNamed:@"article-deselected"] forState:UIControlStateNormal];
    
    [_tableView reloadData];
    
    [self setBanner:DELEGATE.bannerVideoURL];
}
-(IBAction)articleButtonAction:(UIButton *)button{
    
    loadVideoTableView = NO;
    [_videoButton setBackgroundImage:[UIImage imageNamed:@"video-deselected"] forState:UIControlStateNormal];
    [_articleButton setBackgroundImage:[UIImage imageNamed:@"article-selected"] forState:UIControlStateNormal];
    
    [_tableView reloadData];
    
    [self setBanner:DELEGATE.bannerArticleURL];
}
-(void)getDataFromAPI{
    
    [self getVideoDetails:videoCurrentPage];
    [self getArticleDetails:articleCurrentPage];
}


-(void)getVideoDetails:(NSInteger)pageNumber{
    
    NSString *pageNO = [NSString stringWithFormat:@"%ld",(long)pageNumber];
    NSMutableDictionary *videoDic = [[NSMutableDictionary alloc] init];
    [videoDic setValue:pageNO forKey:@"PAGE_NO"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getVideoInfo:videoDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"results"]) {
            
           
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextVideoPageAvailable = YES;
                videoCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                 nextVideoPageAvailable = NO;
            }
            
            NSMutableArray *resultArray = [[result valueForKey:@"results"] mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSMutableDictionary *videoDic = [resultArray objectAtIndex:i];
                
                NSURL *imageUrl = [NSURL URLWithString:[videoDic valueForKey:@"image"]];
                NSURL *link = [NSURL URLWithString:[videoDic valueForKey:@"link"]];
                NSString *name = [videoDic valueForKey:@"title"];
                NSString *dateStr = [videoDic valueForKey:@"created_at"];
                NSString *durationStr = [videoDic valueForKey:@"duration"];
                
                NSString *dateStrParse = [AppSupport dateParser:dateStr];
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                [dic setValue:imageUrl forKey:@"THUMB_URL"];
                [dic setValue:name forKey:@"TITLE"];
                [dic setValue:link forKey:@"LINK"];
                [dic setValue:dateStrParse forKey:@"DATE"];
                [dic setValue:durationStr forKey:@"DURATION"];
                
                
                [videoArray addObject:dic];
            }
            
            [_tableView reloadData];
            
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

-(void)getArticleDetails:(NSInteger)pageNumber{
    
    NSString *pageNO = [NSString stringWithFormat:@"%ld",(long)pageNumber];
    NSMutableDictionary *articleDic = [[NSMutableDictionary alloc] init];
    [articleDic setValue:pageNO forKey:@"PAGE_NO"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getArticleInfo:articleDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"results"]) {
            
         
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextArticlePageAvailable = YES;
                articleCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextArticlePageAvailable = NO;
            }
            
            NSMutableArray *resultArray = [[result valueForKey:@"results"] mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSMutableDictionary *articleDic = [resultArray objectAtIndex:i];
                
                NSURL *imageUrl = [NSURL URLWithString:[articleDic valueForKey:@"image"]];
                NSURL *link = [NSURL URLWithString:[articleDic valueForKey:@"link"]];
                NSString *name = [articleDic valueForKey:@"title"];
                NSString *dateStr = [articleDic valueForKey:@"created_at"];
                
                NSString *dateStrParse = [AppSupport dateParser:dateStr];
             
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                [dic setValue:imageUrl forKey:@"THUMB_URL"];
                [dic setValue:name forKey:@"TITLE"];
                [dic setValue:link forKey:@"LINK"];
                [dic setValue:dateStrParse forKey:@"DATE"];
              
                [articleArray addObject:dic];
            }
            
            [_tableView reloadData];
            
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}




#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return loadVideoTableView ?  videoArray.count : articleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    VideoArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videoArticleCell"];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VideoArticleTableViewCell" owner:self options:nil];
        cell = (VideoArticleTableViewCell *)[topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    if (loadVideoTableView) {
        
        NSDictionary *dic = [videoArray objectAtIndex:indexPath.row];
        
        NSURL *thumbUrl = [dic valueForKey:@"THUMB_URL"];
        NSString *name = [dic valueForKey:@"TITLE"];
      
        
        cell.nameLabel.text = name;
        cell.dateLabel.text = [dic valueForKey:@"DATE"];
        cell.durationLabel.text = [NSString stringWithFormat:@"Duration: %@",[dic valueForKey:@"DURATION"]];
        
    
        [cell.coverImageView sd_setImageWithURL:thumbUrl placeholderImage:[UIImage imageNamed:@"defaultVideo-thumb"] options:SDWebImageRefreshCached];
        
        
        
        if (indexPath.row == videoArray.count-5) {
            if (DELEGATE.isReachable) {
                if (nextVideoPageAvailable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getVideoDetails:videoCurrentPage];
                    });
                }
            }
        }
        
        return cell;
        
        
    }else{
        
        NSDictionary *dic = [articleArray objectAtIndex:indexPath.row];
        
        NSURL *thumbUrl = [dic valueForKey:@"THUMB_URL"];
        NSString *name = [dic valueForKey:@"TITLE"];
        
        cell.nameLabel.text = name;
        cell.dateLabel.text = @"";
        cell.durationLabel.text = [dic valueForKey:@"DATE"];
        
    
        [cell.coverImageView sd_setImageWithURL:thumbUrl placeholderImage:[UIImage imageNamed:@"defaultArticle-thumb"] options:SDWebImageRefreshCached ];
        
        if (indexPath.row == articleArray.count-5) {
            if (DELEGATE.isReachable) {
                if (nextArticlePageAvailable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getArticleDetails:articleCurrentPage];
                    });
                }
            }
        }
        
        return cell;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 90: 81;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary *dic = loadVideoTableView ? [videoArray objectAtIndex:indexPath.row] : [articleArray objectAtIndex:indexPath.row];
    
    NSURL *url = [dic valueForKey:@"LINK"];
    
    [[UIApplication sharedApplication] openURL:url];

}

- (UIImage *)cropImageWithImage:(UIImage *)image {
    
    //62, 1.21
    
    double refWidth = CGImageGetWidth(image.CGImage);
    double refHeight = CGImageGetHeight(image.CGImage);
    
    double x=0,y=0,h=0,w=0;
    
    
    x = 0;
    y = 10.5;
    w = refWidth;
    h = refHeight-21;
    
    CGRect cropRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return croppedImage;
}
- (UIImage *)cropSquareImage:(UIImage *)image {
    
    //62, 1.21
    
    double refWidth = CGImageGetWidth(image.CGImage);
    double refHeight = CGImageGetHeight(image.CGImage);
    
    double x=0,y=0,h=0,w=0;
    
    if (refWidth>=refHeight) {
        
        
        x = (refWidth/2.0) - (refHeight/2.0);
        y = 0;
        w = refHeight;
        h = refHeight;
        
        
    }else{
        
        y = (refHeight/2.0) - (refWidth/2.0);
        x = 0;
        w = refWidth;
        h = refWidth;
    }
    
    
    CGRect cropRect = CGRectMake(x, y, w, h);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return croppedImage;
}

- (UIImage *)resizeImageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


#pragma mark - Banner

-(void)setBanner:(NSURL *)url{
    
    [_bannerAdsView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"ads"] options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType caheType, NSURL *imageUrl){
        
        if (image && caheType == SDImageCacheTypeNone)
            
        {
            [_bannerAdsView setImage:image];
            _bannerAdsView.alpha = 0.0;
            [UIView animateWithDuration:1.0 animations:^{
                [_bannerAdsView setAlpha:1.0];
            }];
        }
    }];
}
@end


