//
//  UnlikeViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "UnlikeViewController.h"
#import "MatchingTableViewCell.h"

@interface UnlikeViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
  
    
    NSMutableArray *array;
    
    NSString *unlikeReason;
    NSString *unlikeDescription;

}

@end

@implementation UnlikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(keyBoardHide)],
                           nil];
    [numberToolbar sizeToFit];
    _textView.inputAccessoryView = numberToolbar;
    _textView.delegate = self;
  
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self loadData];
    
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {

        CGRect labelFrame = _titleLabel.frame;
        labelFrame.origin.y = 85;
        _titleLabel.frame = labelFrame;
    }
    if (IS_IPHONE_5) {
        
        CGRect labelFrame = _titleLabel.frame;
        labelFrame.origin.y = 50;
        _titleLabel.frame = labelFrame;
        _titleLabel.font = [UIFont fontWithName:@"roboto" size:14];
    }
    if (IS_IPHONE_6) {
        
        CGRect labelFrame = _titleLabel.frame;
        labelFrame.origin.y = 65;
        _titleLabel.frame = labelFrame;
        _titleLabel.font = [UIFont fontWithName:@"roboto" size:14];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    unlikeDescription = textView.text;
    if ([unlikeDescription isEqualToString:@"Aa"]) {
        unlikeDescription = @"";
    }
}



-(void)keyBoardHide{
    
   [_textView resignFirstResponder];
    
   
}

-(void)loadData{
    
    
    
    array = [[NSMutableArray alloc] init];
    
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getUnlikeReasonWithInfoWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if (result.count>1) {
            
            NSArray *reasonArray = [result mutableCopy];
            
        
            for (int i = 0 ; i<reasonArray.count; i++) {
                
                NSDictionary *dic = [reasonArray objectAtIndex:i];
                
                NSString *reasonId = [dic valueForKey:@"id"];
                NSString *reason = [dic valueForKey:@"reason"];
              
                
             
                
                NSMutableDictionary *resondic = [[NSMutableDictionary alloc] init];
                
                [resondic setValue:reasonId forKey:@"ID_DETAILS"];
                [resondic setValue:reason forKey:@"REASON_DETAILS"];
                
                [array addObject:dic];
            }
            
            [_tableView reloadData];
            
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
    
   
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return array.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"matchingCell"];
    
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MatchingTableViewCell" owner:self options:nil];
        cell = (MatchingTableViewCell *)[nib objectAtIndex:0];
    }
    
    NSDictionary *dic = [array objectAtIndex:indexPath.row];
    
    cell.titlelabel.text = [dic valueForKey:@"reason"];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MatchingTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    for (int i =0; i<array.count; i++) {
        
        NSIndexPath *customIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        MatchingTableViewCell *cell = [tableView cellForRowAtIndexPath:customIndexPath];
        [cell.button setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
    }
    
    [cell.button setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
    
    unlikeReason = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 45 :( IS_IPHONE_6S_PLUS ? 30 : 26);
}


#pragma mark -
#pragma mark - Button Action

-(IBAction)cancelButtonAction:(UIButton *)button{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)confirmButtonAction:(UIButton *)button{
    
    if (unlikeReason.length==0) {
        
        [DELEGATE makeFillupPopUp:@"Please Choose One"];
        return;
    }
    
    NSString *userId = [_userDic valueForKey:@"ID"];

    NSMutableDictionary *unlikeDic = [[NSMutableDictionary alloc] init];
    [unlikeDic setValue:unlikeDescription forKey:@"unlike_description"];
    [unlikeDic setValue:unlikeReason forKey:@"unlike_reason"];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] dislikeOtherProfileWithInfo:unlikeDic andID:userId WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            [self.navigationController popViewControllerAnimated:YES];
           
        }
        else if ([result valueForKey:@"non_field_errors"]){
            
            [DELEGATE makeFillupPopUp:@"Insufficient points. Please purchase more"];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
   
}

@end
