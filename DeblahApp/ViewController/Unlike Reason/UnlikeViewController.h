//
//  UnlikeViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnlikeViewController : UIViewController

@property (nonatomic, retain)  NSDictionary *userDic;
@property   (nonatomic, retain) NSString *fromVC;
@property   NSInteger indexPathRow;

@property (nonatomic,weak) IBOutlet UITableView *tableView;

@property (nonatomic,weak) IBOutlet UILabel *titleLabel;
@property (nonatomic,weak) IBOutlet UITextView *textView;


@end
