//
//  LoginViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/22/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentTabViewController.h"


@interface LoginViewController : UIViewController

@property (nonatomic, strong) ParentTabViewController *parentTabViewController;

@property (nonatomic, weak) IBOutlet UIView *mainView;


@property (nonatomic, weak) IBOutlet UIImageView *titleImageView;
@property (nonatomic, weak) IBOutlet UIImageView *logoImageView;


@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;

@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) IBOutlet UIButton *forgetButton;
@property (nonatomic, weak) IBOutlet UIButton *signUpButton;

@end
