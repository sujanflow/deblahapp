//
//  LoginViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/22/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "ForgetPasswordViewController.h"

#import "SoulmateInfoViewController.h"
#import "MatchingStepOneViewController.h"
#import "APIClient.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
    UITapGestureRecognizer *tapGesture;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHide:)];
    [self.view addGestureRecognizer:tapGesture];
    
    _emailTextField.delegate = self;
    _passwordTextField.delegate = self;
    
   
}


-(void)viewWillAppear:(BOOL)animated{
 
    _emailTextField.text=@"feeza@khan.com";
    _passwordTextField.text=@"123456";
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark - Button Action

-(BOOL)inputValidate{
    
    if ( _emailTextField.text.length == 0 || _passwordTextField.text.length == 0) {
        [DELEGATE makeFillupPopUp:@"Fill up TextField"];
        return NO;
    }
    
    if ([AppSupport mailAuthentication:_emailTextField.text] == NO) {
        [DELEGATE makeFillupPopUp:@"Invalide Mail"];
        return NO;
    }
    
    return YES;
}

-(IBAction)loginButtonAction:(UIButton *)button{
    
     [self keyBoardHide:nil];
    
    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }
    
    if ([self inputValidate]) {
        
        NSMutableDictionary *loginDic = [[NSMutableDictionary alloc] init];
        [loginDic setValue:_emailTextField.text forKey:@"email"];
        [loginDic setValue:_passwordTextField.text forKey:@"password"];
        
        [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
        
        [[ APIClient sharedInstance] loginWithInfo:loginDic WithComplisionBlock:^(NSDictionary *result) {
            
            if ([result valueForKey:@"token"]) {
                
               
                [[NSUserDefaults standardUserDefaults] setValue:[result valueForKey:@"token"] forKey:@"TOKEN"];
                [[NSUserDefaults standardUserDefaults] setValue:_emailTextField.text forKey:@"MAIL_ID"];
                [[NSUserDefaults standardUserDefaults] setValue:_passwordTextField.text forKey:@"PASSWORD"];

                
                [self getUserDetailsInfo];
               
            }else{
                [DELEGATE makeFillupPopUp:@"Invalid Mail or Password"];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
            });
            
        }];
        
    }
}



-(IBAction)forgetButtonAction:(UIButton *)button{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    ForgetPasswordViewController *forgetVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"ForgetPasswordViewController"];
    
    [self.navigationController pushViewController:forgetVC animated:YES];
    
}
-(IBAction)signUpButtonAction:(UIButton *)button{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    SignUpViewController *signupVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"SignUpViewController"];
    
    [self.navigationController pushViewController:signupVC animated:YES];
    
}

#pragma mark-
#pragma mark- TextField Delegate

-(void)keyBoardHide:(UITapGestureRecognizer*)sender{
    
     [_emailTextField resignFirstResponder];
     [_passwordTextField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
     [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
      [self animateTextField:textField up:NO];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.mainView.frame = CGRectOffset(self.mainView.frame, 0, movement);
    [UIView commitAnimations];
}



#pragma mark - cheak



-(void)getUserDetailsInfo{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getUserDetailsWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"getUserDetailsInfo %@",result);
        
        NSString * userId =  [NSString stringWithFormat:@"%d",[[result objectForKey:@"id"]intValue]];
        
        NSLog(@"user id in getUserDetailsInfo %@",userId);
        [self registerFCMToken:userId];

        
        if ([result valueForKey:@"id"]) {
            
         
            NSNumber *profileComplete = [result valueForKey:@"profile_complete"];
            NSNumber *soulmateComplete = [result valueForKey:@"soulmate_complete"];
            NSNumber *quizComplete = [result valueForKey:@"quiz_complete"];

            
            if ([quizComplete boolValue] == YES && [soulmateComplete boolValue] == YES && [profileComplete boolValue] == YES) {
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DID_COMPLETE_LOGING"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
                self.parentTabViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"ParentTabViewController"];
                
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.parentTabViewController];
                [self presentViewController:navController animated:YES completion:nil];
                
                
            }else if([soulmateComplete boolValue] == YES){
                
                [DELEGATE makeFillupPopUp:@"Quiz not Completed"];
                [self gotoQuizVC];
            }
            else if([profileComplete boolValue] == YES){
                
                [DELEGATE makeFillupPopUp:@"Soumate Requirement not Completed"];
                [self gotoSoulmateVC];
            }else{
               
                [DELEGATE makeFillupPopUp:@"Profile not Completed"];
                [self signUpButtonAction:nil];
            }
            
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}

- (void) registerFCMToken:(NSString*)forUser{
    
    NSLog(@"registrationToken %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"registrationToken"]);
    NSLog(@"deviceToken in login.. : %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]);
    
    NSMutableDictionary *info=[[NSMutableDictionary alloc] init];
    
    [info setObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"registrationToken"] forKey:@"registration_id"];
    [info setObject:[NSNumber numberWithBool:YES] forKey:@"active"];
    [info setObject:@"ios" forKey:@"type"];
    
    [[APIClient sharedInstance] postRegisterFCMToken:info Foruser:forUser WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"post message result %@",result);
        

        
    }];
    
}


-(void)gotoSoulmateVC{
   
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    SoulmateInfoViewController *soulmateVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"SoulmateInfoViewController"];
    
    [self.navigationController pushViewController:soulmateVC animated:YES];
}
-(void)gotoQuizVC{
  
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    MatchingStepOneViewController *matchingOneVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"MatchingStepOneViewController"];
    
    [self.navigationController pushViewController:matchingOneVC animated:YES];
}

@end
