//
//  NewPasswordViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPasswordViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *mainView;


@property (nonatomic, weak) IBOutlet UITextField *pincodeTextField;

@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UITextField *confirmPasswordTextField;

@end
