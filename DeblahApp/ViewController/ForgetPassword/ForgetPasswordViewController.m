//
//  ForgetPasswordViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "NewPasswordViewController.h"

@interface ForgetPasswordViewController ()<UITextFieldDelegate>
{
     UITapGestureRecognizer *tapGesture;
}
@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHide:)];
    [self.view addGestureRecognizer:tapGesture];
    
    _mailTextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-
#pragma mark- Button Action

-(BOOL)inputValidate{
    
    if ( _mailTextField.text.length == 0) {
        [DELEGATE makeFillupPopUp:@"Fill up TextField"];
        return NO;
    }
    
    if ([AppSupport mailAuthentication:_mailTextField.text] == NO) {
        [DELEGATE makeFillupPopUp:@"Invalide Mail"];
        return NO;
    }
    
   
    
    return YES;
}

-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)nextButtonAction:(UIButton *)button{
    
    [self keyBoardHide:nil];
    
    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }
    
    if ([self inputValidate]) {
        
        NSMutableDictionary *forDic = [[NSMutableDictionary alloc] init];
        [forDic setValue:_mailTextField.text forKey:@"email"];
     
        
        [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
        
        [[ APIClient sharedInstance] forgetPasswordWithInfo:forDic WithComplisionBlock:^(NSDictionary *result) {
            
            if ([result valueForKey:@"email"]) {
                
                NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
                NewPasswordViewController *newVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"NewPasswordViewController"];
                [self.navigationController pushViewController:newVC animated:YES];
                
            }else{
                
                [DELEGATE makeFillupPopUp:@"Error"];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
            });
            
        }];
        
    }
    
}

#pragma mark-
#pragma mark- TextField Delegate

-(void)keyBoardHide:(UITapGestureRecognizer*)sender{
    
    [_mailTextField resignFirstResponder];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self animateTextField:textField up:NO];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.mainView.frame = CGRectOffset(self.mainView.frame, 0, movement);
    [UIView commitAnimations];
}
@end
