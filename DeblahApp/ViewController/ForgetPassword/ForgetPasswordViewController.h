//
//  ForgetPasswordViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPasswordViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *mainView;

@property (nonatomic, weak) IBOutlet UITextField *mailTextField;


@end
