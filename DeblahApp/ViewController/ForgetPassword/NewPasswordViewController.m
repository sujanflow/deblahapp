//
//  NewPasswordViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "NewPasswordViewController.h"

@interface NewPasswordViewController ()<UITextFieldDelegate>
{
    UITapGestureRecognizer *tapGesture;
}
@end

@implementation NewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationController.navigationBar.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHide:)];
    [self.view addGestureRecognizer:tapGesture];
    
    _passwordTextField.delegate = self;
    _confirmPasswordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-
#pragma mark- Button Action


-(BOOL)inputValidate{
    
    if ( _passwordTextField.text.length == 0 || _confirmPasswordTextField.text.length == 0 || _pincodeTextField.text.length == 0) {
        [DELEGATE makeFillupPopUp:@"Fill up TextField"];
        return NO;
    }
    
    if (![_passwordTextField.text isEqualToString:_confirmPasswordTextField.text]) {
        [DELEGATE makeFillupPopUp:@"No Password Match"];
        return NO;
    }
    

    return YES;
}

-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)doneButtonAction:(UIButton *)button{
    
    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }
    
    if ([self inputValidate]) {
        
        NSMutableDictionary *forDic = [[NSMutableDictionary alloc] init];
        [forDic setValue:_passwordTextField.text forKey:@"password"];
        [forDic setValue:_confirmPasswordTextField.text forKey:@"confirm_password"];
        [forDic setValue:_pincodeTextField.text forKey:@"code"];
      
        
        [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
        
        [[ APIClient sharedInstance] resetForgetPasswordWithInfo:forDic WithComplisionBlock:^(NSDictionary *result) {
            
            if ([result valueForKey:@"non_field_errors"]) {
                
                 [DELEGATE makeFillupPopUp:@"Code is incorrect"];
                
            }
            else if ([result valueForKey:@"id"]){ // should be changed
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            else{
               [DELEGATE makeFillupPopUp:@"Error"];
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
            });
            
        }];
        
    }
    
}

#pragma mark-
#pragma mark- TextField Delegate

-(void)keyBoardHide:(UITapGestureRecognizer*)sender{
    
  
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
    [_pincodeTextField resignFirstResponder];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
   // [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
  //  [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.mainView.frame = CGRectOffset(self.mainView.frame, 0, movement);
    [UIView commitAnimations];
}
@end
