//
//  SettingsViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsTableViewCell.h"
#import "AddCardTableViewCell.h"

#import "PackageViewController.h"

@interface SettingsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *line1;
    UILabel *line2;
    
    NSMutableArray *totalCardArray;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LANGUAGE"]) {
        [self englishAction:nil];
//    }else{
//         [self arabicAction:nil];
//    }
    
   
    
 
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    NSString * currentL = LocalizationGetLanguage;
    NSLog(@"%@",currentL);
    
    [self updateUI];
   
}

-(void)updateUI{
    
    [self.accountButton setTitle:AMLocalizedString(@"My Account",nil) forState:UIControlStateNormal];
}


-(void)viewWillLayoutSubviews{
    
    self.myAccountView.clipsToBounds = YES;
    self.myAccountView.layer.cornerRadius = 8;
    
    
    self.languageView.clipsToBounds = YES;
    self.languageView.layer.cornerRadius = 8;
    
    self.logoutView.clipsToBounds = YES;
    self.logoutView.layer.cornerRadius = 8;
    
    
    if (line1 != nil) {
        [line1 removeFromSuperview];
    }
    if (line2 != nil) {
        [line2 removeFromSuperview];
    }
    
    line1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 0.3)];
    line1.backgroundColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1];
    [_arabicView addSubview:line1];
    
    
    line2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 0.3)];
    line2.backgroundColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1];
    [_englishView addSubview:line2];
    
    
    if (IS_IPHONE_X) {
        CGRect mainFrame = _mainContentView.frame;
        mainFrame.origin.y = 24;
        _mainContentView.frame = mainFrame;
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - My Account

-(IBAction)myAccountAction:(UIButton *)button{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    PackageViewController *packageVC = (PackageViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"PackageViewController"];
    
    [self.navigationController pushViewController:packageVC animated:YES];
    
}


#pragma mark - Language

-(IBAction)englishAction:(id)sender{
    
    _englishTik.hidden = NO;
    _englishCircle.image = [UIImage imageNamed:@"language-on"];
    
    _arabicTik.hidden = YES;
    _arabicCircle.image = [UIImage imageNamed:@"language-off"];
    

     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LANGUAGE"];
     [[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"English", nil] forKey:@"AppleLanguages"];
     [[NSUserDefaults standardUserDefaults] synchronize];
    
     LocalizationSetLanguage(@"en");

     [self viewWillAppear:NO];
}

-(IBAction)arabicAction:(id)sender{
    
    _arabicTik.hidden = NO;
    _arabicCircle.image = [UIImage imageNamed:@"language-on"];
    
    _englishTik.hidden = YES;
    _englishCircle.image = [UIImage imageNamed:@"language-off"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LANGUAGE"];
    [[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"Arabic", nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  
    LocalizationSetLanguage(@"ar");
    
    [self viewWillAppear:NO];
    
   
}


#pragma mark - Logout

-(IBAction)logoutAction:(id)sender{
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[ APIClient sharedInstance] logOutWithComplisionBlock:^(NSDictionary *result) {
        
        
        if ([[result valueForKey:@"detail"] isEqualToString:@"Successfully logged out."]) {
            
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"TOKEN"];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"DID_COMPLETE_LOGING"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            
            
            [DELEGATE makeLoginViewControllerAsRootViewController];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOVE_TABBAR" object:nil];
            
            
            
            //have to logout from all devices
            
        }
            
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

@end
