//
//  SettingsViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property(nonatomic, weak) IBOutlet UIView *mainContentView;


@property(nonatomic, weak) IBOutlet UIView *myAccountView;
@property(nonatomic, weak) IBOutlet UIButton *accountButton;



@property(nonatomic, weak) IBOutlet UIView *languageView;

@property(nonatomic, weak) IBOutlet UIView *englishView;
@property(nonatomic, weak) IBOutlet UIImageView *englishCircle;
@property(nonatomic, weak) IBOutlet UIImageView *englishTik;

@property(nonatomic, weak) IBOutlet UIView *arabicView;
@property(nonatomic, weak) IBOutlet UIImageView *arabicCircle;
@property(nonatomic, weak) IBOutlet UIImageView *arabicTik;


@property(nonatomic, weak) IBOutlet UIView *logoutView;
@property(nonatomic, weak) IBOutlet UIButton *logoutButton;

@end
