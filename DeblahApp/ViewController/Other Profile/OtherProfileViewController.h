//
//  OtherProfileViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherProfileViewController : UIViewController

@property (nonatomic, retain)  NSDictionary *userDic;
@property   (nonatomic, retain) NSString *fromVC;
@property   NSInteger indexPathRow;



@property (nonatomic, weak) IBOutlet UIView *mainContentView;

@property (nonatomic, weak) IBOutlet UIView *photoView;
@property (nonatomic, weak) IBOutlet UIImageView *coverImageVIew;
@property (nonatomic, weak) IBOutlet UIButton *likeUnlikeButton;



@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageLabel;
@property (nonatomic, weak) IBOutlet UIButton *chatButton;

@property (nonatomic, weak) IBOutlet UITextView *textView;

@property (nonatomic, weak) IBOutlet UILabel *basicInfoLabel;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
