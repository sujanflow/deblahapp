//
//  OtherProfileViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "OtherProfileViewController.h"
#import "ProfileTableViewCell.h"
#import "UnlikeViewController.h"

#import "LiveChatViewController.h"

@interface OtherProfileViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *descriptionArray;
    NSMutableDictionary *descriptionDictionary;
    
   
    NSString *phoneNumber;
    NSString *occupation;

}
@end

@implementation OtherProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];
    self.navigationController.navigationBar.hidden = YES;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionHeaderHeight = 40;
    
    descriptionArray = [[NSMutableArray alloc] init];
    
    _basicInfoLabel.text = AMLocalizedString(@"Basic Information",nil) ;
    
  
}

-(void)viewWillAppear:(BOOL)animated{
    
      [self getDataFromAPI];
    
//    if ([_fromVC isEqualToString:@"HOME"]) {
//          self.userDic = [DELEGATE.soulmateArray objectAtIndex:self.indexPathRow];
//    }
//    else if ([_fromVC isEqualToString:@"RECEIVED"]) {
//        //self.userDic = [DELEGATE.soulmateArray objectAtIndex:self.indexPathRow];
//    }
//    else if ([_fromVC isEqualToString:@"SENT"]) {
//        //self.userDic = [DELEGATE.soulmateArray objectAtIndex:self.indexPathRow];
//    }
//
//

    
}

-(void)viewDidAppear:(BOOL)animated{
 
    
    CGRect rect = self.mainContentView.bounds;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect
                                               byRoundingCorners:UIRectCornerTopLeft |UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *layers = [CAShapeLayer layer];
    layers.frame = rect;
    layers.path = path.CGPath;
    self.mainContentView.layer.mask = layers;
    
    self.coverImageVIew.clipsToBounds = YES;
    self.coverImageVIew.layer.cornerRadius = 15;
    
    [self makeShadowLeft];
    [self makeShadowRight];
    
    
}


-(void)viewWillDisappear:(BOOL)animated{
   
  
    
}

-(void)viewWillLayoutSubviews{
    
    [self.textView setContentOffset:CGPointZero animated:NO];
    
    if (IS_IPHONE_X) {
        CGRect mainFrame = _mainContentView.frame;
        mainFrame.origin.y = 81+18;
        mainFrame.size.height = DEVICE_HEIGHT-81-18-49-33-20;
        _mainContentView.frame = mainFrame;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)makeShadowLeft{
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(_mainContentView.frame.origin.x-1, _mainContentView.frame.origin.y+15, 1, _mainContentView.frame.size.height-140)];
    leftView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:leftView];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(-5, 0, 0, 0);
    CGRect shadowPath = UIEdgeInsetsInsetRect(leftView.bounds, contentInsets);
    leftView.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    leftView.layer.shadowColor = [[UIColor redColor] CGColor];
    leftView.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    leftView.layer.shadowOpacity = 0.4f;
    leftView.layer.masksToBounds=NO;
}

-(void)makeShadowRight{
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(_mainContentView.frame.origin.x+_mainContentView.frame.size.width+1, _mainContentView.frame.origin.y+15, 1, _mainContentView.frame.size.height-140)];
    rightView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:rightView];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(-5, 0, 0, 0);
    CGRect shadowPath = UIEdgeInsetsInsetRect(rightView.bounds, contentInsets);
    rightView.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    rightView.layer.shadowColor = [[UIColor redColor] CGColor];
    rightView.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    rightView.layer.shadowOpacity = 0.4f;
    rightView.layer.masksToBounds=NO;
}


-(void)getDataFromAPI{
    
    NSString *userId = [_userDic valueForKey:@"ID"];
    [self getOtherUserDetails:userId];
    
}

-(void)getOtherUserDetails:(NSString *)userID{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getOtherProfileDetailsWithInfo:userID WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            NSNumber *profileComplete = [result valueForKey:@"profile_complete"];
            NSNumber *soulmateComplete = [result valueForKey:@"soulmate_complete"];
            NSNumber *quizComplete = [result valueForKey:@"quiz_complete"];
            
            [self likeDislikeParse:result];
            
            if ([profileComplete boolValue] && [soulmateComplete boolValue] && [quizComplete boolValue] ) {
                [self getInitialDataForUser:result];
            }else{
                
                [DELEGATE makeFillupPopUp:@"Profile Not Completed"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
                });
                
                return ;
            }
            
            //name
            NSString *name = [NSString stringWithFormat:@"%@ %@",[result valueForKey:@"first_name"] ,[result valueForKey:@"last_name"]];
            _nameLabel.text = name;
            
            //birthday
            NSString *age = [result valueForKey:@"birthday"];
            NSString *year = [AppSupport getTotalAge:age];
            _ageLabel.text = [NSString stringWithFormat:@"Age %@",year];
            
            //phone
            phoneNumber = [result valueForKey:@"phone_number"] ;
            //occupation
            occupation = [result valueForKey:@"occupation"] ;
          
            
           //image
            NSURL *imageUrl = [NSURL URLWithString:[result valueForKey:@"profile_image"]];
            [self setCoverPhoto:imageUrl];
            
            
           
           
            
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}


-(void)getInitialDataForUser:(NSDictionary *)userResult{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getBasicDropDownWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        if (result.count>1) {
            [self parseDataForTableView:result andUserResult:userResult];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}


-(void)parseDataForTableView:(NSDictionary *)result andUserResult:(NSDictionary *)userResult{
    
    NSMutableArray *optionBeautyArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionBloodArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionEducationArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionGenderArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionLivingArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionReligionArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionSkinArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionTribeArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *optionHeightArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionWeightArray = [[NSMutableArray alloc] init];
    
    
    NSArray *beautyArray = [result valueForKey:@"beauty_level_option"];
    for (int i = 0; i<beautyArray.count; i++) {
        NSArray *array = [beautyArray objectAtIndex:i];
        [optionBeautyArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *bloodArray = [result valueForKey:@"blood_group_option"];
    for (int i = 0; i<bloodArray.count; i++) {
        NSArray *array = [bloodArray objectAtIndex:i];
        [optionBloodArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *educationArray = [result valueForKey:@"education_option"];
    for (int i = 0; i<educationArray.count; i++) {
        NSArray *array = [educationArray objectAtIndex:i];
        [optionEducationArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *genderArray = [result valueForKey:@"gender_option"];
    for (int i = 0; i<genderArray.count; i++) {
        NSArray *array = [genderArray objectAtIndex:i];
        [optionGenderArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *livingArray = [result valueForKey:@"living_status_option"];
    for (int i = 0; i<livingArray.count; i++) {
        NSArray *array = [livingArray objectAtIndex:i];
        [optionLivingArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *religionArray = [result valueForKey:@"religion_option"];
    for (int i = 0; i<religionArray.count; i++) {
        NSArray *array = [religionArray objectAtIndex:i];
        [optionReligionArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *skinArray = [result valueForKey:@"skin_color_option"];
    for (int i = 0; i<skinArray.count; i++) {
        NSArray *array = [skinArray objectAtIndex:i];
        [optionSkinArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *tribeArray = [result valueForKey:@"tribe_type_option"];
    for (int i = 0; i<tribeArray.count; i++) {
        NSArray *array = [tribeArray objectAtIndex:i];
        [optionTribeArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *heightArray = [result valueForKey:@"height_range_option"];
    for (int i = 0; i<heightArray.count; i++) {
        NSArray *array = [heightArray objectAtIndex:i];
        [optionHeightArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *weightArray = [result valueForKey:@"weight_range_option"];
    for (int i = 0; i<weightArray.count; i++) {
        NSArray *array = [weightArray objectAtIndex:i];
        [optionWeightArray addObject: [array objectAtIndex:1]];
    }
    

    
   
    
    //---basic info ---
    
    //gender
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    NSString *description = @"Gender";
    NSString *strValue = [userResult valueForKey:@"gender"];
    NSInteger index = [strValue integerValue];
    NSString *valueStr = @"";
    
    valueStr = [optionGenderArray objectAtIndex:index] ;
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    [descriptionArray addObject:dic];
    
    
    
    //religion
    dic = [[NSMutableDictionary alloc] init];
    description = @"Religion";
    strValue = [userResult valueForKey:@"religion"];
    index = [strValue integerValue];
    valueStr = @"";
    
    valueStr = [optionReligionArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //phone_number
    dic = [[NSMutableDictionary alloc] init];
    description = @"Phone No";
    valueStr = phoneNumber;
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //blood_group
    dic = [[NSMutableDictionary alloc] init];
    description = @"Blood Group";
    strValue = [userResult valueForKey:@"blood_group"];
    index = [strValue integerValue];
    valueStr = @"";
    valueStr = [optionBloodArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //occupation
    dic = [[NSMutableDictionary alloc] init];
    description = @"Occupation";
    valueStr = occupation;
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //height
    dic = [[NSMutableDictionary alloc] init];
    description = @"Height";
    
    strValue = [userResult valueForKey:@"height"];
    index = [strValue integerValue];
    valueStr = [optionHeightArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //weight
    dic = [[NSMutableDictionary alloc] init];
    description = @"Weight";
    
    strValue = [userResult valueForKey:@"weight"];
    index = [strValue integerValue];
    valueStr = [optionWeightArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //beauty_level
    dic = [[NSMutableDictionary alloc] init];
    description = @"Beauty Level";
    strValue =  [userResult valueForKey:@"beauty_level"];
    index = [strValue integerValue];
    valueStr = [optionBeautyArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //skin_color
    dic = [[NSMutableDictionary alloc] init];
    description = @"Skin Color";
    strValue = [userResult valueForKey:@"skin_color"];
    index = [strValue integerValue];
    valueStr = [optionSkinArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //living_status
    dic = [[NSMutableDictionary alloc] init];
    description = @"Living Status";
    strValue =  [userResult valueForKey:@"living_status"];
    index = [strValue integerValue];
    valueStr = [optionLivingArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //tribe_type
    dic = [[NSMutableDictionary alloc] init];
    description = @"Tribe Type";
    strValue =  [userResult valueForKey:@"tribe_type"];
    index = [strValue integerValue];
    valueStr = [optionTribeArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //education
    dic = [[NSMutableDictionary alloc] init];
    description = @"Education";
    strValue = [userResult valueForKey:@"education"];
    index = [strValue integerValue];
    valueStr = [optionEducationArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    
    
    [_tableView reloadData];
    
    
}

-(void)setCoverPhoto: (NSURL *)imageUrl{
    
     [self.coverImageVIew sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"home-default"] options:SDWebImageRefreshCached];
}




#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return descriptionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileTableViewCell" owner:self options:nil];
        cell = (ProfileTableViewCell *)[topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [descriptionArray objectAtIndex:indexPath.row];
    
    NSString *description = [dic valueForKey:@"PROFILE_DESCRIPTION"];
    NSString *value = [dic valueForKey:@"PROFILE_VALUE"];
    
    
    cell.firstLabel.text = AMLocalizedString(description,nil);
    cell.secondLabel.text = value;
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return IS_DEVICE_IPAD ? 40: 35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -
#pragma mark - Button Action

-(IBAction)likeUnlikeProfileButtonAction:(UIButton *)button{
    
    
    NSString *userID = [self.userDic valueForKey:@"ID"];
    
    if (button.tag == 0) {
        
        [self likeAction:userID];
        
    }else if (button.tag == 1){
        
         [self unlikeAction:self.userDic];
        
    }else if (button.tag == 2){
        
         [self unlikeAction:self.userDic];
    }
    
}

-(void)likeAction:(NSString *)userId{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] likeOtherProfileWithInfo:nil andID:userId WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
                _likeUnlikeButton.tag = 1;
                [_likeUnlikeButton setImage:[UIImage imageNamed:@"other-profile-like"] forState:UIControlStateNormal];
                _chatButton.hidden = YES;
                
                if ([[_userDic valueForKey:@"CHAT_REACTION"] isEqualToString:@"YES"]) {
                    _likeUnlikeButton.tag = 2;
                    [_likeUnlikeButton setImage:[UIImage imageNamed:@"other-profile-alllike"] forState:UIControlStateNormal];
                    _chatButton.hidden = NO;
                }
            
        }
        else if ([result valueForKey:@"non_field_errors"]){
            
            [DELEGATE makeFillupPopUp:@"Insufficient points. Please purchase more"];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

-(void)unlikeAction:(NSDictionary *)dic{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    UnlikeViewController *unlikeViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"UnlikeViewController"];
    unlikeViewController.userDic = dic;
    unlikeViewController.fromVC = self.fromVC;
    unlikeViewController.indexPathRow = self.indexPathRow;
    [self.navigationController pushViewController:unlikeViewController animated:YES];
}

-(IBAction)chatButtonAction:(UIButton *)button{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    LiveChatViewController *liveChatVC = (LiveChatViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"LiveChatViewController"];
    NSString *userID = [self.userDic valueForKey:@"ID"];
    liveChatVC.otherPersonID = userID;
    [self.navigationController pushViewController:liveChatVC animated:YES];
}


-(void)likeDislikeParse : (NSDictionary *)userDic{
    
    
    NSNumber *userLike = [userDic valueForKey:@"liked"];
    NSInteger liked =  [userLike integerValue];
    NSString *like_Status = (liked == 0)? @"NO" : @"YES";
    
    NSNumber *userChat = [userDic valueForKey:@"chat"];
    NSInteger chated =  [userChat integerValue];
    NSString *chat_Status = (chated == 0)? @"NO" : @"YES";
    
    
        if ([like_Status isEqualToString:@"NO"]) {
    
            _likeUnlikeButton.tag = 0;
            [_likeUnlikeButton setImage:[UIImage imageNamed:@"other-profile-nolike"] forState:UIControlStateNormal];
            _chatButton.hidden = YES;
        }
    
        else if ([like_Status isEqualToString:@"YES"]) {
    
            _likeUnlikeButton.tag = 1;
            [_likeUnlikeButton setImage:[UIImage imageNamed:@"other-profile-like"] forState:UIControlStateNormal];
            _chatButton.hidden = YES;
    
            if ([chat_Status isEqualToString:@"YES"]) {
    
                _likeUnlikeButton.tag = 2;
                [_likeUnlikeButton setImage:[UIImage imageNamed:@"other-profile-alllike"] forState:UIControlStateNormal];
                _chatButton.hidden = NO;
            }
        }
    
}


@end
