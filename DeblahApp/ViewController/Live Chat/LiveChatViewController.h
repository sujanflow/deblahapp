//
//  LiveChatViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "ChatSendTableViewCell.h"
#import "ChatReceiveTableViewCell.h"

#import "EditViewController.h"


#import "JSQMessages.h"
#import "DemoModelData.h"
#import "NSUserDefaults+DemoSettings.h"



@class LiveChatViewController;

@protocol JSQDemoViewControllerDelegate <NSObject>
- (void)didDismissJSQDemoViewController:(LiveChatViewController *)vc;
@end




@interface LiveChatViewController : JSQMessagesViewController <UIActionSheetDelegate, JSQMessagesComposerTextViewPasteDelegate>


@property (strong, nonatomic) NSString  *otherPersonID;
@property (strong, nonatomic) NSString  *myID;


@property (weak, nonatomic) id<JSQDemoViewControllerDelegate> delegateModal;

@property (strong, nonatomic) DemoModelData *demoData;

- (void)receiveMessagePressed:(UIBarButtonItem *)sender;

- (void)closePressed:(UIBarButtonItem *)sender;


@end

