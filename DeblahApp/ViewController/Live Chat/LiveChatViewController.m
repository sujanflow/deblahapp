//
//  LiveChatViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/31/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "LiveChatViewController.h"

#import "JSQMessagesViewAccessoryButtonDelegate.h"

#import "AppSupport.h"
#import "NSDictionary+NullReplacement.h"
#import "VIPhotoView.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface LiveChatViewController ()<JSQMessagesViewAccessoryButtonDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    VIPhotoView *photoView;
    UIButton *crossButton;
    
    int pageIndex;
    int isPickImage;
    NSString* nextPage;
    
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    AVPlayerViewController *playerViewController;
    NSURL *outputFileURL;

}

@property (strong) UIView *popRecoedViewBG;
@property (strong) UIView *popView;

@property (strong,nonatomic) UIButton* recordPauseButton;
@property (strong,nonatomic) UIButton* playButton;
@property (strong,nonatomic) UIButton* stopButton;
@property (strong,nonatomic) UIButton* cancelRecordButton;
@property (strong,nonatomic) UIButton* sendRecordButton;

@end

@implementation LiveChatViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"JSQMessages";
    
    pageIndex=1;
    nextPage=@"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadChatview) name:@"updateSharedImage" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMessagedForRemoteNotification) name:@"updateMessages" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(crossButtonAction:) name:@"ShowInputToolbar" object:nil];
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
   
    //[self.inputToolbar setHidden:YES];
     //NSLog(@"inputToolbar in live %@",self.inputToolbar);
    
    
    /**
     *  Load up our fake data for the demo
     */
    self.demoData = [[DemoModelData alloc] init];
    
    
    /**
     *  Set up message accessory button delegate and configuration
     */
    self.collectionView.accessoryDelegate = self;
    
    /**
     *  You can set custom avatar sizes
     */
    if (![NSUserDefaults incomingAvatarSetting]) {
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    }
    
    if (![NSUserDefaults outgoingAvatarSetting]) {
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    }
    
    self.showLoadEarlierMessagesHeader = YES;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage]
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                             action:@selector(receiveMessagePressed:)];
//
    /**
     *  Register custom menu actions for cells.
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    
    
    /**
     *  OPT-IN: allow cells to be deleted
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    
    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
    
    [self getChatHistoryByPage];
    
}

-(void) reloadMessagedForRemoteNotification
{
    pageIndex=1;
    [self getChatHistoryByPage];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //after close button click in avplayerviewcontroller
    
      self.inputToolbar.hidden=NO;
    
    //
    
    if (self.delegateModal) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                              target:self
                                                                                              action:@selector(closePressed:)];
        
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
    self.collectionView.collectionViewLayout.springinessEnabled = [NSUserDefaults springinessSetting];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom menu actions for cells

- (void)didReceiveMenuWillShowNotification:(NSNotification *)notification
{
    /**
     *  Display custom menu actions for cells.
     */
    UIMenuController *menu = [notification object];
    menu.menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action" action:@selector(customAction:)] ];
    
    [super didReceiveMenuWillShowNotification:notification];
}



#pragma mark - Testing

- (void)pushMainViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:nc.topViewController animated:YES];
}

-(void)reloadChatview
{
    [self.collectionView reloadData];
}


#pragma mark - Actions

- (void)receiveMessagePressed:(UIBarButtonItem *)sender
{
    /**
     *  DEMO ONLY
     *
     *  The following is simply to simulate received messages for the demo.
     *  Do not actually do this.
     */
    
    
    /**
     *  Show the typing indicator to be shown
     */
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    
    /**
     *  Copy last sent message, this will be the new "received" message
     */
    JSQMessage *copyMessage = [[self.demoData.messages lastObject] copy];
    
    if (!copyMessage) {
        copyMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdJobs
                                          displayName:kJSQDemoAvatarDisplayNameJobs
                                                 text:@"First received!"];
    }
    
    /**
     *  Allow typing indicator to show
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSMutableArray *userIds = [[self.demoData.users allKeys] mutableCopy];
        [userIds removeObject:self.senderId];
        NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
        
        JSQMessage *newMessage = nil;
        id<JSQMessageMediaData> newMediaData = nil;
        id newMediaAttachmentCopy = nil;
        
        if (copyMessage.isMediaMessage) {
            /**
             *  Last message was a media message
             */
            id<JSQMessageMediaData> copyMediaData = copyMessage.media;
            
            if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                JSQPhotoMediaItem *photoItemCopy = [((JSQPhotoMediaItem *)copyMediaData) copy];
                photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [UIImage imageWithCGImage:photoItemCopy.image.CGImage];
                
                /**
                 *  Set image to nil to simulate "downloading" the image
                 *  and show the placeholder view
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                JSQLocationMediaItem *locationItemCopy = [((JSQLocationMediaItem *)copyMediaData) copy];
                locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [locationItemCopy.location copy];
                
                /**
                 *  Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                JSQVideoMediaItem *videoItemCopy = [((JSQVideoMediaItem *)copyMediaData) copy];
                videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
                
                /**
                 *  Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = NO;
                
                newMediaData = videoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQAudioMediaItem class]]) {
                JSQAudioMediaItem *audioItemCopy = [((JSQAudioMediaItem *)copyMediaData) copy];
                audioItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [audioItemCopy.audioData copy];
                
                /**
                 *  Reset audio item to simulate "downloading" the audio
                 */
                audioItemCopy.audioData = nil;
                
                newMediaData = audioItemCopy;
            }
            else {
                NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
            }
            
            newMessage = [JSQMessage messageWithSenderId:randomUserId
                                             displayName:self.demoData.users[randomUserId]
                                                   media:newMediaData];
        }
        else {
            /**
             *  Last message was a text message
             */
            newMessage = [JSQMessage messageWithSenderId:randomUserId
                                             displayName:self.demoData.users[randomUserId]
                                                    text:copyMessage.text];
        }
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishReceivingMessage`
         */
        
        // [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
        
        [self.demoData.messages addObject:newMessage];
        [self finishReceivingMessageAnimated:YES];
        
        
        if (newMessage.isMediaMessage) {
            /**
             *  Simulate "downloading" media
             */
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                /**
                 *  Media is "finished downloading", re-display visible cells
                 *
                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
                 *
                 *  Reload the specific item, or simply call `reloadData`
                 */
                
                if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                    ((JSQPhotoMediaItem *)newMediaData).image = newMediaAttachmentCopy;
                    [self.collectionView reloadData];
                }
                else if ([newMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                    [((JSQLocationMediaItem *)newMediaData)setLocation:newMediaAttachmentCopy withCompletionHandler:^{
                        [self.collectionView reloadData];
                    }];
                }
                else if ([newMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                    ((JSQVideoMediaItem *)newMediaData).fileURL = newMediaAttachmentCopy;
                    ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
                    [self.collectionView reloadData];
                }
                else if ([newMediaData isKindOfClass:[JSQAudioMediaItem class]]) {
                    ((JSQAudioMediaItem *)newMediaData).audioData = newMediaAttachmentCopy;
                    [self.collectionView reloadData];
                }
                else {
                    NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
                }
                
            });
        }
        
    });
}

- (void)closePressed:(UIBarButtonItem *)sender
{
    [self.delegateModal didDismissJSQDemoViewController:self];
}




#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    
    [self.demoData.messages addObject:message];
    
    [self sendMessages:text];
    
    [self finishSendingMessageAnimated:YES];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Media messages", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Send photo", nil), NSLocalizedString(@"Send video thumbnail", nil), NSLocalizedString(@"Send audio", nil), nil];
    
    sheet.tag = 100;
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
  //  NSLog(@"didDismissWithButtonIndex");
    
    if (actionSheet.tag == 100) {
        
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            [self.inputToolbar.contentView.textView becomeFirstResponder];
            return;
        }
        
        switch (buttonIndex) {
            case 0:
                
                isPickImage = 1;
                [self addPictureAction];
                break;
                
            case 1:
            {
                //video thumbnail
                
              //  [self.demoData addVideoMediaMessageWithThumbnail];
                
                
                self.inputToolbar.hidden=YES;
                [self.inputToolbar.contentView.textView resignFirstResponder];
                
                isPickImage = 0;
                
                // Present videos from which to choose
                UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
                videoPicker.delegate = self; // ensure you set the delegate so when a video is chosen the right method can be called

                videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
                // This code ensures only videos are shown to the end user
                videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];

                videoPicker.videoQuality = UIImagePickerControllerQualityTypeLow;

                [self presentViewController:videoPicker animated:YES completion:nil];
                
                NSLog(@"video");
                break;
                
            }
            case 2:
                //audio
                // [self.demoData addAudioMediaMessage];
//
//                [self setUpAudioRecoder];
//
//                [self audioRecord];
                [self audioChoosen];
                
                
                NSLog(@"audio");
                break;
                
            case 3:
                //[self.demoData addVideoMediaMessageWithThumbnail];
                break;
                
            case 4:
                break;
        }
        
        // [JSQSystemSoundPlayer jsq_playMessageSentSound];
        
        
        [self finishSendingMessageAnimated:YES];
        
    }else if (actionSheet.tag == 101){
        
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            [self.inputToolbar.contentView.textView becomeFirstResponder];
            return;
        }
        
        switch (buttonIndex) {
            case 0:
                //start recording
                [self startAudioRecord];
                [self audioRecordDoneActionSheet];
                
                break;
        }
    }else if (actionSheet.tag == 102){
        
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            [self.inputToolbar.contentView.textView becomeFirstResponder];
            
            [self audioRecordDone];
            
            return;
        }
        
        switch (buttonIndex) {
            case 0:
                //record done
                [self audioRecordDone];
                
                break;
            case 1:
                //play record
//                [self audioRecordDone];
//
//                [self playRecordedAudio];
//

                break;

        }
    }
    
}


-(void) audioChoosen
{
    [self.view endEditing:YES];
    
    self.popRecoedViewBG= [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.popRecoedViewBG.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.popRecoedViewBG];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgTap)];
    tap.numberOfTapsRequired = 1;
    [self.popRecoedViewBG addGestureRecognizer:tap];
    
    
    self.popView= [[UIView alloc] initWithFrame:(CGRectMake(self.view.bounds.size.width*0.15, self.view.bounds.size.height*0.4, self.view.bounds.size.width*0.70 , self.view.bounds.size.height*0.25))];
    self.popView.backgroundColor = [UIColor colorWithRed:0.52 green:0.49 blue:0.97 alpha:0.90];
    self.popView.layer.cornerRadius=5;
    
    [self.view addSubview: self.popView];
    [self.popRecoedViewBG setHidden:NO];
    
    // self.recordPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.recordPauseButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.recordPauseButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.recordPauseButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [self.recordPauseButton setImage:[UIImage imageNamed:@"record.png"] forState:UIControlStateNormal];
    //[self.recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    self.recordPauseButton.tintColor=[UIColor whiteColor];
    self.recordPauseButton.layer.cornerRadius=5;
    self.recordPauseButton.backgroundColor = [UIColor colorWithRed:0 green:0.0 blue:0.7 alpha:1.0];
    self.recordPauseButton.frame = CGRectMake( self.popView.bounds.size.width*0.15,  self.popView.bounds.size.height*0.7,  self.popView.bounds.size.width*0.30,  self.popView.bounds.size.height*0.15);
    
    // self.recordPauseButton.frame=CGRectMake(50, 50, 100, 100);
    [self.recordPauseButton addTarget:self action:@selector(recordPauseTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.popView addSubview:self.recordPauseButton];
    NSLog(@"self.recordPauseButton %@",self.recordPauseButton);
    
    self.stopButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.stopButton setImage:[UIImage imageNamed:@"stop.png"] forState:UIControlStateNormal];
    //[self.stopButton setTitle:@"Stop" forState:UIControlStateNormal];
    [self.stopButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.stopButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
    
    self.stopButton.tintColor=[UIColor whiteColor];
    self.stopButton.layer.cornerRadius=5;
    self.stopButton.backgroundColor = [UIColor colorWithRed:0.7 green:0.0 blue:0 alpha:1.0];
    self.stopButton.frame = CGRectMake( self.popView.bounds.size.width*0.55,  self.popView.bounds.size.height*0.7,  self.popView.bounds.size.width*0.30,  self.popView.bounds.size.height*0.15);
    
    [self.stopButton addTarget:self action:@selector(stopTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.popView addSubview:self.stopButton];
    
    //cancel record button
    self.cancelRecordButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.cancelRecordButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.cancelRecordButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.cancelRecordButton setTitle:@"Cancel" forState:UIControlStateNormal];
    self.cancelRecordButton.tintColor=[UIColor whiteColor];
    self.cancelRecordButton.layer.cornerRadius=5;
    self.cancelRecordButton.backgroundColor =[UIColor colorWithRed:0.7 green:0.0 blue:0 alpha:0.8] ;
    self.cancelRecordButton.frame = CGRectMake( self.popView.bounds.size.width*0.15,  self.popView.bounds.size.height*0.7,  self.popView.bounds.size.width*0.30,  self.popView.bounds.size.height*0.15);
    
    // self.recordPauseButton.frame=CGRectMake(50, 50, 100, 100);
    [self.cancelRecordButton addTarget:self action:@selector(cancelRecordTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.popView addSubview:self.cancelRecordButton];
    NSLog(@"self.recordPauseButton %@",self.cancelRecordButton);
    
    // send record button
    self.sendRecordButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.sendRecordButton setTitle:@"Send" forState:UIControlStateNormal];
    [self.sendRecordButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.sendRecordButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    self.sendRecordButton.tintColor=[UIColor whiteColor];
    self.sendRecordButton.layer.cornerRadius=5;
    self.sendRecordButton.backgroundColor = [UIColor colorWithRed:0 green:0.0 blue:0.7 alpha:0.8];
    self.sendRecordButton.frame = CGRectMake( self.popView.bounds.size.width*0.55,  self.popView.bounds.size.height*0.7,  self.popView.bounds.size.width*0.30,  self.popView.bounds.size.height*0.15);
    
    [self.sendRecordButton addTarget:self action:@selector(sendRecordTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.popView addSubview:self.sendRecordButton];
    
    //
    
    self.recordPauseButton.hidden = NO;
    self.stopButton.hidden = NO;

    self.cancelRecordButton.hidden = YES;
    self.sendRecordButton.hidden = YES;
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.popView.bounds.size.height*0.2, self.popView.bounds.size.width, self.popView.bounds.size.height*0.2)];
    
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text=@"Record audio";
    label.highlighted = YES;
    label.font = [UIFont fontWithName:@"Helvetica neue" size:20];
    [label setFont:[UIFont boldSystemFontOfSize:20]];
    [self.popView addSubview:label];
    
    [self recordButtontapped];
}
- (void) bgTap{
    //if (!recorder.recording) {
    NSLog(@"rev bg tapped");
    
    [self.popView removeFromSuperview];
    self.popView=nil;
    [self.popRecoedViewBG removeFromSuperview];
    self.popRecoedViewBG=nil;
    //}
}

-(void)recordButtontapped
{
    
    NSLog(@"in record button tapped");
    // Disable Stop/Play button when application launches
    [_stopButton setEnabled:NO];
    [_playButton setEnabled:NO];
    [_recordPauseButton setEnabled:YES];
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudio.m4a",
                               nil];
    outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    NSLog(@"url = %@", outputFileURL);
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
}

- (IBAction)recordPauseTapped:(id)sender {
    // Stop the audio player before recording
    //        if (player.playing) {
    //            [player stop];
    //        }
    NSLog(@"in record pause tapped");
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        NSLog(@"in pause");
        // Start recording
        [recorder record];
        [_recordPauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
        
    } else {
        
        // Pause recording
        [recorder pause];
        [_recordPauseButton setImage:[UIImage imageNamed:@"record.png"]forState:UIControlStateNormal];
    }
    
    [_stopButton setEnabled:YES];
    [_playButton setEnabled:NO];
}

- (IBAction)stopTapped:(id)sender {
    NSLog(@"in stop");
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    self.recordPauseButton.hidden = YES;
    self.stopButton.hidden = YES;
    self.cancelRecordButton.hidden = NO;
    self.sendRecordButton.hidden = NO;
    
}
- (IBAction)cancelRecordTapped:(id)sender{
    
      [self hideRecordPopover];
}

- (IBAction)sendRecordTapped:(id)sender{
    
    NSData *data = [NSData dataWithContentsOfURL: outputFileURL];
    outputFileURL=nil;

    JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:data];
    JSQMessage * message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:audioItem];
    
    [self.demoData.messages addObject:message];
    [self sendMessageWithMediaUrl:recorder.url andFileName:@"Audio.m4a" forFiletype:@"audio"];
    [self finishSendingMessageAnimated:YES];
    
    [self hideRecordPopover];
}

-(void) hideRecordPopover{
    
    outputFileURL=nil;
    
    [self.popView removeFromSuperview];
    self.popView=nil;
    [self.popRecoedViewBG removeFromSuperview];
    self.popRecoedViewBG=nil;

}


- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    
    [_recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    
    
    //NSLog(@"data %@",data);
    //    [_stopButton setEnabled:NO];
    //    [_playButton setEnabled:YES];
    //    NSLog(@"audio saved");
    //    [self.popView setHidden:YES];
    
    // self.inputView.textView.text= @"MyAudio.m4a";
    
    
    
}



//setup for audio record/////////

- (void) setUpAudioRecoder{
    
    
    // Set the audio file
    
    //first remove if exist
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    NSString* foofile = [documentsPath stringByAppendingPathComponent:@"MyAudioMemo.m4a"];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    if (fileExists) {
        
        NSLog(@"fileExists");
        NSError *error;
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:foofile error:&error];
        if (success) {
            NSLog(@" delete file -:%@ ",[error localizedDescription]);
        }
        else
        {
            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
        }
        
    }else{
        
        NSLog(@"fileNotExists");
        
        NSArray *pathComponents = [NSArray arrayWithObjects:
                                   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                   @"MyAudioMemo.m4a",
                                   nil];
        NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
        
        
        
        // Setup audio session
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        
        // Define the recorder setting
        NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
        
        [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        
        // Initiate and prepare the recorder
        recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
        recorder.delegate = self;
        recorder.meteringEnabled = YES;
        [recorder prepareToRecord];
        
        
    }
}


- (void)audioRecord {
    
    NSLog(@"audioRecord");
    
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Audio Record", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Record", nil), nil];
    
    sheet.tag = 101;
    
    [sheet showFromToolbar:self.inputToolbar];
    
}

- (void) startAudioRecord{
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    
    // Start recording
    [recorder record];
    
}

- (void) audioRecordDone{
    
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    //
    
}

//audio record delegate
/*
- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    
    NSLog(@"FinishRecording");
    
    NSString *urlString = recorder.url.absoluteString;
    
    NSLog(@"audio urlString %@",urlString);
    
    NSData *audioData = [NSData dataWithContentsOfFile:urlString];
    
    
        JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audioData];
        JSQMessage * message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:audioItem];
    
        [self.demoData.messages addObject:message];
        [self sendMessageWithMediaUrl:recorder.url andFileName:@"Audio.m4a" forFiletype:@"audio"];
        [self finishSendingMessageAnimated:YES];

}

 */
- (void)audioRecordDoneActionSheet {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Audio Record", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Done", nil), nil];
    
    sheet.tag = 102;
    
    [sheet showFromToolbar:self.inputToolbar];

    
}

//temporary method for test

- (void)playRecordedAudio {
    
    NSLog(@"playRecordedAudio");
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
    [player setDelegate:self];
    [player play];
    
}


#pragma mark  addPictureAction
- (void)addPictureAction {
    
    NSLog(@"addPictureAction");
    
    self.inputToolbar.hidden=YES;
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
  //  actionSheet.view.tintColor = UIColorFromRGB(0x4a4a4a);
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"From camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *cameraImagePicker = [[UIImagePickerController alloc] init];
        cameraImagePicker.delegate = self;
        cameraImagePicker.allowsEditing = YES;
        cameraImagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:cameraImagePicker animated:YES completion:NULL];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"From libaray" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController *galleryImagePicker = [[UIImagePickerController alloc] init];
        galleryImagePicker.delegate = self;
        galleryImagePicker.allowsEditing = YES;
        galleryImagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:galleryImagePicker animated:YES completion:NULL];
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (isPickImage == 1) {
        
        UIImage* selectedImage = (UIImage*) [info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSURL *urlFile = [info objectForKey:UIImagePickerControllerMediaURL];
        NSString *fileName = [[urlFile path] lastPathComponent];
        
        NSLog(@"selectedImage %@ %@",fileName,selectedImage);
        
        
        [picker dismissViewControllerAnimated:YES completion:^{
            
            
            JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:selectedImage];
            JSQMessage * message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:photoItem];
            
            [self.demoData.messages addObject:message];
            [self sendMessageWithPhoto:selectedImage andFileName:@"Picture.jpg"];
            [self finishSendingMessageAnimated:YES];
            
        }];
    }else{
        
        NSLog(@"video picker");
        
        // This is the NSURL of the video object
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSLog(@"VideoURL = %@", videoURL);
        
        [picker dismissViewControllerAnimated:YES completion:^{
            
            
            JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES thumbnailImage:nil];
            JSQMessage * message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:videoItem];
            
            [self.demoData.messages addObject:message];
            [self sendMessageWithMediaUrl:videoURL andFileName:@"Video.mp4" forFiletype:@"video"];
            [self finishSendingMessageAnimated:YES];
            
        }];
    }
    
    self.inputToolbar.hidden=NO;
    [self.inputToolbar.contentView.textView becomeFirstResponder];
    

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    self.inputToolbar.hidden=NO;
    [self.inputToolbar.contentView.textView becomeFirstResponder];
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}

#pragma mark - JSQMessages CollectionView DataSource

- (NSString *)senderId {
    return kJSQDemoAvatarIdSquires;
}

- (NSString *)senderDisplayName {
    return kJSQDemoAvatarDisplayNameSquires;
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.demoData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.demoData.outgoingBubbleImageData;
    }
    
    return self.demoData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        if (![NSUserDefaults outgoingAvatarSetting]) {
            return nil;
        }
    }
    else {
        if (![NSUserDefaults incomingAvatarSetting]) {
            return nil;
        }
    }
    
    
    return [self.demoData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
  //  if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
  //  }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
            cell.textView.backgroundColor = [UIColor colorWithRed:131/255.0 green:126/255.0 blue:247/255.0 alpha:1];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
            cell.textView.backgroundColor = [UIColor colorWithRed:252/255.0 green:169/255.0 blue:206/255.0 alpha:1];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    cell.accessoryButton.hidden = ![self shouldShowAccessoryButtonForMessage:msg];
    
    return cell;
}

- (BOOL)shouldShowAccessoryButtonForMessage:(id<JSQMessageData>)message
{
    return ([message isMediaMessage] && [NSUserDefaults accessoryButtonForMediaMessages]);
}


#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Custom Action", nil)
                                message:nil
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];

}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
     
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
    
    if(nextPage.length)
        [self getChatHistoryByPage];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    
    NSLog(@"currentMessage %@",currentMessage.media);
    
    
    if (currentMessage.isMediaMessage) {
        id<JSQMessageMediaData> mediaItem = currentMessage.media;
        
        if ([mediaItem isKindOfClass:[JSQPhotoMediaItem class]]) {
           
            JSQPhotoMediaItem *photoMedia=(JSQPhotoMediaItem*) currentMessage.media;
            
            if(photoMedia.image)
            {
                photoView = [[VIPhotoView alloc] initWithFrame:self.view.bounds andImage:photoMedia.image];
                photoView.autoresizingMask = (1 << 6) -1;
                
                [self.view addSubview:photoView];
                
                //add cross Button
                
                crossButton = [UIButton buttonWithType:UIButtonTypeCustom];
                
                [crossButton addTarget:self
                                action:@selector(crossButtonAction:)
                      forControlEvents:UIControlEventTouchUpInside];
                [crossButton setTitle:@"" forState:UIControlStateNormal];
                crossButton.frame = CGRectMake(15.0, 20.0, 35.0, 35.0);
                UIImage *btnImage = [UIImage imageNamed:@"search-cancel"];
                [crossButton setImage:btnImage forState:UIControlStateNormal];
                
                [self.view addSubview:crossButton];
            
            
            self.inputToolbar.hidden=YES;
            [self.inputToolbar.contentView.textView resignFirstResponder];
            
            }

            
        }
        else if ([mediaItem isKindOfClass:[JSQVideoMediaItem class]])
        {
            self.inputToolbar.hidden=YES;
            [self.inputToolbar.contentView.textView resignFirstResponder];
            
            
            JSQVideoMediaItem * videoItem = (JSQVideoMediaItem *) currentMessage.media;
            
            
           // NSURL *vedioURL =[NSURL fileURLWithPath:videoItem.fileURL];
            
            AVURLAsset *asset = [AVURLAsset assetWithURL: [NSURL URLWithString:videoItem.fileURL]];
            AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
            
            // Subscribe to the AVPlayerItem's DidPlayToEndTime notification.
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];

            
            AVPlayer* playVideo = [[AVPlayer alloc] initWithPlayerItem:item];
            playerViewController = [[AVPlayerViewController alloc] init];
            playerViewController.player = playVideo;
            playerViewController.player.volume = 0;
            
            [self presentViewController:playerViewController animated:YES completion:^{
                
                [playVideo play];
            }];
            
     }
        
        
    }
    
    else
    {
        //[collectionView reloadItemsAtIndexPaths:@[indexPath]];
        [collectionView reloadData];
    }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.demoData.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}

#pragma mark - JSQMessagesViewAccessoryDelegate methods

- (void)messageView:(JSQMessagesCollectionView *)view didTapAccessoryButtonAtIndexPath:(NSIndexPath *)path
{
    NSLog(@"Tapped accessory button!");
}


-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    
    
    [playerViewController dismissViewControllerAnimated:YES completion:^{
        
        self.inputToolbar.hidden=NO;
        
    }];

    //[self.inputToolbar.contentView.textView becomeFirstResponder];
    
}


#pragma mark - Message Api Integrations

-(void)getChatHistoryByPage
{
    NSLog(@"pageIndex %d %@",pageIndex, nextPage);
    NSMutableDictionary *messagePayload=[[NSMutableDictionary alloc] init];
    [messagePayload setObject:self.otherPersonID forKey:@"USER_ID"];
    [messagePayload setObject:[NSString stringWithFormat:@"%d",pageIndex] forKey:@"PAGE"];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getChatMessageInfo:messagePayload WithComplisionBlock:^(NSDictionary *result)
     {
        
            
           // NSLog(@"Response: %@", result);
            
            NSMutableDictionary *responsedic= [[NSMutableDictionary alloc] initWithDictionary:[result dictionaryByReplacingNullsWithBlanks]];
            
            if ( responsedic!=nil) {

                
                NSMutableArray *tempArray=[[NSMutableArray alloc] initWithArray:[responsedic  objectForKey:@"results"]];

                // messageCount=[[responsedic  objectForKey:@"results"] intValue];
                
                NSMutableArray *messagesArray;
                if(tempArray.count)
                {
                    
                    if(pageIndex==1)
                    {
                        self.demoData.messages =[[NSMutableArray alloc] init];
                        messagesArray=[[[tempArray reverseObjectEnumerator] allObjects] mutableCopy];
                    }
                    else
                    {
                        messagesArray=tempArray;
                        
                    }
                }
                
                NSLog(@"messagesArray: %@", messagesArray);

                for (int i = 0; i < messagesArray.count; i++) {
                    
                    JSQMessage *message;
                    
                    if([[[messagesArray objectAtIndex:i] objectForKey:@"sent_to"] intValue] == [self.otherPersonID intValue])
                    {
                        // NSLog(@"i am the sender");
                        
                        if([[[messagesArray objectAtIndex:i] objectForKey:@"attachment"] length])
                        {
                            
                            //if attachment_type video
                            //[[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] isEqualToString:@"video/quicktime"]
                            if ([[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] rangeOfString:@"video"].location != NSNotFound || [[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] isEqualToString:@"3"]) {
                                
                                NSLog(@"attachment_type video");
                                NSURL *videoURL = [[messagesArray objectAtIndex:i] objectForKey:@"attachment"];
                                
                                JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES thumbnailImage:nil];
                                
                                message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[AppSupport getNSDateFromServerDate:[[messagesArray objectAtIndex:i] objectForKey:@"date"]] media:videoItem];
                                
                             //if attachment_type audio

                            }else if ([[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] rangeOfString:@"audio"].location != NSNotFound || [[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] isEqualToString:@"4"]){
                                
                                NSString *urlString = [[messagesArray objectAtIndex:i] objectForKey:@"attachment"];
                                
                                NSLog(@"audio urlString %@",urlString);
                                
                                NSData *audioData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                                
                                
                                JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audioData];
                                
                                 message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:audioItem];
                                
                            }else{
                                
                                JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@",[[messagesArray objectAtIndex:i] objectForKey:@"attachment"]]];
                                
                                
                                //                            message = [JSQMessage messageWithSenderId:[NSString stringWithFormat:@"%i",[UserAccount sharedManager].userId]
                                //                                                          displayName:[UserAccount sharedManager].userName
                                //                                                                media:photoItem];
                                //
                                // JSQMessage * message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:photoItem];
                                
                                message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[AppSupport getNSDateFromServerDate:[[messagesArray objectAtIndex:i] objectForKey:@"date"]] media:photoItem];
                            }
                            
                            
                            
                        }else
                        {
                            
                            message  = [[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires
                                                          senderDisplayName:kJSQDemoAvatarDisplayNameSquires
                                                                       date:[AppSupport getNSDateFromServerDate:[[messagesArray objectAtIndex:i] objectForKey:@"date"]]
                                                                       text:[[messagesArray objectAtIndex:i] objectForKey:@"message"]];
                            
                        }
                        
                    }
                    else
                    {
                        if([[[messagesArray objectAtIndex:i] objectForKey:@"attachment"] length])
                        {
                            //if attachment_type video
                            if ([[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] rangeOfString:@"video"].location != NSNotFound || [[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] isEqualToString:@"3"]) {
                                
                                NSLog(@"attachment_type video");
                                NSURL *videoURL = [[messagesArray objectAtIndex:i] objectForKey:@"attachment"];
                                
                                JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES thumbnailImage:nil];
                                
                                message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[AppSupport getNSDateFromServerDate:[[messagesArray objectAtIndex:i] objectForKey:@"date"]] media:videoItem];
                                
                            }else if ([[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] rangeOfString:@"audio"].location != NSNotFound || [[[messagesArray objectAtIndex:i] objectForKey:@"attachment_type"] isEqualToString:@"4"]){
                                
                                NSString *urlString = [[messagesArray objectAtIndex:i] objectForKey:@"attachment"];
                                
                                NSLog(@"audio urlString %@",urlString);
                                
                                NSData *audioData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                                
                                
                                JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audioData];
                                
                                message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[NSDate date] media:audioItem];
                                
                            }else{
                                
                                JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImageUrl:[NSString stringWithFormat:@"%@",[[messagesArray objectAtIndex:i] objectForKey:@"attachment"]]];
                                
                                
                                message=[[JSQMessage alloc] initWithSenderId:kJSQDemoAvatarIdSquires senderDisplayName:kJSQDemoAvatarDisplayNameSquires date:[AppSupport getNSDateFromServerDate:[[messagesArray objectAtIndex:i] objectForKey:@"date"]] media:photoItem];
                            }
                            
                        }
                        else
                        {
                            message  = [[JSQMessage alloc] initWithSenderId:self.otherPersonID
                                                          senderDisplayName:self.otherPersonID
                                                                       date:[AppSupport getNSDateFromServerDate:[[messagesArray objectAtIndex:i] objectForKey:@"date"]]
                                                                       text:[[messagesArray objectAtIndex:i] objectForKey:@"message"]];
                        }
                        
                        
                    }
                    
                    if(pageIndex==1)
                        [self.demoData.messages addObject:message];
                    else
                        [self.demoData.messages insertObject:message atIndex:0];
                    
                    
                    if(i==messagesArray.count-1 )
                    {
                        [self.collectionView reloadData];
                        
                        if(pageIndex==1 )
                        {
                            if (self.automaticallyScrollsToMostRecentMessage) {
                                [self scrollToBottomAnimated:YES];
                            }
                        }
//                        else
//                        {
//
//
////                            //                                int indexToScroll=[[paginationInfo objectForKey:@"to"] intValue]-([[paginationInfo objectForKey:@"current_page"] intValue]-1)*[[paginationInfo objectForKey:@"per_page"] intValue];
////                            int indexToScroll=pageIndex+1;
////                            NSLog(@"indexToScroll %d",indexToScroll);
////
////                            NSIndexPath *firstCell = [NSIndexPath indexPathForItem:indexToScroll inSection:0];
////                            [self scrollToIndexPath:firstCell animated:NO];
////
//                        }
                        
                        if([[responsedic  objectForKey:@"count"] intValue]>(pageIndex*10))
                        {
                            pageIndex++;
                            
                        }
                        nextPage=[responsedic  objectForKey:@"next"];
                        
                    }
                    
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:@"Sorry, unable to fetch data. Please try again." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    
                });

            }
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
         });
    }];
   
    
}

-(void) sendMessages: (NSString *) message
{
    NSMutableDictionary *messagePayload=[[NSMutableDictionary alloc] init];
    [messagePayload setObject:message forKey:@"message"];
    [messagePayload setObject:@"1" forKey:@"attachment_type"];
  //  [messagePayload setObject:self.myID forKey:@"received_from"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] postChatMessageInfo:messagePayload andToUserId:self.otherPersonID WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"post message result %@",result);
        
        if (result)
        {

//            if ([result valueForKey:@"next"] != [NSNull null]) {
//                nextchatPageAvailable = YES;
//                chatCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
//
//            }else{
//                nextchatPageAvailable = NO;
//            }

        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
        
    }];
  
}

-(void) sendMessageWithPhoto : (UIImage *)image andFileName:(NSString*)fileName
{
    NSMutableDictionary *messagePayload=[[NSMutableDictionary alloc] init];
    //[messagePayload setObject:@"Testing Media message" forKey:@"message"];
    [messagePayload setObject:[self scaleAndRotateImage:image] forKey:@"media"];
    [messagePayload setObject:fileName forKey:@"fileName"];
    [messagePayload setObject:@"2" forKey:@"attachment_type"];
    [messagePayload setObject:@"" forKey:@"message"];
    
    NSLog(@"messagePayload %@",messagePayload);
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] postPhotoMessageInfo:messagePayload andToUserId:self.otherPersonID WithComplisionBlock:^(NSDictionary *result) {
        NSLog(@"post message result %@",result);
        
        if ([result valueForKey:@"results"])
        {
            NSLog(@"");
            
        }
        else{
        //    [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
    }];

}

-(void) sendMessageWithMediaUrl : (NSURL *)url andFileName:(NSString*)fileName forFiletype:(NSString*)filetype
{
    NSMutableDictionary *messagePayload=[[NSMutableDictionary alloc] init];
    //[messagePayload setObject:@"Testing Media message" forKey:@"message"];
    [messagePayload setObject:url forKey:@"media"];
    [messagePayload setObject:fileName forKey:@"fileName"];
    [messagePayload setObject:filetype forKey:@"fileType"];
    [messagePayload setObject:@"" forKey:@"message"];
    
    if ([filetype isEqualToString:@"video"]) {
        
    [messagePayload setObject:@"3" forKey:@"attachment_type"];

    }else{
        
        [messagePayload setObject:@"4" forKey:@"attachment_type"];

    }
    
    NSLog(@"messagePayload %@",messagePayload);
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] postMediaMessageInfo:messagePayload andToUserId:self.otherPersonID WithComplisionBlock:^(NSDictionary *result) {
        NSLog(@"post message result %@",result);
        
        if ([result valueForKey:@"results"])
        {
            NSLog(@"");
            
        }
        else{
            //    [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
    }];
    
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution)
    {
        CGFloat ratio = width/height;
        if (ratio > 1)
        {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else
        {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft)
    {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else
    {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (void)crossButtonAction:(UIButton*)button
{
    NSLog(@" cross Button  clicked.");
    
    self.inputToolbar.hidden=NO;
    [crossButton removeFromSuperview];
    [photoView removeFromSuperview];
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}





@end


