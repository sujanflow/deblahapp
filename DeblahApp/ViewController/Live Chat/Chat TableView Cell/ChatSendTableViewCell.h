//
//  ChatSendTableViewCell.h
//  iMessageBubble
//
//  Created by TWINBIT MAC3 on 4/17/18.
//  Copyright © 2018 Prateek Grover. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatSendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *chatMessageLabel;
@property (weak, nonatomic) IBOutlet UIView *audioView;

@property (weak, nonatomic) IBOutlet UIView *photoView;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;


@end
