//
//  ChatReceiveTableViewCell.m
//  iMessageBubble
//
//  Created by TWINBIT MAC3 on 4/17/18.
//  Copyright © 2018 Prateek Grover. All rights reserved.
//

#import "ChatReceiveTableViewCell.h"

@implementation ChatReceiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    if (IS_DEVICE_IPAD) {
        
        self.chatUserImage.frame = CGRectMake(20, 5, 34, 34);
        self.chatUserImage.clipsToBounds = YES;
        self.chatUserImage.layer.cornerRadius = 17;
        
    }else{
        
        self.chatUserImage.frame = CGRectMake(20, 5, 46, 46);
        self.chatUserImage.clipsToBounds = YES;
        self.chatUserImage.layer.cornerRadius = 23;
    }
    
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
