//
//  PaymentViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "PaymentViewController.h"
#import "DropDownMenu.h"
#import "PayTabWebViewController.h"

@interface PaymentViewController ()<DropMenuDelegate,UIGestureRecognizerDelegate>
{
    UITapGestureRecognizer *tapGesture;
    
    NSString *paymentUrl;
    
    DropDownMenu *monthDropMenu;
    DropDownMenu *yearDropMenu;
    
    
}
@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];
    self.navigationController.navigationBar.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHide:)];
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    tapGesture.cancelsTouchesInView = NO;
    
}


-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        CGRect labFrame = _titleLabel.frame;
        labFrame.origin.y = 100;
        _titleLabel.frame = labFrame;
    }
    
    if (IS_IPHONE_5) {
        CGRect labFrame = _titleLabel.frame;
        labFrame.origin.y = 72;
        _titleLabel.frame = labFrame;
    }
    
}

-(void)keyBoardHide:(UITapGestureRecognizer*)sender{
    
    [_billingAdressTextField resignFirstResponder];
    [_stateTextField resignFirstResponder];
    [_cityTextField resignFirstResponder];
    [_postalTextField resignFirstResponder];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidDisappear:(BOOL)animated{
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}




#pragma mark -
#pragma mark - Button Action

-(BOOL)inputValidate{
    
    if ( _billingAdressTextField.text.length == 0 || _stateTextField.text.length == 0 ||  _cityTextField.text.length == 0 || _postalTextField.text.length == 0) {
        [DELEGATE makeFillupPopUp:@"Fill up TextField"];
        return NO;
    }
    
    if ( _postalTextField.text.length <5) {
        [DELEGATE makeFillupPopUp:@"Postal Code should be 5 digits"];
        return NO;
    }
    
    
    return YES;
}

-(IBAction)cancelButtonAction:(UIButton *)button{
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)purchaseButtonAction:(UIButton *)button{
    
    [_billingAdressTextField resignFirstResponder];
    [_stateTextField resignFirstResponder];
    [_cityTextField resignFirstResponder];
    [_postalTextField resignFirstResponder];
    
    if ([self inputValidate]) {
       [self createPay];
    }
    
}

-(void)createPay{
    
    NSString *redirectUrl = @"https://www.google.com/";
    
    NSMutableDictionary *payDic = [[NSMutableDictionary alloc] init];
    
    [payDic setValue:redirectUrl forKey:@"return_url"];
    [payDic setValue:self.selectedPoint forKey:@"quantity"];
    [payDic setValue:_billingAdressTextField.text forKey:@"billing_address"];
    [payDic setValue:_stateTextField.text forKey:@"state"];
    [payDic setValue:_cityTextField.text forKey:@"city"];
    [payDic setValue:_postalTextField.text forKey:@"postal_code"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getCreatePayInfo:payDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"billing_address"]) {
            
//            NSString *billingAddress = [result valueForKey:@"billing_address"];
//            NSString *city = [result valueForKey:@"city"];
//            NSString *postalCode = [result valueForKey:@"postal_code"];
//            NSString *quantity = [result valueForKey:@"quantity"];
//            NSString *state = [result valueForKey:@"state"];
//            NSString *url = [result valueForKey:@"return_url"];
            
            NSDictionary *payment = [result valueForKey:@"payment"];
            
            paymentUrl = [payment valueForKey:@"payment_url"];
        
            NSNumber *pid = [payment valueForKey:@"p_id"];
            
            DELEGATE.paymentID = [NSString stringWithFormat:@"%ld",[pid integerValue]];
            
            
            NSLog(@"%@",DELEGATE.paymentID);
         
            
            [self gotoPayTab];
            
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}

-(void)gotoPayTab{
    
    PayTabWebViewController *paytabVC = [[PayTabWebViewController alloc] initWithNibName:@"PayTabWebViewController" bundle:nil];
    paytabVC.paymentUrl = [NSURL URLWithString:paymentUrl];
    [self presentViewController:paytabVC animated:YES completion:nil];
    
    
}
@end
