//
//  PackageViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageViewController : UIViewController




@property (nonatomic, weak) IBOutlet UIView *navBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
