//
//  PaymentViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentViewController : UIViewController

@property (nonatomic, retain) NSString *selectedPoint;

@property(nonatomic, weak) IBOutlet UILabel *titleLabel;



@property(nonatomic, weak) IBOutlet UITextField *billingAdressTextField;
@property(nonatomic, weak) IBOutlet UITextField *stateTextField;
@property(nonatomic, weak) IBOutlet UITextField *cityTextField;
@property(nonatomic, weak) IBOutlet UITextField *postalTextField;


@property(nonatomic, weak) IBOutlet UIButton *cancelButton;
@property(nonatomic, weak) IBOutlet UIButton *confirmButton;




@end
