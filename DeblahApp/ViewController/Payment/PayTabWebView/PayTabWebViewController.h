//
//  PayTabWebViewController.h
//  DeblahApp
//
//  Created by Sabuj on 31/5/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayTabWebViewController : UIViewController

@property (nonatomic,weak) IBOutlet UIView *navView;
@property (nonatomic,weak) IBOutlet UIWebView *webVIew;

@property (nonatomic,retain) NSURL *paymentUrl;


@end
