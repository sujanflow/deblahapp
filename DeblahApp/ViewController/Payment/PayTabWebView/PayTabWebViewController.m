//
//  PayTabWebViewController.m
//  DeblahApp
//
//  Created by Sabuj on 31/5/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "PayTabWebViewController.h"

@interface PayTabWebViewController ()<UIWebViewDelegate>

@end

@implementation PayTabWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webVIew.delegate = self;
    
  
    [self loadPayTabWebView];
    
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        CGRect navFrame = _navView.frame;
        navFrame.size.height = 85;
        _navView.frame = navFrame;
        
        CGRect webFrame = _webVIew.frame;
        webFrame.origin.y = 85;
        webFrame.size.height = DEVICE_HEIGHT-85;
        _webVIew.frame = webFrame;
    }
    
}

-(IBAction)cancelAction:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadPayTabWebView{
    
    NSURLRequest *requestUrl = [NSURLRequest requestWithURL:self.paymentUrl];
    [_webVIew loadRequest:requestUrl];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *requestedURL = [request URL];
    NSLog(@"%@",requestedURL);
    
  
    if ([requestedURL.absoluteString isEqualToString:@"https://www.google.com/"]) {
        
        DELEGATE.isPaymentComplete = YES;
        
        [self cancelAction:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
   
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
    });
}


@end
