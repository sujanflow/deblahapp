//
//  PackageViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "PackageViewController.h"
#import "PackageTableViewCell.h"
#import "PaymentViewController.h"

@interface PackageViewController ()<UITableViewDelegate,UITableViewDataSource>
{
   
    NSMutableArray *packageArray;
    NSMutableArray *imageArray;
    
    NSString *selectedPoint;
    
    NSMutableArray *checkSelectedArray;
  
}
@end

@implementation PackageViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1];
    self.navigationController.navigationBar.hidden = YES;
    
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    packageArray = [[NSMutableArray alloc] init];
    imageArray = [[NSMutableArray alloc] init];
    
    [self getDataFromAPI];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
   
}


-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 90;
        tableFrame.size.height = 450;
        _tableView.frame = tableFrame;
    }
    
    if (IS_IPHONE_5) {
        
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 65;
        tableFrame.size.height = 290;
        _tableView.frame = tableFrame;
    }
    
    if (IS_IPHONE_6) {
        
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 65;
        tableFrame.size.height = 350;
        _tableView.frame = tableFrame;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getCoinIcon{
    
    [imageArray addObject:@"60-point"];
    [imageArray addObject:@"110-point"];
    [imageArray addObject:@"130-point"];
    [imageArray addObject:@"145-point"];
    [imageArray addObject:@"160-point"];
    [imageArray addObject:@"180-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    [imageArray addObject:@"200-point"];
    
}

-(void)getDataFromAPI{
    
    packageArray = [[NSMutableArray alloc] init];
    checkSelectedArray = [[NSMutableArray alloc] init];
    
    imageArray = [[NSMutableArray alloc] init];
    [self getCoinIcon];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getPackageListInfoWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"package_options"]) {
            
            NSArray *array = [result valueForKey:@"package_options"];
            
            for (int i = 0 ; i<array.count; i++) {
                
                NSArray *nestedArray = [array objectAtIndex:i];
                
                NSNumber *num = [nestedArray objectAtIndex:0];
                NSString *point = [NSString stringWithFormat:@"%ld",[num integerValue]];
                
                NSString *str = [nestedArray objectAtIndex:1];
                
                
                NSArray *comps = [str componentsSeparatedByString:@"for"];
                NSString *sar  = [comps objectAtIndex:1];
                NSInteger money = [sar integerValue];
                NSString *amount = [NSString stringWithFormat:@"%ld",money];
            
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                
                [dic setValue:str forKey:@"DETAILS"];
                [dic setValue:point forKey:@"POINT"];
                [dic setValue:amount forKey:@"SAR"];
                
                [packageArray addObject:dic];
                
                
                //check
                [checkSelectedArray addObject:@"NO"];
            }
            
            
            //check
            [self checkOldPurchaseAmount];
            
            
            [_tableView reloadData];
           
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}

-(void)checkOldPurchaseAmount{
    
    NSString *oldAmount = [[NSUserDefaults standardUserDefaults]valueForKey:@"PURCHASE_AMOUNT"];

    if (oldAmount.length>1) {
        
        
        NSInteger index = -1;
        
        for (int i = 0 ; i< packageArray.count; i++) {
            
            NSDictionary *dic = [packageArray objectAtIndex:i];
            NSString *pointStr = [dic valueForKey:@"SAR"];
            NSString *oldStr = oldAmount;
            
            if ([pointStr integerValue] == [oldStr integerValue]) {
                index = i;
                break;
            }
        }
        
        
        if (index != -1) {
            
            [checkSelectedArray replaceObjectAtIndex:index withObject:@"YES"];
            
            NSDictionary *dic = [packageArray objectAtIndex:index];
            selectedPoint = [dic valueForKey:@"POINT"];
        }
        
    }
    
}
#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return packageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PackageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"packageCell"];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PackageTableViewCell" owner:self options:nil];
        cell = (PackageTableViewCell *)[topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [packageArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = [dic valueForKey:@"DETAILS"];
    cell.coverImageView.image = [UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]];
    
    
    //check
    if ([[checkSelectedArray objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
        [cell.loveButton setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
    }else{
        [cell.loveButton setImage:[UIImage imageNamed:@"mcq-sign"] forState:UIControlStateNormal];
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 60: 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    PackageTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [cell.loveButton setImage:[UIImage imageNamed:@"mcq-sign-selected"] forState:UIControlStateNormal];
    
    NSDictionary *dic = [packageArray objectAtIndex:indexPath.row];
    selectedPoint = [dic valueForKey:@"POINT"];
    
    //check
    [checkSelectedArray removeAllObjects];
    for (int i = 0; i < packageArray.count; i++) {
        [checkSelectedArray addObject:@"NO"];
    }
    [checkSelectedArray replaceObjectAtIndex:indexPath.row withObject:@"YES"];
    [_tableView reloadData];
}

#pragma mark -
#pragma mark - Button Action

-(IBAction)cancelButtonAction:(UIButton *)button{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)purchaseButtonAction:(UIButton *)button{
    
    if (selectedPoint.length>1) {
        
        NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
        
        PaymentViewController *paymentViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"PaymentViewController"];
        paymentViewController.selectedPoint = selectedPoint;
        
        [self.navigationController pushViewController:paymentViewController animated:YES];
    }else{
        
        [DELEGATE makeFillupPopUp:@"Please choose one package"];
    }
    
  
}
@end
