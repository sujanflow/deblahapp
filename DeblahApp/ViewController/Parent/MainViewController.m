//
//  ViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/22/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // -----  LoginViewController -------
    
    self.loginViewController = [[LoginViewController alloc] init];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    self.loginViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"LoginViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.loginViewController];
    
    [self addChildViewController:navController];
    [navController willMoveToParentViewController:nil];
    navController.view.frame = CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT);
    [self.view addSubview:navController.view];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
