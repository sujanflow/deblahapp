//
//  SignUpViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *mainView;


@property (nonatomic, weak) IBOutlet UIButton *profileButton;


@property (nonatomic, weak) IBOutlet UITextField *nameTextField;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UITextField *confirmPasswordTextField;

@property (nonatomic, weak) IBOutlet UIButton *termButton;


@property (nonatomic, weak) IBOutlet UIButton *backButton;

@end
