//
//  BasicInfoViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "BasicInfoViewController.h"
#import "ParentTabViewController.h"
#import "SoulmateInfoViewController.h"

#import "DropDownMenu.h"
#import "ZHDatePicker.h"


@interface BasicInfoViewController ()<DropMenuDelegate,UIGestureRecognizerDelegate,ZHDatePickerDelegate>
{
    UITapGestureRecognizer *tapGesture;
    
  
    
    //----DropDown ---
    
    DropDownMenu *genderDropMenu;
    DropDownMenu *religionDropMenu;
    DropDownMenu *bloodDropMenu;
    DropDownMenu *beautyDropMenu;
    DropDownMenu *skinDropMenu;
    DropDownMenu *livingDropMenu;
    DropDownMenu *tribeDropMenu;
    DropDownMenu *educationDropMenu;
    DropDownMenu *heightDropMenu;
    DropDownMenu *weightDropMenu;
    
    NSString *mailType;
    NSString *userNameType;
    
    NSString *genderType;
    NSString *birthdayType;
    NSString *religionType;
    NSString *bloodType;
    
    NSString *beautyType;
    NSString *skinType;
    NSString *livingType;
    NSString *tribeType;
    NSString *educationType;
    
    NSString *heightType;
    NSString *weightType;
    
}

@property(nonatomic,strong) ZHDatePicker *picker;

@end

@implementation BasicInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationController.navigationBar.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHide:)];
    [self.view addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    tapGesture.cancelsTouchesInView = NO;
    
    [self navigationBackGestureRemove];
    

   
    
    [self getDataForDropDown];
    

}

-(void)viewDidAppear:(BOOL)animated{
    
    // profile_image
    if (_profileImage == nil) {
        NSString *imageStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"PROFILE_IMAGE"];
        NSURL *imageUrl = [NSURL URLWithString:[imageStr stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        _profileImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
    }
}

-(void)navigationBackGestureRemove{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark- Get Data

-(void)getDataForDropDown{
    
   
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getBasicDropDownWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if (result.count>1) {
            
            [self parseDropDownMenu:result];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}

-(void)parseDropDownMenu:(NSDictionary *)result{
    
    NSMutableArray *optionBeautyArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionBloodArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionEducationArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionGenderArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionLivingArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionReligionArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionSkinArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionTribeArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *optionHeightArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionWeightArray = [[NSMutableArray alloc] init];
    
    
    NSArray *beautyArray = [result valueForKey:@"beauty_level_option"];
    for (int i = 0; i<beautyArray.count; i++) {
        NSArray *array = [beautyArray objectAtIndex:i];
        [optionBeautyArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *bloodArray = [result valueForKey:@"blood_group_option"];
    for (int i = 0; i<bloodArray.count; i++) {
        NSArray *array = [bloodArray objectAtIndex:i];
        [optionBloodArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *educationArray = [result valueForKey:@"education_option"];
    for (int i = 0; i<educationArray.count; i++) {
        NSArray *array = [educationArray objectAtIndex:i];
        [optionEducationArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *genderArray = [result valueForKey:@"gender_option"];
    for (int i = 0; i<genderArray.count; i++) {
        NSArray *array = [genderArray objectAtIndex:i];
        [optionGenderArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *livingArray = [result valueForKey:@"living_status_option"];
    for (int i = 0; i<livingArray.count; i++) {
        NSArray *array = [livingArray objectAtIndex:i];
        [optionLivingArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *religionArray = [result valueForKey:@"religion_option"];
    for (int i = 0; i<religionArray.count; i++) {
        NSArray *array = [religionArray objectAtIndex:i];
        [optionReligionArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *skinArray = [result valueForKey:@"skin_color_option"];
    for (int i = 0; i<skinArray.count; i++) {
        NSArray *array = [skinArray objectAtIndex:i];
        [optionSkinArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *tribeArray = [result valueForKey:@"tribe_type_option"];
    for (int i = 0; i<tribeArray.count; i++) {
        NSArray *array = [tribeArray objectAtIndex:i];
        [optionTribeArray addObject: [array objectAtIndex:1]];
    }
    
    
    
    NSArray *heightArray = [result valueForKey:@"height_range_option"];
    for (int i = 0; i<heightArray.count; i++) {
        NSArray *array = [heightArray objectAtIndex:i];
        [optionHeightArray addObject: [array objectAtIndex:1]];
    }
    
    
    NSArray *weightArray = [result valueForKey:@"weight_range_option"];
    for (int i = 0; i<weightArray.count; i++) {
        NSArray *array = [weightArray objectAtIndex:i];
        [optionWeightArray addObject: [array objectAtIndex:1]];
    }
    

    
    [self setGenderDropDownMenu:optionGenderArray];
    [self setReligionDropDownMenu:optionReligionArray];
    [self setBloodDropDownMenu:optionBloodArray];
    [self setBeautyDropDownMenu:optionBeautyArray];
    [self setSkinDropDownMenu:optionSkinArray];
    [self setLivingDropDownMenu:optionLivingArray];
    [self setTribeDropDownMenu:optionTribeArray];
    [self setEducationDropDownMenu:optionEducationArray];
    
    [self setHeightDropDownMenu:optionHeightArray];
    [self setWeightDropDownMenu:optionWeightArray];
    
    [self setBirthdayPicker];
    
}



#pragma mark-
#pragma mark- Button Action

-(BOOL)inputValidate{
    
    if (_firstNameTextField.text.length == 0 || _lastNameTextField.text.length == 0 || _phoneTextField.text.length == 0 || _occupationTextField.text.length == 0) {
        [DELEGATE makeFillupPopUp:@"Fill up TextField"];
        return NO;
    }
    
    if ([AppSupport phoneAuthentication: _phoneTextField.text] == NO) {
        [DELEGATE makeFillupPopUp:@"Invalid Phone Number"];
        return NO;
    }
    

    if (genderDropMenu.tag == 0 ||  religionDropMenu.tag == 0 || bloodDropMenu.tag == 0 ||  beautyDropMenu.tag == 0 ||skinDropMenu.tag == 0 ||  livingDropMenu.tag == 0 || tribeDropMenu.tag == 0 ||  educationDropMenu.tag == 0  || heightDropMenu.tag == 0 ||  weightDropMenu.tag == 0) {
        [DELEGATE makeFillupPopUp:@"Invalid Dropdown"];
        return NO;
    }
    
    if (_birthButton.tag == 0) {
        [DELEGATE makeFillupPopUp:@"Invalid Birthday"];
        return NO;
    }
    
       
    return YES;
}


-(IBAction)nextButtonAction:(id)sender{

    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }
  
    if ([self inputValidate]) {
        
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];

        mailType = [[NSUserDefaults standardUserDefaults] valueForKey:@"MAIL_ID"];
        userNameType = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"];

        [infoDic setValue:mailType forKey:@"email"];
        [infoDic setValue:userNameType forKey:@"user_name"];
        [infoDic setValue:_firstNameTextField.text forKey:@"first_name"];
        [infoDic setValue:_lastNameTextField.text forKey:@"last_name"];
        [infoDic setValue:genderType forKey:@"gender"];
        [infoDic setValue:birthdayType forKey:@"birthday"];
        [infoDic setValue:religionType forKey:@"religion"];
        [infoDic setValue:_phoneTextField.text forKey:@"phone_number"];
        [infoDic setValue:bloodType forKey:@"blood_group"];
        [infoDic setValue:_occupationTextField.text forKey:@"occupation"];
        [infoDic setValue:heightType forKey:@"height"];
        [infoDic setValue:weightType forKey:@"weight"];
        [infoDic setValue:beautyType forKey:@"beauty_level"];
        [infoDic setValue:skinType forKey:@"skin_color"];
        [infoDic setValue:livingType forKey:@"living_status"];
        [infoDic setValue:tribeType forKey:@"tribe_type"];
        [infoDic setValue:educationType forKey:@"education"];
       
      
        [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
        
        [[APIClient sharedInstance] userProfileUpdateWithInfo:infoDic andImage:_profileImage WithComplisionBlock:^(NSDictionary *result) {

            NSLog(@"%@",result);

            if ([result valueForKey:@"id"]) {

                NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
                SoulmateInfoViewController *soulmateVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"SoulmateInfoViewController"];

                [self.navigationController pushViewController:soulmateVC animated:YES];

            }else{

                [DELEGATE makeFillupPopUp:@"Error"];
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
            });

        }];
    }
    
}



-(void)keyBoardHide:(UITapGestureRecognizer*)sender{
    
    [_firstNameTextField resignFirstResponder];
    [_lastNameTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_occupationTextField resignFirstResponder];
}

#pragma mark -
#pragma mark - KPDropMenu Delegate

-(void)setGenderDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _genderImageView.frame;
    genderDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    genderDropMenu.tag = 0;
    genderDropMenu.delegate = self;
    genderDropMenu.items = optionArray;
    genderDropMenu.title = @"Gender";
    genderDropMenu.titleFontSize = 12;
    genderDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    genderDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    genderDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    genderDropMenu.itemHeight = 40;
    genderDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:genderDropMenu];

}

-(void)setReligionDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _religionImageView.frame;
    religionDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    religionDropMenu.tag = 0;
    religionDropMenu.delegate = self;
    religionDropMenu.items = optionArray;
    religionDropMenu.title = @"Religion";
    religionDropMenu.titleFontSize = 12;
    religionDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    religionDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    religionDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    religionDropMenu.itemHeight = 40;
    religionDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:religionDropMenu];

}
-(void)setBloodDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _bloodImageView.frame;
    bloodDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    bloodDropMenu.tag = 0;
    bloodDropMenu.delegate = self;
    bloodDropMenu.items = optionArray;
    bloodDropMenu.title = @"Blood Group";
    bloodDropMenu.titleFontSize = 12;
    bloodDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    bloodDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    bloodDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    bloodDropMenu.itemHeight = 40;
    bloodDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:bloodDropMenu];


}
-(void)setBeautyDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _beautyImageView.frame;
    beautyDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    beautyDropMenu.tag = 0;
    beautyDropMenu.delegate = self;
    beautyDropMenu.items = optionArray;
    beautyDropMenu.title = @"Beauty level";
    beautyDropMenu.titleFontSize = 12;
    beautyDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    beautyDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    beautyDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    beautyDropMenu.itemHeight = 40;
    beautyDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:beautyDropMenu];
}
-(void)setSkinDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _skinImageView.frame;
    skinDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    skinDropMenu.tag = 0;
    skinDropMenu.delegate = self;
    skinDropMenu.items = optionArray;
    skinDropMenu.title = @"Skin color";
    skinDropMenu.titleFontSize = 12;
    skinDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    skinDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    skinDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    skinDropMenu.itemHeight = 40;
    skinDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:skinDropMenu];

}
-(void)setLivingDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _livingImageView.frame;
    livingDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    livingDropMenu.tag = 0;
    livingDropMenu.delegate = self;
    livingDropMenu.items = optionArray;
    livingDropMenu.title = @"Living status";
    livingDropMenu.titleFontSize = 12;
    livingDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    livingDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    livingDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    livingDropMenu.itemHeight = 40;
    livingDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:livingDropMenu];
}
-(void)setTribeDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _triveImageView.frame;
    tribeDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    tribeDropMenu.tag = 0;
    tribeDropMenu.delegate = self;
    tribeDropMenu.items = optionArray;
    tribeDropMenu.title = @"Tribe type";
    tribeDropMenu.titleFontSize = 12;
    tribeDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    tribeDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    tribeDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    tribeDropMenu.itemHeight = 40;
    tribeDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:tribeDropMenu];
}
-(void)setEducationDropDownMenu:(NSArray *)optionArray{

    CGRect outletFrame = _educationImageView.frame;
    educationDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    educationDropMenu.tag = 0;
    educationDropMenu.delegate = self;
    educationDropMenu.items = optionArray;
    educationDropMenu.title = @"Education";
    educationDropMenu.titleFontSize = 12;
    educationDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    educationDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    educationDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    educationDropMenu.itemHeight = IS_IPHONE_5 ? 30 : 40;
    educationDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:educationDropMenu];

}


-(void)setHeightDropDownMenu:(NSArray *)optionArray{
    
    CGRect outletFrame = _heightTImageView.frame;
    heightDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    heightDropMenu.tag = 0;
    heightDropMenu.delegate = self;
    heightDropMenu.items = optionArray;
    heightDropMenu.title = @"Height limit [inch]";
    heightDropMenu.titleFontSize = 12;
    heightDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    heightDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    heightDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    heightDropMenu.itemHeight = 40;
    heightDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:heightDropMenu];
    
}

-(void)setWeightDropDownMenu:(NSArray *)optionArray{
    
    CGRect outletFrame = _weightImageView.frame;
    weightDropMenu = [[DropDownMenu alloc] initWithFrame:outletFrame];
    weightDropMenu.tag = 0;
    weightDropMenu.delegate = self;
    weightDropMenu.items = optionArray;
    weightDropMenu.title = @"Weight limit [kg]";
    weightDropMenu.titleFontSize = 12;
    weightDropMenu.titleColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
    weightDropMenu.itemsFont = [UIFont fontWithName:@"Roboto" size:12.0];
    weightDropMenu.titleTextAlignment = NSTextAlignmentLeft;
    weightDropMenu.itemHeight = 40;
    weightDropMenu.itemTextColor = [UIColor colorWithRed:1/255.0 green:1/255.0 blue:1/255.0 alpha:1];
    [self.view addSubview:weightDropMenu];
    
}



-(void)didSelectItem : (DropDownMenu *) dropMenu atIndex : (int) atIntedex{
    
    if (dropMenu == genderDropMenu) {
         genderDropMenu.tag = 1;
         genderType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == religionDropMenu){
        religionDropMenu.tag = 1;
        religionType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == bloodDropMenu){
        bloodDropMenu.tag = 1;
        bloodType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == beautyDropMenu){
        beautyDropMenu.tag = 1;
        beautyType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == skinDropMenu){
        skinDropMenu.tag = 1;
        skinType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == livingDropMenu){
        livingDropMenu.tag = 1;
        livingType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == tribeDropMenu){
        tribeDropMenu.tag = 1;
        tribeType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == educationDropMenu){
        educationDropMenu.tag = 1;
        educationType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == heightDropMenu){
        heightDropMenu.tag = 1;
        heightType = [NSString stringWithFormat:@"%d",atIntedex];
    }
    else if (dropMenu == weightDropMenu){
        weightDropMenu.tag = 1;
        weightType = [NSString stringWithFormat:@"%d",atIntedex];
    }
}

-(void)didShow:(DropDownMenu *)dropMenu{
    NSLog(@"didShow");
    [self keyBoardHide:nil];
}

-(void)didHide:(DropDownMenu *)dropMenu{
    NSLog(@"didHide");
}




#pragma mark - DatePickerVCDelegate

-(ZHDatePicker *)picker {

    if (nil == _picker) {

        ZHDatePicker *picker = [ZHDatePicker presentOnWindow];
        [picker setDelegate:self];
        _picker = picker;
    }
    return _picker;
}

-(void)setBirthdayPicker{
    
    CGRect outletFrame = _birthImageView.frame;
    outletFrame.origin.x = _birthImageView.frame.origin.x+15;
    outletFrame.size.width = _birthImageView.frame.size.width-30;
  
    _birthButton.frame = outletFrame;
    
}

-(IBAction)birthbuttonAction:(UIButton *)button{
    
    [self.picker present];
    
}


- (void)didSelectDateWithDate:(NSDate *)date year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    if (nil == date) {
        return;
    }

    _birthButton.tag = 1;
    NSString *text = [NSString stringWithFormat:@"%@-%@-%@", @(year), @(month), @(day)];
    [_birthButton setTitle:text forState:UIControlStateNormal];
    [_birthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    birthdayType = text;
}



@end
