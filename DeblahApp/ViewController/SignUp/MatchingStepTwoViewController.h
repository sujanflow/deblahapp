//
//  MatchingStepTwoViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchingStepTwoViewController : UIViewController

@property (nonatomic, strong) UIPageViewController *pageController;
@property (nonatomic, weak) IBOutlet UIView *quizView;
@property (nonatomic, weak) IBOutlet UILabel *quizLabel;

@property (nonatomic, weak) IBOutlet UIButton *backButton;
@end
