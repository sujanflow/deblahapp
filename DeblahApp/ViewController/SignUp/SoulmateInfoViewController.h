//
//  SoulmateInfoViewController.h
//  DeblahApp
//
//  Created by Sabuj on 19/4/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoulmateInfoViewController : UIViewController



@property (nonatomic, weak) IBOutlet UITextField *occupationTextField;

@property (nonatomic, weak) IBOutlet UIImageView *heightTImageView;
@property (nonatomic, weak) IBOutlet UIImageView *weightImageView;
@property (nonatomic, weak) IBOutlet UIImageView *genderImageView;
@property (nonatomic, weak) IBOutlet UIImageView *religionImageView;
@property (nonatomic, weak) IBOutlet UIImageView *beautyImageView;
@property (nonatomic, weak) IBOutlet UIImageView *skinImageView;
@property (nonatomic, weak) IBOutlet UIImageView *livingImageView;
@property (nonatomic, weak) IBOutlet UIImageView *triveImageView;
@property (nonatomic, weak) IBOutlet UIImageView *educationImageView;

@property (nonatomic, weak) IBOutlet UIImageView *birthImageView;


@property (nonatomic, weak) IBOutlet UIButton *backButton;

@end
