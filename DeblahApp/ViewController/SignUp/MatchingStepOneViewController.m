//
//  MatchingStepOneViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "MatchingStepOneViewController.h"
#import "PageViewController.h"
#import "MatchingStepTwoViewController.h"

@interface MatchingStepOneViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource>
{
    NSInteger pageNo;
    PageViewController *contoller;
    NSArray *viewControllers;
}

@end


@implementation MatchingStepOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = YES;
    
  
}
-(void)viewWillAppear:(BOOL)animated{
    
    _quizLabel.text = @"";
    DELEGATE.quizType = @"PSYCHOLOGICAL";
    [self getPsychologicalQuestion];
    
}
-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        _backButton.frame = CGRectMake(10, 30, 44, 44);
    }
}
-(void)manualPagingDisable{
    
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
}



-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)inputValidate{
    
    for (int i=0; i<DELEGATE.psychologicalAnswerArray.count; i++) {
        
        NSNumber *num = [DELEGATE.psychologicalAnswerArray objectAtIndex:i];
        NSInteger value = [num integerValue];
        
        if(value == -1){
            [DELEGATE makeFillupPopUp:@"Answer all Question"];
            return NO;
        }
        
    }
    
    return YES;
}


-(IBAction)finishButtonAction:(id)sender{
    
    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }

    __block NSInteger count = 0;
    
    if ([self inputValidate]) {
        

        for (int i = 0; i<DELEGATE.psychologicalQuizArray.count; i++) {
            
            [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
            
            NSDictionary *dic = [DELEGATE.psychologicalQuizArray objectAtIndex:i];
            NSString *idType  = [dic valueForKey:@"QUESTION_ID"];
            
            NSNumber *answerNum  = [DELEGATE.psychologicalAnswerArray objectAtIndex:i];
            
            NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];
            [infoDic setValue:answerNum forKey:@"answer"];
            [infoDic setValue:idType forKey:@"quiz"];
            
           
            [[APIClient sharedInstance] answerQuizWithInfo:infoDic WithComplisionBlock:^(NSDictionary *result) {
                
                NSLog(@"%@",result);
                
                if ([result valueForKey:@"user"] || [result valueForKey:@"quiz"]) {
                    
                    count ++;
                    
                    if (count == DELEGATE.psychologicalAnswerArray.count) {
                         [self gotoMCQStepTwo];
                    }
                   
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
                });
                
                
            }];
        }
        
        
    }
}

-(void)gotoMCQStepTwo{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    MatchingStepTwoViewController *matchingTwoVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"MatchingStepTwoViewController"];
    
    [self.navigationController pushViewController:matchingTwoVC animated:YES];
}

-(void)getPsychologicalQuestion{
    
    DELEGATE.psychologicalQuizArray = [[NSMutableArray alloc] init];
    DELEGATE.psychologicalAnswerArray = [[NSMutableArray alloc] init];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] quizPsychologicalWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
       
        if (result.count>1) {
            
            NSMutableArray *questionArray = [result mutableCopy];
            
            for (int i = 0; i<questionArray.count; i++) {
                
                NSDictionary *dic = [questionArray objectAtIndex:i];
                
                 NSArray *answer_type  = [dic valueForKey:@"answer_type"];
                 NSString *idType  = [dic valueForKey:@"id"];
                 NSString *question  = [dic valueForKey:@"question"];
                // NSString *match_type  = [dic valueForKey:@"match_type"];
               //  NSString *quiz_type  = [dic valueForKey:@"quiz_type"];
                
                
                NSMutableArray *optionArray = [[NSMutableArray alloc] init];
                for (int j=0; j<answer_type.count; j++) {
                    
                    // NSNumber *value = [[answer_type objectAtIndex:j] objectAtIndex:0];
                    NSString *str = [[answer_type objectAtIndex:j] objectAtIndex:1];
                    
                    NSString *optionValue  = [NSString stringWithFormat:@"Option -%@",str];
                    [optionArray addObject:optionValue];
                }
                
                NSMutableDictionary *mainDic = [[NSMutableDictionary alloc] init];
                
                [mainDic setValue:idType forKey:@"QUESTION_ID"];
                [mainDic setValue:question forKey:@"QUESTION"];
                [mainDic setValue:optionArray forKey:@"OPTION"];
                
                [DELEGATE.psychologicalQuizArray addObject:mainDic];
                
               
            }
            
             [self quizLoadOnTableView];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
    
}

-(void)quizLoadOnTableView{
    
    for (int i = 0; i<DELEGATE.psychologicalQuizArray.count; i++) {
        [DELEGATE.psychologicalAnswerArray addObject:[NSNumber numberWithInteger:-1]];
    }
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    
    
    [[self.pageController view] setFrame:CGRectMake(0, 0 , _quizView.frame.size.width, _quizView.frame.size.height+10)];
    
    
    PageViewController *initialViewController = [self viewControllerAtIndex:0];
    
    viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    [self addChildViewController:self.pageController];
    [_quizView addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    
    [self manualPagingDisable];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Pager

- (PageViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    contoller = [[PageViewController alloc] initWithNibName:@"PageViewController" bundle:nil];
    contoller.index=index;
    
    [self updatQuizLabel:index+1];
    return contoller;
}



- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(PageViewController *)viewController index];
    
    index++;
    
    if (index == DELEGATE.psychologicalQuizArray.count) {
        return nil;
    }
    
    pageNo = index;
    NSLog(@"index = %ld",index);
    
    return [self viewControllerAtIndex:index];
    
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    
    NSUInteger index = [(PageViewController *)viewController index];
    
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    pageNo = index;
    
    return [self viewControllerAtIndex:index];
    
}


- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed){return;}
    
}

#pragma mark-
#pragma mark - Actin

- (void)changePage:(UIPageViewControllerNavigationDirection)direction{
    
    if (direction == UIPageViewControllerNavigationDirectionForward){
        pageNo++;
    }else {
        pageNo--;
    }
    
    if (pageNo<0) {
        pageNo = 0;
    }
    if (pageNo>DELEGATE.psychologicalQuizArray.count-1) {
        pageNo = DELEGATE.psychologicalQuizArray.count-1;
    }
    
    PageViewController *viewController = [self  viewControllerAtIndex:pageNo];
    if (viewController == nil) {
        return;
    }
    
    [self.pageController setViewControllers:@[viewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

-(IBAction)leftButtonAction:(UIButton *)button{
    
    [self changePage:UIPageViewControllerNavigationDirectionReverse];
}

-(IBAction)rightButtonAction:(UIButton *)button{
    
    [self changePage:UIPageViewControllerNavigationDirectionForward];
}

-(void)updatQuizLabel:(NSInteger)questionNumber{
    
    NSString *quizText = [NSString stringWithFormat:@"%02ld of %02ld",questionNumber,DELEGATE.psychologicalQuizArray.count];
 
    NSMutableAttributedString *colorText = [[NSMutableAttributedString alloc] initWithString:quizText];
    [colorText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:178/255.0 green:96/255.0 blue:212/255.0 alpha:1] range:NSMakeRange(0,2)];
  
    _quizLabel.attributedText = colorText;
}

@end
