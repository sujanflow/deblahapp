//
//  BasicInfoViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasicInfoViewController : UIViewController


@property (nonatomic, strong) UIImage *profileImage;


@property (nonatomic, weak) IBOutlet UITextField *firstNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@property (nonatomic, weak) IBOutlet UITextField *occupationTextField;



@property (nonatomic, weak) IBOutlet UIImageView *genderImageView;
@property (nonatomic, weak) IBOutlet UIImageView *religionImageView;
@property (nonatomic, weak) IBOutlet UIImageView *bloodImageView;
@property (nonatomic, weak) IBOutlet UIImageView *beautyImageView;
@property (nonatomic, weak) IBOutlet UIImageView *skinImageView;
@property (nonatomic, weak) IBOutlet UIImageView *livingImageView;
@property (nonatomic, weak) IBOutlet UIImageView *triveImageView;
@property (nonatomic, weak) IBOutlet UIImageView *educationImageView;

@property (nonatomic, weak) IBOutlet UIImageView *heightTImageView;
@property (nonatomic, weak) IBOutlet UIImageView *weightImageView;

@property (nonatomic, weak) IBOutlet UIImageView *birthImageView;
@property (nonatomic, weak) IBOutlet UIButton *birthButton;






@end
