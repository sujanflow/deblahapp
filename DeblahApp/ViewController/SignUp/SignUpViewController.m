//
//  SignUpViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "SignUpViewController.h"
#import "BasicInfoViewController.h"
#import "SoulmateInfoViewController.h"
#import "MatchingStepOneViewController.h"

@interface SignUpViewController ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UITapGestureRecognizer *tapGesture;
    
    UIImage *chosenImage;
    
}
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHide:)];
    [self.view addGestureRecognizer:tapGesture];
    
    
    _nameTextField.delegate = self;
    _emailTextField.delegate = self;
    _passwordTextField.delegate = self;
    _confirmPasswordTextField.delegate = self;
    
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        _backButton.frame = CGRectMake(10, 30, 44, 44);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-
#pragma mark- Button Action

-(BOOL)inputValidate{
    
    if (_nameTextField.text.length == 0 || _emailTextField.text.length == 0 || _passwordTextField.text.length == 0 || _confirmPasswordTextField.text.length == 0) {
        [DELEGATE makeFillupPopUp:@"Fill up TextField"];
        return NO;
    }
    
    if ([AppSupport mailAuthentication:_emailTextField.text] == NO) {
        [DELEGATE makeFillupPopUp:@"Invalide Mail"];
        return NO;
    }
    
    
    if (_passwordTextField.text != _confirmPasswordTextField.text) {
        [DELEGATE makeFillupPopUp:@"Password doesnt Match"];
        return NO;
    }
    
    if (chosenImage == nil) {
        [DELEGATE makeFillupPopUp:@"Choose a Profile Image"];
        return NO;
    }
    
    if (_termButton.tag == 0) {
        [DELEGATE makeFillupPopUp:@"Select Terms Condition"];
        return NO;
    }
    
    return YES;
}

-(IBAction)backButtonAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)termButtonAction:(id)sender{
    
    if (_termButton.tag == 0) {
        
        [_termButton setImage:[UIImage imageNamed:@"sign-tick-on"] forState:UIControlStateNormal];
        _termButton.tag = 1;
        
    }else{
        
        [_termButton setImage:[UIImage imageNamed:@"sign-tick-off"] forState:UIControlStateNormal];
        _termButton.tag = 0;
    }
    
}

-(IBAction)profilePictureButtonAction :(UIButton*)button{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}


-(IBAction)submitButtonAction:(id)sender{
    
    [self keyBoardHide:nil];
    
    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }
    
    if ([self inputValidate]) {
        
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];
        
        [infoDic setValue:_nameTextField.text forKey:@"user_name"];
        [infoDic setValue:_emailTextField.text forKey:@"email"];
        [infoDic setValue:_passwordTextField.text forKey:@"password1"];
        [infoDic setValue:_confirmPasswordTextField.text forKey:@"password2"];
        [infoDic setValue:@"true" forKey:@"terms_conditions"];
        
        [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
        
        [[APIClient sharedInstance] signUpWithInfo:infoDic andImage:chosenImage WithComplisionBlock:^(NSDictionary *result) {
            
            NSLog(@"%@",result);
            
            if ([result valueForKey:@"token"]) {
                
                [[NSUserDefaults standardUserDefaults] setValue:[result valueForKey:@"token"] forKey:@"TOKEN"];
                
                NSDictionary *dic = [result valueForKey:@"user"];
                
                 NSString *imageUrl = [dic valueForKey:@"profile_image"];
               
                [[NSUserDefaults standardUserDefaults] setValue:_emailTextField.text forKey:@"MAIL_ID"];
                [[NSUserDefaults standardUserDefaults] setValue:_nameTextField.text forKey:@"USER_NAME"];
                [[NSUserDefaults standardUserDefaults] setValue:imageUrl forKey:@"MY_PROFILE_IMAGE_URL"];
                [[NSUserDefaults standardUserDefaults] setValue:_passwordTextField.text forKey:@"PASSWORD"];

        
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DID_COMPLETE_SIGNUP"];
                
                
                NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
                BasicInfoViewController *basicVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"BasicInfoViewController"];
                basicVC.profileImage = chosenImage;
                [self.navigationController pushViewController:basicVC animated:YES];
                
            }else{
                
                if ([result valueForKey:@"email"]) {
                     [DELEGATE makeFillupPopUp:@"User with this User Email already exists"];
                    
                  //  NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:@"TOKEN"];
                   
//                    if (token.length>1) {
//                        [self getUserDetailsInfo];
//                    }else{
//                          [DELEGATE makeFillupPopUp:@"Please Login first"];
//                    }
                    
                }else{
                    [DELEGATE makeFillupPopUp:@"Error"];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
            });
           
        }];
    }
}




#pragma mark-
#pragma mark- UImage Picker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    _profileButton.clipsToBounds = YES;
    _profileButton.layer.cornerRadius = _profileButton.frame.size.width/2.0;
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    [self.profileButton setImage:chosenImage forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark-
#pragma mark- TextField Delegate

-(void)keyBoardHide:(UITapGestureRecognizer*)sender{
    
    [_nameTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPasswordTextField resignFirstResponder];
  
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self animateTextField:textField up:NO];
}


-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.mainView.frame = CGRectOffset(self.mainView.frame, 0, movement);
    [UIView commitAnimations];
}



-(void)getUserDetailsInfo{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getUserDetailsWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            [[NSUserDefaults standardUserDefaults] setValue:[result valueForKey:@"email"] forKey:@"MAIL_ID"];
            [[NSUserDefaults standardUserDefaults] setValue:[result valueForKey:@"user_name"] forKey:@"USER_NAME"];
            
           // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DID_COMPLETE_SIGNUP"];
            
            
            NSNumber *profileComplete = [result valueForKey:@"profile_complete"];
            NSNumber *soulmateComplete = [result valueForKey:@"soulmate_complete"];
            NSNumber *quizComplete = [result valueForKey:@"quiz_complete"];
            
            
            if ([quizComplete boolValue] == YES && [soulmateComplete boolValue] == YES && [profileComplete boolValue] == YES ) {
                
                [self showMessageCpmpleteAlert];
                [DELEGATE makeLoginViewControllerAsRootViewController];
                
                
            }else if([soulmateComplete boolValue] == YES){
                
                [DELEGATE makeFillupPopUp:@"Quiz not Completed"];
                [self gotoQuizVC];
            }
            else if([profileComplete boolValue] == YES){
                
                [DELEGATE makeFillupPopUp:@"Soumate Requirement not Completed"];
                [self gotoSoulmateVC];
            }else{
                
                [DELEGATE makeFillupPopUp:@"Profile not Completed"];
                [self gotoBasicVC];
            }

        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}

-(void)gotoBasicVC{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    BasicInfoViewController *basicVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"BasicInfoViewController"];
    basicVC.profileImage = chosenImage;
    [self.navigationController pushViewController:basicVC animated:YES];
    
}
-(void)gotoSoulmateVC{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    SoulmateInfoViewController *soulmateVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"SoulmateInfoViewController"];
    
    [self.navigationController pushViewController:soulmateVC animated:YES];
}
-(void)gotoQuizVC{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    MatchingStepOneViewController *matchingOneVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"MatchingStepOneViewController"];
    
    [self.navigationController pushViewController:matchingOneVC animated:YES];
}

-(void)showMessageCpmpleteAlert{
    
     [DELEGATE makeFillupPopUp:@"Completed Profile. Please Login.."];
}
@end
