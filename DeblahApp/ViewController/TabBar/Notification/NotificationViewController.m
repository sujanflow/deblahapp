//
//  NotificationViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"

@interface NotificationViewController ()<UITableViewDelegate,UITableViewDataSource>
{
   
    NSMutableArray *notificationArray;
    
}
@end

@implementation NotificationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationpage) name:@"NOTIFICATIONPAGE" object:nil];

    
    self.view.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.9];
    self.navigationController.navigationBar.hidden = YES;
    
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    
    _emptyView.hidden = YES;
  
    [self setBanner:DELEGATE.bannerNotificationURL];
}

-(void)viewWillAppear:(BOOL)animated{
    
    notificationArray = [[NSMutableArray alloc] init];
    [self getDataFromAPI];
}

-(void)viewWillLayoutSubviews{
    
    
    CGRect adsBannerFrame = self.bannerAdsView.frame;
    adsBannerFrame.origin.y = DEVICE_HEIGHT -49-60- 20;
    self.bannerAdsView.frame = adsBannerFrame;
    
    if (IS_IPHONE_X) {
        
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 81;
        tableFrame.size.height = DEVICE_HEIGHT-81;
        _tableView.frame = tableFrame;
        _emptyView.frame = tableFrame;
        
        CGRect adsBannerFrame = self.bannerAdsView.frame;
        adsBannerFrame.origin.y = DEVICE_HEIGHT -49-60- 20-33;
        self.bannerAdsView.frame = adsBannerFrame;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDataFromAPIFake{
    
    
    for (int i=0; i<10; i++) {
        
        NSMutableDictionary *notificationDictionaray = [[NSMutableDictionary alloc] init];
        
        NSURL *imageUrl = [NSURL URLWithString:@"https://130513-375933-1-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2016/03/Disha-Patani-1.jpg"];
        
        NSString *name = @"Nusrat Jahan Aisha";
        NSString *notificationDate = @"18 May 2018";
        
        [notificationDictionaray setValue:imageUrl forKey:@"IMAGE_URL"];
        [notificationDictionaray setValue:name forKey:@"NAME"];
        [notificationDictionaray setValue:notificationDate forKey:@"NOTIFICATION_DATE"];
        
        
        [notificationArray addObject:notificationDictionaray];
    }
    
}

-(void)getDataFromAPI{
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getNotificationInfoWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if (result.count>0) {
            
            NSMutableArray *resultArray = [result mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSDictionary *dic = [resultArray objectAtIndex:i];
                
              
                NSString *userName = [dic valueForKey:@"name"];
                NSURL *imagUrl = [NSURL URLWithString: [dic valueForKey:@"image"]] ;
                
               // NSString *message = [dic valueForKey:@"message"];
               // message = [message stringByReplacingOccurrencesOfString:@"\n" withString:@""];
              //  message = [message stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                
                NSString *message = [dic valueForKey:@"message"];
                message = [message stringByReplacingOccurrencesOfString:@"Your transaction results:\nThe payment is completed successfully!." withString:@""];
                message = [message stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                
                NSString *lastDate = [dic valueForKey:@"created_at"];
                
                
                NSMutableDictionary *notificationDic = [[NSMutableDictionary alloc] init];
                
                [notificationDic setValue:message forKey:@"MESSAGE"];
                [notificationDic setValue:lastDate forKey:@"NOTIFICATION_DATE"];
                
                [notificationDic setValue:userName forKey:@"NAME"];
                [notificationDic setValue:imagUrl forKey:@"IMAGE_URL"];
             
                
                
                [notificationArray addObject:notificationDic];
                
            }
            
          
            
            [_tableView reloadData];
            
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"No Results Found"];
        }
        
        //empty Check
        if (notificationArray.count>0) {
            _emptyView.hidden = YES;
        }else{
            _emptyView.hidden = NO;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
        
    }];
    
}

#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return notificationArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell"];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil];
        cell = (NotificationTableViewCell *)[topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [notificationArray objectAtIndex:indexPath.row];
    
    NSURL *imageUrl = [dic valueForKey:@"IMAGE_URL"];
    NSString *name = [dic valueForKey:@"NAME"];
    NSString *message = [dic valueForKey:@"MESSAGE"];
    NSString *notificationDate = [dic valueForKey:@"NOTIFICATION_DATE"];
    
    
    NSString *nameAndMessage = [NSString stringWithFormat:@"%@ %@",name,message];
    NSInteger r = name.length;
    
    NSMutableAttributedString *colorText = [[NSMutableAttributedString alloc] initWithString:nameAndMessage];
    [colorText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:131/255.0 green:126/255.0 blue:247/255.0 alpha:1] range:NSMakeRange(0,r)];
    
    cell.nameLabel.attributedText = colorText;
    cell.dateLabel.text = [AppSupport dateParser:notificationDate];
    
    cell.detailsIcon.image = (name.length == 0) ? [UIImage imageNamed:@"notification-payment"] : [UIImage imageNamed:@"notification-love"];
    
    [cell.coverImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"notification-default"] options:SDWebImageRefreshCached];

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 90: 81;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - Notification
-(void)notificationpage{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark - Banner

-(void)setBanner:(NSURL *)url{
    
    [_bannerAdsView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"ads"] options:SDWebImageCacheMemoryOnly completed:^(UIImage *image, NSError *error, SDImageCacheType caheType, NSURL *imageUrl){
        
        if (image && caheType == SDImageCacheTypeNone)
            
        {
            [_bannerAdsView setImage:image];
            _bannerAdsView.alpha = 0.0;
            [UIView animateWithDuration:1.0 animations:^{
                [_bannerAdsView setAlpha:1.0];
            }];
        }
    }];
}
@end
