//
//  HomeViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeTableViewCell.h"

#import "UIImageView+WebCache.h"

#import "OtherProfileViewController.h"
#import "UnlikeViewController.h"

#import "VideoArticleViewController.h"
#import "SearchViewController.h"

@interface HomeViewController ()<UITableViewDelegate, UITableViewDataSource,HomeTableViewCellDelegate>
{
    
    NSInteger soulmateCurrentPage;
    BOOL nextSoulmatePageAvailable;

    
    //-- Nav ---
    VideoArticleViewController *videoArticleVC;
    SearchViewController *searchVC;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addVideoArticle) name:@"VIDEOARTICLE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSearch) name:@"SEARCH" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homepage) name:@"HOMEPAGE" object:nil];
    
    self.view.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    self.navigationController.navigationBar.hidden = YES;
    
    
     _homeTableViewCell.delegate = self;
    UINib *nib = [UINib nibWithNibName:@"HomeTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"homeCell"];
   
  
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    
    _emptyView.hidden = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
 
    DELEGATE.soulmateArray = [[NSMutableArray alloc] init];
    [self getDataFromAPI];
    
    [_tableView reloadData];
  
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 81;
        tableFrame.size.height = DEVICE_HEIGHT-81;
        _tableView.frame = tableFrame;
        _emptyView.frame = tableFrame;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)getDataFromAPI{
    
    nextSoulmatePageAvailable = NO;
    soulmateCurrentPage = 1;
    
    [self getSoulmateList:soulmateCurrentPage];
}

-(void)getSoulmateList: (NSInteger)pageNumber{
    
    NSString *pageNO = [NSString stringWithFormat:@"%ld",(long)pageNumber];
    NSMutableDictionary *soulDic = [[NSMutableDictionary alloc] init];
    [soulDic setValue:pageNO forKey:@"PAGE_NO"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getMySoulmateInfo:soulDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"results"]) {
            
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextSoulmatePageAvailable = YES;
                soulmateCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextSoulmatePageAvailable = NO;
            }
            
            NSMutableArray *resultArray = [[result valueForKey:@"results"] mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSMutableDictionary *dic = [resultArray objectAtIndex:i];
                
              
                NSString *match = [dic valueForKey:@"match"];
        
                NSDictionary *userDic = [dic valueForKey:@"user"];
                NSString *userId = [userDic valueForKey:@"id"];
                NSString *profileImg = [userDic valueForKey:@"profile_image"];
                NSString *userName = [userDic valueForKey:@"user_name"];
                NSString *userBirth = [userDic valueForKey:@"age"];
                
                NSNumber *userLike = [userDic valueForKey:@"liked"];
                NSInteger liked =  [userLike integerValue];
                NSString *like_Status = (liked == 0)? @"NO" : @"YES";
                
                NSNumber *userChat = [userDic valueForKey:@"chat"];
                NSInteger chated =  [userChat integerValue];
                NSString *chat_Status = (chated == 0)? @"NO" : @"YES";

                NSMutableDictionary *soulmateDictionary = [[NSMutableDictionary alloc] init];
                
                [soulmateDictionary setValue:userId forKey:@"ID"];
                [soulmateDictionary setValue:match forKey:@"MATCHING_PERCENTAGE"];
                [soulmateDictionary setValue:profileImg forKey:@"IMAGE_URL"];
                [soulmateDictionary setValue:userName forKey:@"NAME"];
                [soulmateDictionary setValue:userBirth forKey:@"AGE"];
                
                [soulmateDictionary setValue:like_Status forKey:@"LOVE_REACTION"];
                [soulmateDictionary setValue:chat_Status forKey:@"CHAT_REACTION"];

                
                [DELEGATE.soulmateArray addObject:soulmateDictionary];
                
            }
            
            [_tableView reloadData];
            
            
            
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Not Found"];
        }
        
        
        //empty Check
        if (DELEGATE.soulmateArray.count>0) {
            _emptyView.hidden = YES;
        }else{
            _emptyView.hidden = NO;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}
#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return DELEGATE.soulmateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.homeTableViewCell = (HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    self.homeTableViewCell.delegate = self;
    
    self.homeTableViewCell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor whiteColor];
    [self.homeTableViewCell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [DELEGATE.soulmateArray objectAtIndex:indexPath.row];
    
    NSURL *imageUrl = [dic valueForKey:@"IMAGE_URL"];
    NSString *name = [dic valueForKey:@"NAME"];
    NSString *age = [dic valueForKey:@"AGE"];
    NSString *matchingPercentage = [dic valueForKey:@"MATCHING_PERCENTAGE"];
    NSString *loveReaction = [dic valueForKey:@"LOVE_REACTION"];
    NSString *chatReaction = [dic valueForKey:@"CHAT_REACTION"];

    
    NSInteger matchingValue = [matchingPercentage integerValue];
    NSString *finalMatchingStr = [NSString stringWithFormat:@"%ld%%",matchingValue];
    
    

    self.homeTableViewCell.nameLabel.text = name;
    self.homeTableViewCell.ageLabel.text = [NSString stringWithFormat:@"%@ Years",age];
    self.homeTableViewCell.parcentLabel.text = finalMatchingStr;
    
    if ([loveReaction isEqualToString:@"NO"]) {
         [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-off"] forState:UIControlStateNormal];
        self.homeTableViewCell.loveButton.tag = 0;
        
    }else{
         [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-on"] forState:UIControlStateNormal];
          self.homeTableViewCell.loveButton.tag = 1;
        if ([chatReaction isEqualToString:@"YES"]) {
             [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-all"] forState:UIControlStateNormal];
            self.homeTableViewCell.loveButton.tag = 2;
        }
    }
   
    
    [self.homeTableViewCell.coverImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"home-default"] options:SDWebImageRefreshCached];
    
    
    
    if (indexPath.row == DELEGATE.soulmateArray.count-5) {
        if (DELEGATE.isReachable) {
            if (nextSoulmatePageAvailable) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getSoulmateList:soulmateCurrentPage];
                });
            }
        }
    }
    
    
    
    self.homeTableViewCell.layer.shadowColor= [UIColor redColor].CGColor;
    self.homeTableViewCell.layer.shadowRadius = 1.0;
    self.homeTableViewCell.layer.shadowOffset = CGSizeMake(0, 1);
    self.homeTableViewCell.layer.shadowOpacity = 0.1;
    
    return self.homeTableViewCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 550:(IS_IPHONE_5 ? 285 : (IS_IPHONE_6 ? 320:(IS_IPHONE_X ? 320: 353)));
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary *dic = [DELEGATE.soulmateArray objectAtIndex:indexPath.row];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    OtherProfileViewController *otherProfileViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"OtherProfileViewController"];
    otherProfileViewController.userDic = dic;
    otherProfileViewController.fromVC = @"HOME";
    otherProfileViewController.indexPathRow = indexPath.row;
    [self.navigationController pushViewController:otherProfileViewController animated:YES];
    
}



#pragma mark - Cell Delegate

-(void)likeButtonPressedAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = [DELEGATE.soulmateArray objectAtIndex:indexPath.row];
    
    NSString *loveReaction = [dic valueForKey:@"LOVE_REACTION"];
    
    if ([loveReaction isEqualToString:@"NO"]) {
        [self likeAction:(NSMutableDictionary *)dic andIndex:indexPath];
    }else{
        [self unlikeAction:dic andIndex:indexPath.row];
    }
}


-(void)likeAction:(NSMutableDictionary *)dic andIndex:(NSIndexPath *)indexPath{
    
    
    NSString *userId = [dic valueForKey:@"ID"];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] likeOtherProfileWithInfo:nil andID:userId WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            HomeTableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
            
            [cell.loveButton setImage:[UIImage imageNamed:@"home-love-on"] forState:UIControlStateNormal];
            cell.loveButton.tag = 1;
            
            if ([[dic valueForKey:@"CHAT_REACTION"] isEqualToString:@"YES"]) {
                [cell.loveButton setImage:[UIImage imageNamed:@"home-love-all"] forState:UIControlStateNormal];
                cell.loveButton.tag = 2;
            }
            
            
            [dic removeObjectForKey:@"LOVE_REACTION"];
            [dic setValue:@"YES" forKey:@"LOVE_REACTION"];
            
            NSLog(@"%@",dic);
            
            [DELEGATE.soulmateArray replaceObjectAtIndex:indexPath.row withObject:dic];
           
    
        }
        else if ([result valueForKey:@"non_field_errors"]){
            
            [DELEGATE makeFillupPopUp:@"Insufficient points. Please purchase more"];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

-(void)unlikeAction:(NSDictionary *)dic andIndex: (NSInteger)indexPathRow{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    UnlikeViewController *unlikeViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"UnlikeViewController"];
    
    unlikeViewController.userDic = dic;
    unlikeViewController.indexPathRow = indexPathRow;
    unlikeViewController.fromVC = @"HOME";
    
    [self.navigationController pushViewController:unlikeViewController animated:YES];
}


#pragma mark - Notification

-(void)homepage{
    [self.navigationController popToRootViewControllerAnimated:NO];
    [self removeSearchAndVideoArticle];
}

-(void)addSearch{
  
    [self.navigationController popToRootViewControllerAnimated:NO];

    if (searchVC != nil) {
        [searchVC.view removeFromSuperview];
    }
    
    NSString *xibName = IS_DEVICE_IPAD ? @"SearchViewController_iPad" : @"SearchViewController";
    searchVC = [[SearchViewController alloc] initWithNibName:xibName bundle:nil];
    
    
    [self addChildViewController:searchVC];
    searchVC.view.frame = IS_IPHONE_X ? CGRectMake(0, 85, DEVICE_WIDTH, DEVICE_HEIGHT-85-82): CGRectMake(0, 57, DEVICE_WIDTH, DEVICE_HEIGHT-57-49);
    
    [self.view addSubview:searchVC.view];
    
    [searchVC didMoveToParentViewController:self];
    
}

-(void)addVideoArticle{
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    if (videoArticleVC != nil) {
        [videoArticleVC.view removeFromSuperview];
    }
    

    NSString *xibName = IS_DEVICE_IPAD ? @"VideoArticleViewController_iPad" : @"VideoArticleViewController";
    videoArticleVC = [[VideoArticleViewController alloc] initWithNibName:xibName bundle:nil];
    
    [self addChildViewController:videoArticleVC];
    videoArticleVC.view.frame = IS_IPHONE_X ? CGRectMake(0, 85, DEVICE_WIDTH, DEVICE_HEIGHT-85-82): CGRectMake(0, 57, DEVICE_WIDTH, DEVICE_HEIGHT-57-49);
    [self.view addSubview:videoArticleVC.view];
    [videoArticleVC didMoveToParentViewController:self];
}

-(void)removeSearchAndVideoArticle{
    
    if (videoArticleVC != nil) {
        [videoArticleVC.view removeFromSuperview];
    }
    
    if (searchVC != nil) {
        [searchVC.view removeFromSuperview];
    }
}
@end
