//
//  FavoutiteViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "FavoutiteViewController.h"
#import "HomeTableViewCell.h"

#import "OtherProfileViewController.h"
#import "UnlikeViewController.h"



@interface FavoutiteViewController ()<UITableViewDelegate, UITableViewDataSource,HomeTableViewCellDelegate>
{
    NSMutableArray *receivedSoulmateArray;
    NSMutableArray *sentSoulmateArray;
    
    BOOL loadReceivedSoulmateTableView;
    
    BOOL nextReceivedPageAvailable;
    BOOL nextSentPageAvailable;
    
    NSInteger receivedCurrentPage;
    NSInteger sentCurrentPage;
    
}
@end

@implementation FavoutiteViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favouritepage) name:@"FAVOURITEPAGE" object:nil];

    
    self.view.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    self.navigationController.navigationBar.hidden = YES;
    
    
    _homeTableViewCell.delegate = self;
    UINib *nib = [UINib nibWithNibName:@"HomeTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"homeCell"];
    
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionHeaderHeight = IS_DEVICE_IPAD ? 60 : 40;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
   
    _emptyView.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    
    receivedSoulmateArray = [[NSMutableArray alloc] init];
    sentSoulmateArray = [[NSMutableArray alloc] init];
    
    receivedCurrentPage = 1;
    sentCurrentPage = 1;
    
    [self receivedButtonAction:nil];
    [self getDataFromAPI];
    
    
    
    [self updateUI];
}

-(void)updateUI{
    
    [_receivedButton setTitle:AMLocalizedString(@"Received Request",nil) forState:UIControlStateNormal];
    [_sentButton setTitle:AMLocalizedString(@"Sent Request",nil) forState:UIControlStateNormal];
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 81;
        tableFrame.size.height = DEVICE_HEIGHT-81;
        _tableView.frame = tableFrame;
        
        
        CGRect emFrame = _tableView.frame;
        emFrame.origin.y = 81+40;
        emFrame.size.height = DEVICE_HEIGHT-81-40;
        _emptyView.frame = emFrame;
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)receivedButtonAction:(UIButton *)button{
    
    loadReceivedSoulmateTableView = YES;
    [_receivedButton setBackgroundImage:[UIImage imageNamed:@"purpleButtonIconSmall"] forState:UIControlStateNormal];
    [_sentButton setBackgroundImage:[UIImage imageNamed:@"grayButtonIconSmall"] forState:UIControlStateNormal];
    
    [_tableView reloadData];
    
    
      //empty Check
    _emptyLabel.text = @"You do not have any request received yet";
  
    if (receivedSoulmateArray.count>0) {
        _emptyView.hidden = YES;
    }else{
        _emptyView.hidden = NO;
    }
    
}
-(IBAction)sentButtonAction:(UIButton *)button{
    
    loadReceivedSoulmateTableView = NO;
    [_receivedButton setBackgroundImage:[UIImage imageNamed:@"grayButtonIconSmall"] forState:UIControlStateNormal];
    [_sentButton setBackgroundImage:[UIImage imageNamed:@"purpleButtonIconSmall"] forState:UIControlStateNormal];
    
    [_tableView reloadData];
    
    
     //empty Check
    _emptyLabel.text = @"You do not have any request sent yet";
    if (sentSoulmateArray.count>0) {
        _emptyView.hidden = YES;
    }else{
        _emptyView.hidden = NO;
    }
    
}

-(void)getDataFromAPI{
    
    [self getReceivedDetails:receivedCurrentPage];
    [self getSentDetails:sentCurrentPage];
}

-(void)getReceivedDetails:(NSInteger)pageNumber{
    
    NSString *pageNO = [NSString stringWithFormat:@"%ld",(long)pageNumber];
    NSMutableDictionary *receivedDic = [[NSMutableDictionary alloc] init];
    [receivedDic setValue:pageNO forKey:@"PAGE_NO"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getSoulmateReceivedRequestInfo:receivedDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"results"]) {
            
            
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextReceivedPageAvailable = YES;
                receivedCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextReceivedPageAvailable = NO;
            }
            
            NSMutableArray *resultArray = [[result valueForKey:@"results"] mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSDictionary *receivedDic = [resultArray objectAtIndex:i];
                
                NSString *userId = [receivedDic valueForKey:@"id"];
                NSString *profileImg = [receivedDic valueForKey:@"profile_image"];
                NSString *userName = [receivedDic valueForKey:@"user_name"];
                NSString *userBirth = [receivedDic valueForKey:@"age"];
                
                NSNumber *userLike = [receivedDic valueForKey:@"liked"];
                NSInteger liked =  [userLike integerValue];
                NSString *like_Status = (liked == 0)? @"NO" : @"YES";
                
                NSNumber *userChat = [receivedDic valueForKey:@"chat"];
                NSInteger chated =  [userChat integerValue];
                NSString *chat_Status = (chated == 0)? @"NO" : @"YES";
                
                
                if (profileImg == [NSNull null]) {
                    profileImg = @"www.google.com";
                }
                if (userBirth == [NSNull null]) {
                    userBirth = @"0";
                }
                
                
                
                
                NSMutableDictionary *receivedDictionary = [[NSMutableDictionary alloc] init];
                
                [receivedDictionary setValue:userId forKey:@"ID"];
                [receivedDictionary setValue:profileImg forKey:@"IMAGE_URL"];
                [receivedDictionary setValue:userName forKey:@"NAME"];
                [receivedDictionary setValue:userBirth forKey:@"AGE"];
                
                [receivedDictionary setValue:like_Status forKey:@"LOVE_REACTION"];
                [receivedDictionary setValue:chat_Status forKey:@"CHAT_REACTION"];
                
                
                [receivedSoulmateArray addObject:receivedDictionary];
            }
            
            
          
            
            [_tableView reloadData];
            
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Not Found"];
        }
        
        
        //empty Check
        if (receivedSoulmateArray.count>0) {
            _emptyView.hidden = YES;
        }else{
            _emptyView.hidden = NO;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

-(void)getSentDetails:(NSInteger)pageNumber{
    
    NSString *pageNO = [NSString stringWithFormat:@"%ld",(long)pageNumber];
    NSMutableDictionary *sentDic = [[NSMutableDictionary alloc] init];
    [sentDic setValue:pageNO forKey:@"PAGE_NO"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getSoulmateSentRequestInfo:sentDic  WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"results"]) {
            
            
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextSentPageAvailable = YES;
                sentCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextSentPageAvailable = NO;
            }
            
            NSMutableArray *resultArray = [[result valueForKey:@"results"] mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSDictionary *sentDic = [resultArray objectAtIndex:i];
                
                NSString *userId = [sentDic valueForKey:@"id"];
                NSString *profileImg = [sentDic valueForKey:@"profile_image"];
                NSString *userName = [sentDic valueForKey:@"user_name"];
                NSString *userBirth = [sentDic valueForKey:@"age"];
                
                NSNumber *userLike = [sentDic valueForKey:@"liked"];
                NSInteger liked =  [userLike integerValue];
                NSString *like_Status = (liked == 0)? @"NO" : @"YES";
                
                NSNumber *userChat = [sentDic valueForKey:@"chat"];
                NSInteger chated =  [userChat integerValue];
                NSString *chat_Status = (chated == 0)? @"NO" : @"YES";
                
                
                if (profileImg == [NSNull null]) {
                    profileImg = @"www.google.com";
                }
                if (userBirth == [NSNull null]) {
                    userBirth = @"0";
                }
                
                
                
                
                NSMutableDictionary *sentDictionary = [[NSMutableDictionary alloc] init];
                
                [sentDictionary setValue:userId forKey:@"ID"];
                [sentDictionary setValue:profileImg forKey:@"IMAGE_URL"];
                [sentDictionary setValue:userName forKey:@"NAME"];
                [sentDictionary setValue:userBirth forKey:@"AGE"];
                
                [sentDictionary setValue:like_Status forKey:@"LOVE_REACTION"];
                [sentDictionary setValue:chat_Status forKey:@"CHAT_REACTION"];
                
                
                [sentSoulmateArray addObject:sentDictionary];
            }
            
            
//            //empty Check
//            if (sentSoulmateArray.count>0) {
//                _emptyView.hidden = YES;
//            }else{
//                _emptyView.hidden = NO;
//            }
//
            
            [_tableView reloadData];
            
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Not Found"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}



#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return loadReceivedSoulmateTableView ? receivedSoulmateArray.count : sentSoulmateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.homeTableViewCell = (HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"homeCell"];
    self.homeTableViewCell.delegate = self;

    self.homeTableViewCell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self.homeTableViewCell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic;
    
    if (loadReceivedSoulmateTableView) {
        dic = [receivedSoulmateArray objectAtIndex:indexPath.row];
    }else{
       dic = [sentSoulmateArray objectAtIndex:indexPath.row];
    }
    
    NSURL *imageUrl = [dic valueForKey:@"IMAGE_URL"];
    NSString *name = [dic valueForKey:@"NAME"];
    NSString *age = [dic valueForKey:@"AGE"];
    
    NSString *loveReaction = [dic valueForKey:@"LOVE_REACTION"];
    NSString *chatReaction = [dic valueForKey:@"CHAT_REACTION"];
    
    
    self.homeTableViewCell.nameLabel.text = name;
    self.homeTableViewCell.ageLabel.text = [NSString stringWithFormat:@"%@ Years",age];
    self.homeTableViewCell.parcentLabel.hidden = YES;
    
    if ([loveReaction isEqualToString:@"NO"]) {
        [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-off"] forState:UIControlStateNormal];
        self.homeTableViewCell.loveButton.tag = 0;
        
    }else{
        [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-on"] forState:UIControlStateNormal];
          self.homeTableViewCell.loveButton.tag = 1;
        if ([chatReaction isEqualToString:@"YES"]) {
            [self.homeTableViewCell.loveButton setImage:[UIImage imageNamed:@"home-love-all"] forState:UIControlStateNormal];
            self.homeTableViewCell.loveButton.tag = 2;
        }
    }
    
    
    [self.homeTableViewCell.coverImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"home-default"] options:SDWebImageRefreshCached];
    
    
    self.homeTableViewCell.layer.shadowColor= [UIColor redColor].CGColor;
    self.homeTableViewCell.layer.shadowRadius = 1.0;
    self.homeTableViewCell.layer.shadowOffset = CGSizeMake(0, 1);
    self.homeTableViewCell.layer.shadowOpacity = 0.1;
    
    
    if (loadReceivedSoulmateTableView) {
        
        if (indexPath.row == receivedSoulmateArray.count-5) {
            if (DELEGATE.isReachable) {
                if (nextReceivedPageAvailable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getReceivedDetails:receivedCurrentPage];
                    });
                }
            }
        }
        
    }else{
        
        if (indexPath.row == sentSoulmateArray.count-5) {
            if (DELEGATE.isReachable) {
                if (nextSentPageAvailable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getSentDetails:sentCurrentPage];
                    });
                }
            }
        }
        
    }
    
    return self.homeTableViewCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 550:(IS_IPHONE_5 ? 285 : (IS_IPHONE_6 ? 320:(IS_IPHONE_X ? 320: 353)));
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary *dic = loadReceivedSoulmateTableView ?  [receivedSoulmateArray objectAtIndex:indexPath.row] : [sentSoulmateArray objectAtIndex:indexPath.row];
    NSString *userID = [dic valueForKey:@"ID"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getOtherProfileDetailsWithInfo:userID WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            NSNumber *profileComplete = [result valueForKey:@"profile_complete"];
            NSNumber *soulmateComplete = [result valueForKey:@"soulmate_complete"];
            NSNumber *quizComplete = [result valueForKey:@"quiz_complete"];
            
            if ([profileComplete boolValue] && [soulmateComplete boolValue] && [quizComplete boolValue] ) {
                
                NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
                
                OtherProfileViewController *otherProfileViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"OtherProfileViewController"];
                
                otherProfileViewController.userDic = loadReceivedSoulmateTableView ?  [receivedSoulmateArray objectAtIndex:indexPath.row] : [sentSoulmateArray objectAtIndex:indexPath.row] ;
                
                otherProfileViewController.fromVC =  loadReceivedSoulmateTableView ? @"RECEIVED" : @"SENT";
                
                otherProfileViewController.indexPathRow = indexPath.row;
                
                [self.navigationController pushViewController:otherProfileViewController animated:YES];
                
                
            }else{
                
                [DELEGATE makeFillupPopUp:@"Profile Not Completed"];
            }
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

#pragma mark - Cell Delegate

-(void)likeButtonPressedAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = loadReceivedSoulmateTableView ?  [receivedSoulmateArray objectAtIndex:indexPath.row] : [sentSoulmateArray objectAtIndex:indexPath.row] ;
    
    NSString *loveReaction = [dic valueForKey:@"LOVE_REACTION"];
    
    if ([loveReaction isEqualToString:@"NO"]) {
        [self likeAction:(NSMutableDictionary *)dic andIndex:indexPath];
    }else{
        [self unlikeAction:dic andIndex:indexPath.row];
    }
}


-(void)likeAction:(NSMutableDictionary *)dic andIndex: (NSIndexPath *)indexPath{
    
    
    NSString *userId = [dic valueForKey:@"ID"];
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] likeOtherProfileWithInfo:nil andID:userId WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            HomeTableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
            
            [cell.loveButton setImage:[UIImage imageNamed:@"home-love-on"] forState:UIControlStateNormal];
            cell.loveButton.tag = 1;
            
            if ([[dic valueForKey:@"CHAT_REACTION"] isEqualToString:@"YES"]) {
                [cell.loveButton setImage:[UIImage imageNamed:@"home-love-all"] forState:UIControlStateNormal];
                cell.loveButton.tag = 2;
            }
            
            [dic removeObjectForKey:@"LOVE_REACTION"];
            [dic setValue:@"YES" forKey:@"LOVE_REACTION"];
            
            NSLog(@"%@",dic);
            
            if (loadReceivedSoulmateTableView == YES) {
                [receivedSoulmateArray replaceObjectAtIndex:indexPath.row withObject:dic];
            }else{
                [sentSoulmateArray replaceObjectAtIndex:indexPath.row withObject:dic];
            }
            
        }
        else if ([result valueForKey:@"non_field_errors"]){
            
            [DELEGATE makeFillupPopUp:@"Insufficient points. Please purchase more"];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
 
        
    }];
    
}

-(void)unlikeAction:(NSDictionary *)dic andIndex: (NSInteger)indexPathRow{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    UnlikeViewController *unlikeViewController = [mainStoryboard instantiateViewControllerWithIdentifier: @"UnlikeViewController"];
    
    unlikeViewController.userDic = loadReceivedSoulmateTableView ?  [receivedSoulmateArray objectAtIndex:indexPathRow] : [sentSoulmateArray objectAtIndex:indexPathRow] ;
    unlikeViewController.indexPathRow = indexPathRow;
    unlikeViewController.fromVC = loadReceivedSoulmateTableView ? @"RECEIVED" : @"SENT";
    
    [self.navigationController pushViewController:unlikeViewController animated:YES];
}


#pragma mark - Notification
-(void)favouritepage{
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end
