//
//  FavoutiteViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"

@interface FavoutiteViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIButton *receivedButton;
@property (nonatomic, weak) IBOutlet UIButton *sentButton;

@property (strong, nonatomic) HomeTableViewCell *homeTableViewCell;

@property (nonatomic, weak) IBOutlet UIView *emptyView;
@property (nonatomic, weak) IBOutlet UILabel *emptyLabel;

@end
