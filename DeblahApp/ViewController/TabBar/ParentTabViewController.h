//
//  ParentTabViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentTabViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *navBar;
@property (nonatomic, weak) IBOutlet UIButton *videoArticleButton;
@property (nonatomic, weak) IBOutlet UIImageView *titleImageView;



@property (nonatomic, weak) IBOutlet UIView *searchView;
@property(nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UIButton *searchButton;
@property (nonatomic, weak) IBOutlet UIButton *searchCancelButton;


@property (strong,nonatomic) UITabBarController *tabBarController;

@end
