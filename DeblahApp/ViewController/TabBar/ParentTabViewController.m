//
//  ParentTabViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "ParentTabViewController.h"

#import "HomeViewController.h"
#import "ChatViewController.h"
#import "FavoutiteViewController.h"
#import "NotificationViewController.h"
#import "MyProfileViewController.h"


@interface ParentTabViewController ()<UITextFieldDelegate,UITabBarControllerDelegate>
{
    //------line--------//
    UILabel *navline;
    

}
@end

@implementation ParentTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(allTabItemDeselected) name:@"TAB_DESELECT" object:nil];

    
    self.navigationController.navigationBar.hidden = YES;
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    
    [self addChildViewController:self.tabBarController];
    [self.tabBarController willMoveToParentViewController:nil];
    
    [self tabBarSetupAppearence];
    [self setupTabbarController];
    
    [self.view addSubview:self.tabBarController.view];
    
    [self.view bringSubviewToFront:_navBar];
    
    
    [self setupSearchView];
    
}

-(void)viewWillAppear:(BOOL)animated{
   
    [self getBannerDetails];
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        CGRect navFrame = _navBar.frame;
        navFrame.size.height = 85;
        _navBar.frame = navFrame;
    }
    [self addLine];
  
}

-(void)addLine{
    
    if (navline != nil) {
        [navline removeFromSuperview];
    }
   
    navline = [[UILabel alloc] initWithFrame:CGRectMake(0, _navBar.frame.size.height-0.3, DEVICE_WIDTH, 0.3)];
    [_navBar addSubview:navline];
    navline.backgroundColor = [UIColor colorWithRed:219/255.0 green:219/255.0 blue:219/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Search View

-(void)setupSearchView{
    
    _searchCancelButton.alpha = 0.0;
    
    self.searchTextField.delegate = self;
    self.searchView.backgroundColor = [UIColor clearColor];
    self.searchView.alpha = 0.0;
    
    CGFloat width = 321;
    CGFloat height = 30;
    
    
    if (IS_DEVICE_IPAD) {
        width = 480;
        height = 33;
    }else{
        width = IS_IPHONE_5 ? 200 : (IS_IPHONE_6 ? 270 : (IS_IPHONE_X ? 270 : 321));
        height = IS_IPHONE_5 ? 25 : (IS_IPHONE_6 ? 28 : (IS_IPHONE_X ? 28 : 30));
    }
    
    self.searchView.frame = CGRectMake(0, 13, width, height);
    self.searchView.center = CGPointMake(DEVICE_WIDTH/2.0, IS_IPHONE_X ? 56.5+6 : 28.5+6);
    
    self.searchView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
   
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    NSString *searchText = textField.text;
    
    if (searchText.length>0) {
    
        DELEGATE.SEARCH_TEXT = searchText;
        
        [self.tabBarController setSelectedIndex:0];
        DELEGATE.TABBAR_SELECTED_INDEX = 0;
        [self allTabItemDeselected];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SEARCH" object:nil];
        
        [self hideSearchView];
    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

-(IBAction)searchCancelAction:(UIButton *)sender{
    
    _searchTextField.text = @"";
    [_searchTextField resignFirstResponder];
    [self hideSearchView];
}
-(void)showSearchView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _titleImageView.alpha = 0.0;
        _searchView.alpha = 1.0;
        _searchButton.alpha = 0.0;
        
        _searchCancelButton.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
        
    }];
}
-(void)hideSearchView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _titleImageView.alpha = 1.0;
        _searchView.alpha = 0.0;
        _searchButton.alpha = 1.0;
        
        _searchCancelButton.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
    }];
}




#pragma mark -
#pragma mark - Tab Bar Details

-(void)tabBarSetupAppearence
{
    
    [[UITabBar appearance] setTranslucent:YES];
    [[UITabBar appearance] setBarTintColor:NAVBAR_BAR_TINT_COLOR];
    [[UITabBar appearance] setTintColor:NAVBAR_TINT_COLOR];
    
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:NAVBAR_BAR_TINT_COLOR];
    [[UINavigationBar appearance] setTintColor:NAVBAR_TINT_COLOR];
    
    
    /*
     [[UITabBarItem appearance] setTitleTextAttributes:@{
     NSFontAttributeName : [UIFont systemFontOfSize:10]
     } forState:UIControlStateNormal];
     */
}


-(void)setupTabbarController{
    
    NSMutableArray *localViewControllersArray = [[NSMutableArray alloc] initWithCapacity:5];
    
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    
    HomeViewController *homeVC = (HomeViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
    UINavigationController *firstNavVC = [[UINavigationController alloc] initWithRootViewController:homeVC];
    firstNavVC.tabBarItem.image = [UIImage imageNamed:@"home-deselected"];
    [firstNavVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"home-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    firstNavVC.tabBarItem.imageInsets = IS_DEVICE_IPAD ?  UIEdgeInsetsMake(0, 0, 0, 0) : UIEdgeInsetsMake(6, 0, -6, 0);
    
    
    
    ChatViewController *chatVC = (ChatViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ChatViewController"];
    UINavigationController *secondNavVC = [[UINavigationController alloc] initWithRootViewController:chatVC];
    secondNavVC.tabBarItem.image = [UIImage imageNamed:@"chat-deselected"];
    [secondNavVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"chat-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    secondNavVC.tabBarItem.imageInsets =  IS_DEVICE_IPAD ?  UIEdgeInsetsMake(0, 0, 0, 0) : UIEdgeInsetsMake(6, 0, -6, 0);
    
    
    FavoutiteViewController *favouriteVC = (FavoutiteViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"FavoutiteViewController"];
    UINavigationController *thirdNavVC = [[UINavigationController alloc] initWithRootViewController:favouriteVC];
    thirdNavVC.tabBarItem.image = [UIImage imageNamed:@"favourite-deselected"];
    [thirdNavVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"favourite-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    thirdNavVC.tabBarItem.imageInsets =  IS_DEVICE_IPAD ?  UIEdgeInsetsMake(0, 0, 0, 0) : UIEdgeInsetsMake(6, 0, -6, 0);
    
    
    
    NotificationViewController *notificationVC = (NotificationViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"NotificationViewController"];
    UINavigationController *forthNavVC = [[UINavigationController alloc] initWithRootViewController:notificationVC];
    forthNavVC.tabBarItem.image = [UIImage imageNamed:@"notification-deselected"];
    [forthNavVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"notification-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    forthNavVC.tabBarItem.imageInsets =  IS_DEVICE_IPAD ?  UIEdgeInsetsMake(0, 0, 0, 0) : UIEdgeInsetsMake(6, 0, -6, 0);
    
    
    MyProfileViewController *profileVC = (MyProfileViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MyProfileViewController"];
    UINavigationController *fifthNavVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    fifthNavVC.tabBarItem.image = [UIImage imageNamed:@"profile-deselected"];
    [fifthNavVC.tabBarItem setSelectedImage:[[UIImage imageNamed:@"settings-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    fifthNavVC.tabBarItem.imageInsets =  IS_DEVICE_IPAD ?  UIEdgeInsetsMake(0, 0, 0, 0) : UIEdgeInsetsMake(6, 0, -6, 0);
    
    
    [localViewControllersArray addObject:firstNavVC];
    [localViewControllersArray addObject:secondNavVC];
    [localViewControllersArray addObject:thirdNavVC];
    [localViewControllersArray addObject:forthNavVC];
    [localViewControllersArray addObject:fifthNavVC];
    
    self.tabBarController.viewControllers = localViewControllersArray;
    self.tabBarController.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self.tabBarController setSelectedIndex:0];
    DELEGATE.TABBAR_SELECTED_INDEX = 0;
    
    [[UITabBar appearance] setTintColor:[UIColor grayColor]];
    if (@available(iOS 10.0, *)) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor grayColor]];
    }
   
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    [self removeVideoandArticleView];
    
    [[UITabBar appearance] setTintColor:[UIColor grayColor]];
    if (@available(iOS 10.0, *)) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor grayColor]];
    }
    
    if (tabBarController.selectedIndex == 0) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HOMEPAGE" object:nil];
        
        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"home-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
    }else if (tabBarController.selectedIndex == 1){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CHATPAGE" object:nil];

        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"chat-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else if (tabBarController.selectedIndex == 2){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FAVOURITEPAGE" object:nil];

        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"favourite-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else if (tabBarController.selectedIndex == 3){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIFICATIONPAGE" object:nil];

        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"notification-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else if (tabBarController.selectedIndex == 4){
        
        if ( DELEGATE.TABBAR_SELECTED_INDEX == 4) {

            if ( [DELEGATE.TAB_IMAGE_NAME isEqualToString:@"SETTINGS_DESELECT"]) {

                [[NSNotificationCenter defaultCenter] postNotificationName:@"SETTINGSPAGE" object:nil];
                [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"settings-selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                DELEGATE.TAB_IMAGE_NAME = @"SETTINGS_SELECT";

            }else{

                [[NSNotificationCenter defaultCenter] postNotificationName:@"PROFILEPAGE" object:nil];
                
                [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"settings-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                
                DELEGATE.TAB_IMAGE_NAME = @"SETTINGS_DESELECT";
            }

        }else{

            [[NSNotificationCenter defaultCenter] postNotificationName:@"PROFILEPAGE" object:nil];

            [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"settings-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

            DELEGATE.TAB_IMAGE_NAME = @"SETTINGS_DESELECT";
        }
    }
    
    
    DELEGATE.TABBAR_SELECTED_INDEX = tabBarController.selectedIndex;
}

-(void)allTabItemDeselected{
    
    [[UITabBar appearance] setTintColor:[UIColor grayColor]];
    if (@available(iOS 10.0, *)) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor grayColor]];
    }
    
    if (DELEGATE.TABBAR_SELECTED_INDEX == 0) {
        
        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"home-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
    }else if (DELEGATE.TABBAR_SELECTED_INDEX == 1){
        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"chat-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else if (DELEGATE.TABBAR_SELECTED_INDEX == 2){
        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"favourite-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else if (DELEGATE.TABBAR_SELECTED_INDEX == 3){
        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"notification-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    else if (DELEGATE.TABBAR_SELECTED_INDEX == 4){
        [self.tabBarController.tabBar.selectedItem setSelectedImage:[[UIImage imageNamed:@"profile-deselected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        DELEGATE.TAB_IMAGE_NAME = @"PROFILE_DESELECT";
    }
    
}



#pragma mark -
#pragma mark - Nav Bar

-(IBAction)navLeftButtonAction:(UIButton *)button{
    
    [_videoArticleButton setImage:[UIImage imageNamed:@"navPlay-selected"] forState:UIControlStateNormal];
 
    [[NSNotificationCenter defaultCenter] postNotificationName:@"VIDEOARTICLE" object:nil];
    
    [self.tabBarController setSelectedIndex:0];
    DELEGATE.TABBAR_SELECTED_INDEX = 0;
    [self allTabItemDeselected];

}

-(IBAction)navRightButtonAction:(UIButton *)button{
    
    [self showSearchView];
   // [self allTabItemDeselected];
}



#pragma mark -
#pragma mark - Remove Others View

-(void)removeVideoandArticleView{
    
    [_videoArticleButton setImage:[UIImage imageNamed:@"navPlay"] forState:UIControlStateNormal];
    
  
    

//    if (DELEGATE.videoArticleVC != nil) {
//        [DELEGATE.videoArticleVC.view removeFromSuperview];
//    }
//
//    if (DELEGATE.searchVC != nil) {
//        [DELEGATE.searchVC.view removeFromSuperview];
//    }
    
}



#pragma mark - Banner

-(void)getBannerDetails{
    
    DELEGATE.bannerNotificationURL = [[NSURL alloc] init];
    DELEGATE.bannerVideoURL = [[NSURL alloc] init];
    DELEGATE.bannerArticleURL = [[NSURL alloc] init];
   
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getBannerInfoWithComplisionBlock:^(NSDictionary *result) {
        
        //NSLog(@"%@",result);
        
        if (result.count>2) {
            
            NSMutableArray *resultArray = [result mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSMutableDictionary *bannerDic = [resultArray objectAtIndex:i];
                
                NSString *link = [bannerDic valueForKey:@"banner"];
                
                NSArray *array1 = [bannerDic valueForKey:@"choice"];
                NSArray *array = [array1 objectAtIndex:0];
                NSNumber *num = [array objectAtIndex:0];

                
                if ([num integerValue] == 0) {
                    DELEGATE.bannerNotificationURL = [NSURL URLWithString:link];
                }else if ([num integerValue] == 1){
                    DELEGATE.bannerVideoURL = [NSURL URLWithString:link];
                }
                else if ([num integerValue] == 2){
                    DELEGATE.bannerArticleURL = [NSURL URLWithString:link];
                }
            }
            
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}

@end
