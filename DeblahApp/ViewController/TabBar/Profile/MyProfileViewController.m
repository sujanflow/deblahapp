//
//  MyProfileViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ProfileTableViewCell.h"

#import "SettingsViewController.h"
#import "EditViewController.h"
#import "PackageViewController.h"

#import "AudioAndRecorderViewController.h"

@interface MyProfileViewController ()<UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImage *chosenImage;
    NSMutableArray *descriptionArray;
    
    NSMutableDictionary *mainDic;

}

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsSelectedAction) name:@"SETTINGSPAGE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profilepage) name:@"PROFILEPAGE" object:nil];


    self.view.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];
    self.navigationController.navigationBar.hidden = YES;
    
    
//    self.mainContentView.clipsToBounds = YES;
//    self.mainContentView.layer.cornerRadius = 15;
    
    CGRect rect = self.mainContentView.bounds;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect
                                               byRoundingCorners:UIRectCornerTopLeft |UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *layers = [CAShapeLayer layer];
    layers.frame = rect;
    layers.path = path.CGPath;
    self.mainContentView.layer.mask = layers;
    
    
    self.coverImageVIew.clipsToBounds = YES;
    self.coverImageVIew.layer.cornerRadius = 15;
    
    
    self.textView.editable = NO;
    self.textView.selectable = NO;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionHeaderHeight = 40;
    

    [self makeShadowLeft];
    [self makeShadowRight];
    
    
    _basicInfoLabel.text = AMLocalizedString(@"Basic Information",nil) ;
    
   
    NSString *imageStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"PROFILE_IMAGE"];
    NSURL *imageUrl = [NSURL URLWithString:imageStr];
    [self setCoverPhoto:imageUrl];
    
    mainDic = [[NSMutableDictionary alloc] init];
    
    descriptionArray = [[NSMutableArray alloc] init];
 
     //  [self getUserDetailsInfo];
}

-(void)viewWillAppear:(BOOL)animated{
    
    
     [self getUserDetailsInfo];
    
//    if (DELEGATE.isEditedProfile) {
//        DELEGATE.isEditedProfile = NO;
//
//        descriptionArray = [[NSMutableArray alloc] init];
//
//        [self getInitialDataForUser];
//    }
    
    NSString *point = [[NSUserDefaults standardUserDefaults] valueForKey:@"POINTS"];
    _countLabel.text = point;
    
    if (DELEGATE.isPaymentComplete == YES) {
        [self makeTransaction];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    
}
-(void)viewWillLayoutSubviews{

    [self.textView setContentOffset:CGPointZero animated:NO];
    
  
    if (IS_IPHONE_X) {
        CGRect mainFrame = _mainContentView.frame;
        mainFrame.origin.y = 81+18;
        mainFrame.size.height = DEVICE_HEIGHT-81-18-49-33-20;
        _mainContentView.frame = mainFrame;
    }
        
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
   
}

-(void)makeShadowLeft{
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(_mainContentView.frame.origin.x-1, _mainContentView.frame.origin.y+15, 1, _mainContentView.frame.size.height-140)];
    leftView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:leftView];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(-5, 0, 0, 0);
    CGRect shadowPath = UIEdgeInsetsInsetRect(leftView.bounds, contentInsets);
    leftView.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    leftView.layer.shadowColor = [[UIColor redColor] CGColor];
    leftView.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    leftView.layer.shadowOpacity = 0.4f;
    leftView.layer.masksToBounds=NO;
}

-(void)makeShadowRight{
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(_mainContentView.frame.origin.x+_mainContentView.frame.size.width+1, _mainContentView.frame.origin.y+15, 1, _mainContentView.frame.size.height-140)];
    rightView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:rightView];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(-5, 0, 0, 0);
    CGRect shadowPath = UIEdgeInsetsInsetRect(rightView.bounds, contentInsets);
    rightView.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    rightView.layer.shadowColor = [[UIColor redColor] CGColor];
    rightView.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    rightView.layer.shadowOpacity = 0.4f;
    rightView.layer.masksToBounds=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getUserDetailsInfo{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getUserDetailsWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            NSString *point = [[NSUserDefaults standardUserDefaults] valueForKey:@"POINTS"];
            _countLabel.text = point;
            
            NSString *imageStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"PROFILE_IMAGE"];
            NSURL *imageUrl = [NSURL URLWithString:imageStr];
            [self setCoverPhoto:imageUrl];
            
             [self getInitialDataForUser];
            
            //profile image
            if (chosenImage == nil) {
                NSString *imageStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"PROFILE_IMAGE"];
                NSURL *imageUrl = [NSURL URLWithString:[imageStr stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                chosenImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
            }
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}



-(void)getInitialDataForUser{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getBasicDropDownWithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        if (result.count>1) {
            [self parseDataForTableView:result];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
}


-(void)parseDataForTableView:(NSDictionary *)result{
    
    NSMutableArray *optionBeautyArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionBloodArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionEducationArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionGenderArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionLivingArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionReligionArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionSkinArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionTribeArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *optionHeightArray = [[NSMutableArray alloc] init];
    NSMutableArray *optionWeightArray = [[NSMutableArray alloc] init];
    
    
    
    NSArray *beautyArray = [result valueForKey:@"beauty_level_option"];
    for (int i = 0; i<beautyArray.count; i++) {
        NSArray *array = [beautyArray objectAtIndex:i];
        [optionBeautyArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *bloodArray = [result valueForKey:@"blood_group_option"];
    for (int i = 0; i<bloodArray.count; i++) {
        NSArray *array = [bloodArray objectAtIndex:i];
        [optionBloodArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *educationArray = [result valueForKey:@"education_option"];
    for (int i = 0; i<educationArray.count; i++) {
        NSArray *array = [educationArray objectAtIndex:i];
        [optionEducationArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *genderArray = [result valueForKey:@"gender_option"];
    for (int i = 0; i<genderArray.count; i++) {
        NSArray *array = [genderArray objectAtIndex:i];
        [optionGenderArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *livingArray = [result valueForKey:@"living_status_option"];
    for (int i = 0; i<livingArray.count; i++) {
        NSArray *array = [livingArray objectAtIndex:i];
        [optionLivingArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *religionArray = [result valueForKey:@"religion_option"];
    for (int i = 0; i<religionArray.count; i++) {
        NSArray *array = [religionArray objectAtIndex:i];
        [optionReligionArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *skinArray = [result valueForKey:@"skin_color_option"];
    for (int i = 0; i<skinArray.count; i++) {
        NSArray *array = [skinArray objectAtIndex:i];
        [optionSkinArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *tribeArray = [result valueForKey:@"tribe_type_option"];
    for (int i = 0; i<tribeArray.count; i++) {
        NSArray *array = [tribeArray objectAtIndex:i];
        [optionTribeArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *heightArray = [result valueForKey:@"height_range_option"];
    for (int i = 0; i<heightArray.count; i++) {
        NSArray *array = [heightArray objectAtIndex:i];
        [optionHeightArray addObject: [array objectAtIndex:1]];
    }
    
    NSArray *weightArray = [result valueForKey:@"weight_range_option"];
    for (int i = 0; i<weightArray.count; i++) {
        NSArray *array = [weightArray objectAtIndex:i];
        [optionWeightArray addObject: [array objectAtIndex:1]];
    }
    
    //name
    NSString *name = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"FIRST_NAME"] ,[[NSUserDefaults standardUserDefaults] valueForKey:@"LAST_NAME"] ];
    _nameLabel.text = name;
    
    //birthday
    NSString *age = [[NSUserDefaults standardUserDefaults] valueForKey:@"BIRTH_DAY"];
    NSString *year = [AppSupport getTotalAge:age];
    _ageLabel.text = [NSString stringWithFormat:@"Age %@",year];
    
    
    //---basic info ---
    
    //gender
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    NSString *description = @"Gender";
    NSString *strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"GENDER"];
    NSInteger index = [strValue integerValue];
    NSString *valueStr = @"";
    
    valueStr = [optionGenderArray objectAtIndex:index] ;
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    [descriptionArray addObject:dic];
    
    
    
    //religion
    dic = [[NSMutableDictionary alloc] init];
    description = @"Religion";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"RELIGION"];
    index = [strValue integerValue];
    valueStr = @"";
    
    valueStr = [optionReligionArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //phone_number
    dic = [[NSMutableDictionary alloc] init];
    description = @"Phone No";
    valueStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"PHONE_NUMBER"];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //blood_group
    dic = [[NSMutableDictionary alloc] init];
    description = @"Blood Group";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"BLOOD_GROUP"];
    index = [strValue integerValue];
    valueStr = @"";
    valueStr = [optionBloodArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //occupation
    dic = [[NSMutableDictionary alloc] init];
    description = @"Occupation";
    valueStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"OCCUPATION"];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //height
    dic = [[NSMutableDictionary alloc] init];
    description = @"Height";
   
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"HEIGHT"];
    index = [strValue integerValue];
    valueStr = [optionHeightArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //weight
    dic = [[NSMutableDictionary alloc] init];
    description = @"Weight";
    
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"WEIGHT"];
    index = [strValue integerValue];
    valueStr = [optionWeightArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //beauty_level
    dic = [[NSMutableDictionary alloc] init];
    description = @"Beauty Level";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"BEAUTY_LEVEL"];
    index = [strValue integerValue];
    valueStr = [optionBeautyArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //skin_color
    dic = [[NSMutableDictionary alloc] init];
    description = @"Skin Color";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"SKIN_COLOR"];
    index = [strValue integerValue];
    valueStr = [optionSkinArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //living_status
    dic = [[NSMutableDictionary alloc] init];
    description = @"Living Status";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"LIVING_STATUS"];
    index = [strValue integerValue];
    valueStr = [optionLivingArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //tribe_type
    dic = [[NSMutableDictionary alloc] init];
    description = @"Tribe Type";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"TRIBE_TYPE"];
    index = [strValue integerValue];
    valueStr = [optionTribeArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //education
    dic = [[NSMutableDictionary alloc] init];
    description = @"Education";
    strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"EDUCATION"];
    index = [strValue integerValue];
    valueStr = [optionEducationArray objectAtIndex:index];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    
    
    [_tableView reloadData];

    
}


-(void)dataParse{
    //point
    
    
    //profile_image
    NSString *image = [mainDic valueForKey:@"profile_image"];
    NSURL *imageUrl = [NSURL URLWithString:image];
    [self setCoverPhoto:imageUrl];
    

    //name
    NSString *name = [NSString stringWithFormat:@"%@ %@",[mainDic valueForKey:@"first_name"],[mainDic valueForKey:@"last_name"]];
    _nameLabel.text = name;
    
    //birthday
    NSString *age = [mainDic valueForKey:@"birthday"];
    NSString *year = [AppSupport getTotalAge:age];
    _ageLabel.text = [NSString stringWithFormat:@"Age %@",year];
    
    
    //---basic info ---
    
    //gender
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    NSString *description = @"Gender";
    NSNumber *value = [mainDic valueForKey:@"gender"];
    NSString *valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Male" : ([value integerValue] == 1) ? @"Female" : @"Others";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
   
    
    //religion
    dic = [[NSMutableDictionary alloc] init];
    description = @"Religion";
    value = [mainDic valueForKey:@"religion"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Muslim" : ([value integerValue] == 1) ? @"Cristian" : ([value integerValue] == 2) ? @"Hindu" :([value integerValue] == 3) ? @"Buddist" : @"Others";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //phone_number
    dic = [[NSMutableDictionary alloc] init];
    description = @"Phone No";
    valueStr = [mainDic valueForKey:@"phone_number"];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //blood_group
    dic = [[NSMutableDictionary alloc] init];
    description = @"Blood Group";
    value = [mainDic valueForKey:@"blood_group"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"A+" : ([value integerValue] == 1) ? @"A-" : ([value integerValue] == 2) ? @"B+" :([value integerValue] == 3) ? @"B-" : ([value integerValue] == 4) ? @"O+" : ([value integerValue] == 5) ? @"O-" : ([value integerValue] == 6) ? @"AB+" : @"AB-";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //occupation
    dic = [[NSMutableDictionary alloc] init];
    description = @"Occupation";
    valueStr = [mainDic valueForKey:@"occupation"];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //height
    dic = [[NSMutableDictionary alloc] init];
    description = @"Height";
    value = [mainDic valueForKey:@"height"];
    valueStr = [NSString stringWithFormat:@"%ld",[value integerValue]];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //weight
    dic = [[NSMutableDictionary alloc] init];
    description = @"Weight";
    value = [mainDic valueForKey:@"weight"];
    valueStr = [NSString stringWithFormat:@"%ld",[value integerValue]];
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //beauty_level
    dic = [[NSMutableDictionary alloc] init];
    description = @"Beauty level";
    value = [mainDic valueForKey:@"beauty_level"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Choose 0" : ([value integerValue] == 1) ? @"Choose 1" : ([value integerValue] == 2) ? @"Choose 2" :([value integerValue] == 3) ? @"Choose 3" : @"Choose 4";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //skin_color
    dic = [[NSMutableDictionary alloc] init];
    description = @"Skin color";
    value = [mainDic valueForKey:@"skin_color"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Choose 0" : ([value integerValue] == 1) ? @"Choose 1" : ([value integerValue] == 2) ? @"Choose 2" :([value integerValue] == 3) ? @"Choose 3" : @"Choose 4";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];

    [descriptionArray addObject:dic];
    
    //living_status
    dic = [[NSMutableDictionary alloc] init];
    description = @"Living Status";
    value = [mainDic valueForKey:@"living_status"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Choose 0" : ([value integerValue] == 1) ? @"Choose 1" : ([value integerValue] == 2) ? @"Choose 2" :([value integerValue] == 3) ? @"Choose 3" : @"Choose 4";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //tribe_type
    dic = [[NSMutableDictionary alloc] init];
    description = @"Tribe type";
    value = [mainDic valueForKey:@"tribe_type"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Choose 0" : ([value integerValue] == 1) ? @"Choose 1" : ([value integerValue] == 2) ? @"Choose 2" :([value integerValue] == 3) ? @"Choose 3" : @"Choose 4";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    //education
    dic = [[NSMutableDictionary alloc] init];
    description = @"Education";
    value = [mainDic valueForKey:@"education"];
    valueStr = @"";
    
    valueStr = ([value integerValue] == 0) ? @"Choose 0" : ([value integerValue] == 1) ? @"Choose 1" : ([value integerValue] == 2) ? @"Choose 2" :([value integerValue] == 3) ? @"Choose 3" : @"Choose 4";
    
    [dic setValue:description forKey:@"PROFILE_DESCRIPTION"];
    [dic setValue:valueStr forKey:@"PROFILE_VALUE"];
    
    [descriptionArray addObject:dic];
    
    
   
    [_tableView reloadData];
}

-(void)setCoverPhoto: (NSURL *)imageUrl{
    
    [self.coverImageVIew sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"home-default"] options:SDWebImageRefreshCached];
}


#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return descriptionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileTableViewCell" owner:self options:nil];
        cell = (ProfileTableViewCell *)[topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [descriptionArray objectAtIndex:indexPath.row];
    
    NSString *description = [dic valueForKey:@"PROFILE_DESCRIPTION"];
    NSString *value = [dic valueForKey:@"PROFILE_VALUE"];
    
    
    cell.firstLabel.text = AMLocalizedString(description,nil);
    cell.secondLabel.text = value;
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 40: 35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}



#pragma mark - Button Action


-(IBAction)cameraButtonAction:(UIButton *)button{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

-(IBAction)editButtonAction:(UIButton *)button{
   
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    EditViewController *editVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"EditViewController"];

    editVC.profileImage = chosenImage;
    editVC.descriptionArray = descriptionArray;

    [self presentViewController:editVC animated:YES completion:nil];
    
}
-(IBAction)pointButtonAction:(UIButton *)button{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    PackageViewController *packagetVC = [mainStoryboard instantiateViewControllerWithIdentifier: @"PackageViewController"];
    
    [self.navigationController pushViewController:packagetVC animated:YES];
}


-(void)updateProfileImage{
    
    
    if (DELEGATE.isReachable == NO) {
        [DELEGATE makeFillupPopUp:@"No Internet"];
        return;
    }
    
    NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] init];
    
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_NAME"] forKey:@"user_name"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"FIRST_NAME"] forKey:@"first_name"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"LAST_NAME"] forKey:@"last_name"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"GENDER"] forKey:@"gender"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"BIRTH_DAY"] forKey:@"birthday"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"RELIGION"] forKey:@"religion"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"PHONE_NUMBER"] forKey:@"phone_number"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"BLOOD_GROUP"] forKey:@"blood_group"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"OCCUPATION"] forKey:@"occupation"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"HEIGHT"] forKey:@"height"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"HEIGHT"] forKey:@"weight"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"BEAUTY_LEVEL"]forKey:@"beauty_level"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SKIN_COLOR"] forKey:@"skin_color"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"LIVING_STATUS"] forKey:@"living_status"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"TRIBE_TYPE"] forKey:@"tribe_type"];
    [infoDic setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"EDUCATION"] forKey:@"education"];
    
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] userProfileUpdateWithInfo:infoDic andImage:chosenImage WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            NSString *image = [result valueForKey:@"profile_image"];
            NSURL *imageUrl = [NSURL URLWithString:image];
            [self setCoverPhoto:imageUrl];
            
        }else{
            
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
    }];
    
}
#pragma mark-
#pragma mark- UImage Picker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    [self updateProfileImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - Notification

-(void)settingsSelectedAction{
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    SettingsViewController *settingsVC = (SettingsViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"SettingsViewController"];
    
    [self.navigationController pushViewController:settingsVC animated:YES];
}

-(void)profilepage{
   
     [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark - Payment

-(void)makeTransaction{
    
    NSLog(@"%@",DELEGATE.paymentID);
    
    if (DELEGATE.paymentID.length == 0) {
        
        return;
    }
    
    NSMutableDictionary *payDic = [[NSMutableDictionary alloc] init];
    [payDic setValue:DELEGATE.paymentID forKey:@"p_id"];
    
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getTransactionInfo:payDic WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([result valueForKey:@"id"]) {
            
            DELEGATE.isPaymentComplete = NO;
            DELEGATE.paymentID = @"";
            
            NSString *amount = [result valueForKey:@"amount"];
            [[NSUserDefaults standardUserDefaults] setValue:amount forKey:@"PURCHASE_AMOUNT"];
            
//            NSString *transactionId = [result valueForKey:@"id"];
//            NSString *amount = [result valueForKey:@"amount"];
//            NSString *currency = [result valueForKey:@"currency"];
//            NSString *payment_reference = [result valueForKey:@"payment_reference"];
//            NSString *pt_invoice_id = [result valueForKey:@"pt_invoice_id"];
//            NSString *reference_no = [result valueForKey:@"reference_no"];
//            NSString *response_code = [result valueForKey:@"response_code"];
//            NSString *result = [result valueForKey:@"result"];
//            NSString *transaction_id = [result valueForKey:@"transaction_id"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
            });
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];

            
            
            [DELEGATE makeFillupPopUp:@"Payment Successfull"];
            [self getUserDetailsInfo];
           
        }
        else{
            [DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
      
    }];
   
}
@end

