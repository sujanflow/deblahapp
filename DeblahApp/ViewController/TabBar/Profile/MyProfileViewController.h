//
//  MyProfileViewController.h
//  DeblahApp
//
//  Created by Sabuj on 3/30/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : UIViewController


@property (nonatomic, weak) IBOutlet UIView *mainContentView;

@property (nonatomic, weak) IBOutlet UIView *photoView;

@property (nonatomic, weak) IBOutlet UIImageView *coverImageVIew;
@property (nonatomic, weak) IBOutlet UIButton *cameraButton;
@property (nonatomic, weak) IBOutlet UIButton *editButton;


@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageLabel;

@property (nonatomic, weak) IBOutlet UIButton *likeButton;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;

@property (nonatomic, weak) IBOutlet UITextView *textView;


@property (nonatomic, weak) IBOutlet UILabel *basicInfoLabel;

@property (nonatomic, weak) IBOutlet UITableView *tableView;


@end
