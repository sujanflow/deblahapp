//
//  ChatViewController.m
//  DeblahApp
//
//  Created by Sabuj on 3/23/18.
//  Copyright © 2018 Sabuj. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatTableViewCell.h"
#import "LiveChatViewController.h"

@interface ChatViewController ()<UITableViewDelegate,UITableViewDataSource>
{
   
    NSMutableArray *chatArray;

    NSInteger chatCurrentPage;
    BOOL nextchatPageAvailable;
    
}
@end

@implementation ChatViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatpage) name:@"CHATPAGE" object:nil];

  
    self.view.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1];
    self.navigationController.navigationBar.hidden = YES;
    
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    _emptyView.hidden = YES;
 
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    chatArray = [[NSMutableArray alloc] init];
    
   [self getDataFromAPI];
  //  [self getDataFromAPIFake];
}

-(void)viewWillLayoutSubviews{
    
    if (IS_IPHONE_X) {
        CGRect tableFrame = _tableView.frame;
        tableFrame.origin.y = 81;
        tableFrame.size.height = DEVICE_HEIGHT-81;
        _tableView.frame = tableFrame;
        _emptyView.frame = tableFrame;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDataFromAPIFake{
    
    
    for (int i=0; i<10; i++) {
        
        NSMutableDictionary *chatDictionaray = [[NSMutableDictionary alloc] init];
        
        NSURL *imageUrl = [NSURL URLWithString:@"https://130513-375933-1-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2016/03/Disha-Patani-1.jpg"];
        
        NSString *name = @"Nusrat Jahan Aisha";
        NSString *lastConversationDate = @"18 May 2018";
        
        NSString *userId = [NSString stringWithFormat:@"%d",i+1];
        [chatDictionaray setValue:userId forKey:@"USER_ID"];
        [chatDictionaray setValue:imageUrl forKey:@"IMAGE_URL"];
        [chatDictionaray setValue:name forKey:@"NAME"];
        [chatDictionaray setValue:lastConversationDate forKey:@"LAST_CONVERSATION_DATE"];
       
        
        [chatArray addObject:chatDictionaray];
    }
    
}

-(void)getDataFromAPI{
    
    nextchatPageAvailable = NO;
    chatCurrentPage = 1;
    
    [self getChatHistoryByPage:chatCurrentPage];
   
}

-(void)getChatHistoryByPage : (NSInteger)pageNumber{
    
    [MBProgressHUD showHUDAddedTo:DELEGATE.window animated:YES];
    
    [[APIClient sharedInstance] getChatHistryListInfoPage:pageNumber WithComplisionBlock:^(NSDictionary *result) {
        
        NSLog(@"getChatHistoryByPage result %@",result);
        
        if ([result valueForKey:@"results"]) {
            
            if ([result valueForKey:@"next"] != [NSNull null]) {
                nextchatPageAvailable = YES;
                chatCurrentPage = [AppSupport nextPageParser:[result valueForKey:@"next"]];
                
            }else{
                nextchatPageAvailable = NO;
            }
            
            NSMutableArray *resultArray = [[result valueForKey:@"results"] mutableCopy];
            
            for (int i=0; i<resultArray.count; i++) {
                
                NSDictionary *dic = [resultArray objectAtIndex:i];
                
                NSNumber *num = [dic valueForKey:@"id"];
                NSString *userID = [NSString stringWithFormat:@"%ld",[num integerValue]];
                NSString *userName = [dic valueForKey:@"user_name"];
                NSString *lastDate = [AppSupport getDateStringFromDate:[AppSupport getNSDateFromServerDate:[dic valueForKey:@"last_conversation"]]];
                NSURL *imagUrl = [NSURL URLWithString: [dic valueForKey:@"profile_image"]] ;
              //  NSString *lastConversation = [dic valueForKey:@"last_conversation"];
                
                
                NSMutableDictionary *chatDic = [[NSMutableDictionary alloc] init];
            
                [chatDic setValue:userID forKey:@"USER_ID"];
                [chatDic setValue:imagUrl forKey:@"IMAGE_URL"];
                [chatDic setValue:userName forKey:@"NAME"];
                [chatDic setValue:lastDate forKey:@"LAST_CONVERSATION_DATE"];
                
                
                [chatArray addObject:chatDic];
                
            }
            
           
            
            
            [_tableView reloadData];
            
            
            
        }else{
            
            //[DELEGATE makeFillupPopUp:@"Error"];
        }
        
        
        //empty Check
        if (chatArray.count>0) {
            _emptyView.hidden = YES;
        }else{
            _emptyView.hidden = NO;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:DELEGATE.window animated:YES];
        });
        
        
    }];
    
}

#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return chatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatCell"];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ChatTableViewCell" owner:self options:nil];
        cell = (ChatTableViewCell *)[topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    NSDictionary *dic = [chatArray objectAtIndex:indexPath.row];
    
    NSURL *imageUrl = [dic valueForKey:@"IMAGE_URL"];
    NSString *name = [dic valueForKey:@"NAME"];
    NSString *lastConversation = [dic valueForKey:@"LAST_CONVERSATION_DATE"];
   
    cell.nameLabel.text = name;
    cell.dateLabel.text = [NSString stringWithFormat:@"Last Conv: %@",lastConversation];
    
    [cell.coverImageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"chat-default"] options:SDWebImageRefreshCached];
    
    
    if (indexPath.row == chatArray.count-5) {
        if (DELEGATE.isReachable) {
            if (nextchatPageAvailable) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getChatHistoryByPage:chatCurrentPage];
                });
            }
        }
    }
   
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_DEVICE_IPAD ? 90: 81;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"TAB_DESELECT" object:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSString *storyBoardName = IS_DEVICE_IPAD ? @"Main_iPad" : @"Main";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyBoardName bundle: nil];
    
    
    LiveChatViewController *liveChatViewController = (LiveChatViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"LiveChatViewController"];
    
    NSDictionary *dic = [chatArray objectAtIndex:indexPath.row];
    liveChatViewController.otherPersonID = [dic valueForKey:@"USER_ID"];
   // [self.navigationController pushViewController:liveChatViewController animated:YES];
    [self.navigationController presentViewController:liveChatViewController animated:YES completion:nil];
}

#pragma mark - Notification
-(void)chatpage{
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end
